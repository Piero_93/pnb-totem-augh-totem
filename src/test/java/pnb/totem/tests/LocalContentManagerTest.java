package pnb.totem.tests;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.testkit.javadsl.TestKit;
import io.swagger.client.model.ContentMetaData;
import org.apache.commons.collections4.CollectionUtils;
import pnb.totem.actors.LocalContentManager;
import pnb.totem.internetcommunication.FTPClient;
import pnb.totem.internetcommunication.SwaggerClient;
import pnb.totem.messages.*;
import pnb.totem.model.*;
import pnb.totem.utilities.Utilities;

import java.util.*;

import static akka.testkit.JavaTestKit.duration;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static pnb.totem.utilities.SysKB.*;

/**
 * Created by Biagini on 18/04/2017.
 */
public class LocalContentManagerTest extends AbstractActorTester {

    private static final String TEST_TIMEOUT = "20 seconds";

    @Override
    public void test(ActorRef actor, Map<String, TestKit> probeRefs) throws Exception {
        //sample tags set
        Set<String> tags = new HashSet<>();
        tags.add(SAMPLE_TAG);

        //sample content
        ContentMetaData metaData = SwaggerClient.getContentWithId("" + SAMPLE_CONTENT_ID);
        ContentBundle bundle = null;
        try {
            //ZipFile zipFile = FTPClient.fetchContent(metaData.getUrl());
            //bundle = new ContentBundleImpl(Integer.parseInt(metaData.getId()), zipFile);
            bundle = new ContentBundleImpl(Integer.parseInt(metaData.getId()), FTPClient.fetchContent(metaData.getUrl()));
        } catch (Exception ignored) {
            assertTrue(false);
        }
        Content content = new Content(
                Integer.parseInt(metaData.getId()),
                metaData.getProviderId(),
                new HashSet<>(metaData.getTag()),
                metaData.getUrl(),
                metaData.getExpirationDate().toCalendar(Locale.ITALIAN),
                metaData.getRev(),
                bundle);

        {
            actor.tell(new MsgCheckContentsForTags(tags), ActorRef.noSender());
            MsgFetchContentIdsForTag msgFetchContentIdsForTag = probeRefs.get(INTERNET_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgFetchContentIdsForTag.class);
            assertEquals(SAMPLE_TAG, msgFetchContentIdsForTag.getTag());
        }

        {
            Set<Integer> contentsId = new HashSet<>();
            contentsId.add(SAMPLE_CONTENT_ID);
            actor.tell(new MsgSaveContentIds(SAMPLE_TAG, contentsId), ActorRef.noSender());
            for (int i = 0; i < contentsId.size(); i++) {
                MsgFetchContentWithId msgFetchContentWithId = probeRefs.get(INTERNET_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgFetchContentWithId.class);
                assertEquals(SAMPLE_CONTENT_ID, msgFetchContentWithId.getId());
            }
        }

        {
            actor.tell(new MsgSaveContent(content), ActorRef.noSender());
            actor.tell(new MsgRequestSingleContentForTag(metaData.getTag().get(0)), ActorRef.noSender());
            MsgResponseSingleContentForTag msgResponseSingleContentForTag = probeRefs.get(GUI_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgResponseSingleContentForTag.class);
            assertEquals(content, msgResponseSingleContentForTag.getContent());
        }

        {
            actor.tell(new MsgRequestSingleContentForTag(SAMPLE_TAG), ActorRef.noSender());
            MsgContentNotPresent msgContentNotPresent = probeRefs.get(GUI_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgContentNotPresent.class);
            assertEquals(SAMPLE_TAG, msgContentNotPresent.getTag());
        }

        {
            actor.tell(new MsgRequestSingleContentForTag(SAMPLE_TAG), ActorRef.noSender());
            MsgContentNotPresent msgContentNotPresent = probeRefs.get(GUI_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgContentNotPresent.class);
            assertEquals(SAMPLE_TAG, msgContentNotPresent.getTag());
        }

        {
            List<ContentMetadataWithoutThumbnailAndSplash> metaDatas = new ArrayList<>();
            ContentMetaData rawMetaData = SwaggerClient.getContentWithId("" + SAMPLE_CONTENT_ID);
            ContentMetadataWithoutThumbnailAndSplash metadata = new ContentMetadataWithoutThumbnailAndSplash();
            metadata.setContentId(Integer.parseInt(rawMetaData.getId()));
            metadata.setExpirationDate(Utilities.dateToString(rawMetaData.getExpirationDate().toCalendar(Locale.ENGLISH)));
            metadata.setProviderId(rawMetaData.getProviderId());
            metadata.setTag(new HashSet<>(rawMetaData.getTag()));
            metadata.setUrl(rawMetaData.getUrl());
            metadata.setRev(rawMetaData.getRev());
            metaDatas.add(metadata);
            int token = 1;
            actor.tell(new MsgAppContentList(metaDatas, token), ActorRef.noSender());
            MsgRequestShownContents msgRequestShownContents = probeRefs.get(GUI_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgRequestShownContents.class);
            assertTrue(msgRequestShownContents.getMsg().isPresent());
            assertEquals(msgRequestShownContents.getMsg().get().getToken(), token);
            assertTrue(CollectionUtils.isEqualCollection(msgRequestShownContents.getMsg().get().getContents(), metaDatas));

            //try MsgResponseShownContents
            List<Content> shownContents = new ArrayList<>();
            shownContents.add(content);
            actor.tell(new MsgResponseShownContents(new MsgAppContentList(metaDatas, token),shownContents, 4), ActorRef.noSender());
            MsgSpreadContentIds msgSpreadContentIds = probeRefs.get(LOCAL_COMMUNICATION_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgSpreadContentIds.class);
            assertTrue(msgSpreadContentIds.getToBeSent().isEmpty());
            assertTrue(msgSpreadContentIds.getToBeReceived().isEmpty());
            assertEquals(msgSpreadContentIds.getToken(), token);
        }

        {
            int token = 1;
            actor.tell(new MsgGetContentToSpread(Integer.parseInt(metaData.getId()), token), ActorRef.noSender());
            MsgSendContentToSpread msgSendContentToSpread = probeRefs.get(LOCAL_COMMUNICATION_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgSendContentToSpread.class);
            assertTrue(msgSendContentToSpread.getContent().isPresent());
            assertEquals(msgSendContentToSpread.getContent().get(), content);
            assertEquals(msgSendContentToSpread.getToken(), token);
        }
    }

    @Override
    public Class<? extends UntypedActor> getTestActorClass() {
        return LocalContentManager.class;
    }
}
