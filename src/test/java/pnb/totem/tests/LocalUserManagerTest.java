package pnb.totem.tests;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.testkit.javadsl.TestKit;
import org.apache.commons.collections4.CollectionUtils;
import pnb.totem.actors.LocalUserManager;
import pnb.totem.messages.*;
import pnb.totem.utilities.SysKB;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static akka.testkit.JavaTestKit.duration;
import static pnb.totem.utilities.SysKB.*;
import static org.junit.Assert.*;


/**
 * Created by Marco on 18/04/2017.
 */
public class LocalUserManagerTest extends AbstractActorTester {

    private static final String TEST_TIMEOUT = "20 seconds";

    @Override
    public void test(ActorRef actor, Map<String, TestKit> probeRefs) throws Exception {
        Set<String> set = new HashSet<>();
        set.add(SAMPLE_TAG);
        {
            //MsgSaveTagsForUser message
            actor.tell(new MsgSaveTagsForUser(SAMPLE_USER_NAME, set), ActorRef.noSender());
            MsgCheckContentsForTags msgCheckContentsForTags = probeRefs.get(CONTENT_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgCheckContentsForTags.class);
            assertTrue(CollectionUtils.isEqualCollection(set, msgCheckContentsForTags.getTags()));
        }
        {
            //MsgCheckTagsForUser message
            String username;
            //Case user doesn't exist
            username = "IDontExist";
            actor.tell(new MsgCheckTagsForUser(username), ActorRef.noSender());
            MsgFetchTagsForUser msgFetchTagsForUser = probeRefs.get(INTERNET_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgFetchTagsForUser.class);
            assertTrue(username.equals(msgFetchTagsForUser.getUsername()));
            //Case user exists
            username = SAMPLE_USER_NAME;
            actor.tell(new MsgCheckTagsForUser(username), ActorRef.noSender());
            MsgCheckContentsForTags msgCheckContentsForTags = probeRefs.get(CONTENT_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgCheckContentsForTags.class);
            assertTrue(CollectionUtils.isEqualCollection(set, msgCheckContentsForTags.getTags()));
            Thread.sleep(SysKB.USER_CACHE_TIMEOUT_MILLIS+1000);
            //Case User timeouted
            actor.tell(new MsgCheckTagsForUser(username), ActorRef.noSender());
            msgFetchTagsForUser = probeRefs.get(INTERNET_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgFetchTagsForUser.class);
            assertTrue(username.equals(msgFetchTagsForUser.getUsername()));
        }
        {
            actor.tell(new MsgSaveTagsForUser(SAMPLE_USER_NAME, set), ActorRef.noSender());
            //MsgFetchTopTags message
            //EMPTY notToTake (expect full ranking)
            actor.tell(new MsgFetchTopTags(5, new HashSet<>()), ActorRef.noSender());
            MsgRanking msgRanking = probeRefs.get(GUI_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgRanking.class);
            assertTrue(CollectionUtils.isEqualCollection(msgRanking.getRanking(), set));
            //FULL notToTake (expect empty ranking)
            actor.tell(new MsgFetchTopTags(5, set), ActorRef.noSender());
            msgRanking = probeRefs.get(GUI_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgRanking.class);
            assertTrue(msgRanking.getRanking().isEmpty());

        }
    }

    @Override
    public Class<? extends UntypedActor> getTestActorClass() {
        return LocalUserManager.class;
    }
}
