package pnb.totem.tests;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.testkit.javadsl.TestKit;
import io.swagger.client.model.Position;
import org.apache.commons.collections4.CollectionUtils;
import pnb.totem.actors.InternetCommunicationManager;
import pnb.totem.internetcommunication.SwaggerClient;
import pnb.totem.messages.*;

import java.util.Map;

import static akka.testkit.JavaTestKit.duration;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static pnb.totem.utilities.SysKB.CONTENT_MANAGER_KEY;
import static pnb.totem.utilities.SysKB.USER_MANAGER_KEY;

/**
 * Created by Biagini on 18/04/2017.
 */
public class InternetCommunicationManagerTest extends AbstractActorTester {

    private static final String TEST_TIMEOUT = "20 seconds";

    @Override
    public void test(ActorRef actor, Map<String, TestKit> probeRefs) throws Exception {
        {
            Position position = new Position();
            position.setLatitude(0.0);
            position.setLongitude(0.0);
            actor.tell(new MsgNearUser(SAMPLE_USER_NAME, position), ActorRef.noSender());
            MsgCheckTagsForUser msgCheckTagsForUser = probeRefs.get(USER_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgCheckTagsForUser.class);
            assertEquals(SAMPLE_USER_NAME, msgCheckTagsForUser.getUsername());
        }

        {
            actor.tell(new MsgFetchTagsForUser(SAMPLE_USER_NAME), ActorRef.noSender());
            MsgSaveTagsForUser msgSaveTagsForUser = probeRefs.get(USER_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgSaveTagsForUser.class);
            assertEquals(SAMPLE_USER_NAME, msgSaveTagsForUser.getUsername());
            assertTrue(CollectionUtils.isEqualCollection(SwaggerClient.getTagsForUser(SAMPLE_USER_NAME), msgSaveTagsForUser.getTags()));
        }

        {
            actor.tell(new MsgFetchContentIdsForTag(SAMPLE_TAG), ActorRef.noSender());
            MsgSaveContentIds msgSaveContentIds = probeRefs.get(CONTENT_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgSaveContentIds.class);
            assertEquals(SAMPLE_TAG, msgSaveContentIds.getTag());
            assertTrue(msgSaveContentIds.getContents().isEmpty());
        }

        {
            actor.tell(new MsgFetchContentWithId(SAMPLE_CONTENT_ID), ActorRef.noSender());
            MsgSaveContent msgSaveContent = probeRefs.get(CONTENT_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgSaveContent.class);
            assertEquals(SAMPLE_CONTENT_ID, msgSaveContent.getContent().getId());
        }

        {
            actor.tell(new MsgFetchContentWithId(SAMPLE_NOT_VALID_CONTENT_ID), ActorRef.noSender());
            MsgTimeoutWhileDownloading msgTimeoutWhileDownloading = probeRefs.get(CONTENT_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgTimeoutWhileDownloading.class);
            assertEquals(SAMPLE_NOT_VALID_CONTENT_ID, msgTimeoutWhileDownloading.getContentId());
        }
    }

    @Override
    public Class<? extends UntypedActor> getTestActorClass() {
        return InternetCommunicationManager.class;
    }
}
