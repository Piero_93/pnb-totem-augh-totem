package pnb.totem.tests;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.testkit.javadsl.TestKit;
import io.swagger.client.model.ContentMetaData;
import org.apache.commons.collections4.CollectionUtils;
import pnb.totem.actors.LocalContentManager;
import pnb.totem.internetcommunication.FTPClient;
import pnb.totem.internetcommunication.SwaggerClient;
import pnb.totem.messages.*;
import pnb.totem.model.Content;
import pnb.totem.model.ContentBundle;
import pnb.totem.model.ContentBundleImpl;
import pnb.totem.utilities.SysKB;
import pnb.totem.utilities.Utilities;

import java.util.*;

import static akka.testkit.JavaTestKit.duration;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static pnb.totem.utilities.SysKB.GUI_MANAGER_KEY;
import static pnb.totem.utilities.SysKB.LOCAL_COMMUNICATION_MANAGER_KEY;
import static pnb.totem.utilities.Utilities.contentToContentMetadataWithoutThumbnailAndSplash;

/**
 * Created by Biagini on 11/07/2017.
 */
public class SpreadingAlgorithmTest extends AbstractActorTester {
    public static final String TEST_TIMEOUT = "20 seconds";

    private static final String SAMPLE_TAG_1 = "tag1";
    private static final String SAMPLE_TAG_2 = "tag2";
    private static final String SAMPLE_TAG_3 = "tag3";

    @Override
    public void test(ActorRef actor, Map<String, TestKit> probeRefs) throws Exception {
        ContentMetaData metaData = SwaggerClient.getContentWithId("" + SAMPLE_CONTENT_ID);
        ContentBundle bundle = new ContentBundleImpl(Integer.parseInt(metaData.getId()), FTPClient.fetchContent(metaData.getUrl()));


        List<Content> sampleContents = new ArrayList<>();
        Set<String> tags = new HashSet<>();

        List<Content> appContent = new ArrayList<>();

        { //Content 0
            tags.add(SAMPLE_TAG_1);
            tags.add(SAMPLE_TAG_2);
            tags.add(SAMPLE_TAG_3);
            sampleContents.add(new Content(0, "prov0", tags, "url0", Calendar.getInstance(), 0, bundle));
            tags.clear();
        }

        { //Content 1
            tags.add(SAMPLE_TAG_1);
            sampleContents.add(new Content(1, "prov1", tags, "url1", Calendar.getInstance(), 0, bundle));
            tags.clear();
        }

        { //Content 2
            tags.add(SAMPLE_TAG_2);
            sampleContents.add(new Content(2, "prov2", tags, "url2", Calendar.getInstance(), 0, bundle));
            tags.clear();
        }

        { //Content 3
            tags.add(SAMPLE_TAG_3);
            sampleContents.add(new Content(3, "prov3", tags, "url3", Calendar.getInstance(), 0, bundle));
            tags.clear();
        }

        { //Content 4
            tags.add(SAMPLE_TAG_3);
            sampleContents.add(new Content(4, "prov4", tags, "url4", Calendar.getInstance(), 0, bundle));
            tags.clear();
        }

        actor.tell(new MsgSaveContent(sampleContents.get(3)), ActorRef.noSender());

        appContent.add(sampleContents.get(2));
        appContent.add(sampleContents.get(4));

        actor.tell(new MsgResponseShownContents(
                new MsgAppContentList(contentToContentMetadataWithoutThumbnailAndSplash(appContent), 1),
                new ArrayList<>(0),
                1), ActorRef.noSender());

        MsgSpreadContentIds msgSpreadContentIds = probeRefs.get(LOCAL_COMMUNICATION_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgSpreadContentIds.class);
        assertTrue(CollectionUtils.isEqualCollection(msgSpreadContentIds.getToBeReceived(), Collections.singleton(sampleContents.get(2).getId())));
        assertTrue(CollectionUtils.isEqualCollection(msgSpreadContentIds.getToBeSent(), Collections.singleton(sampleContents.get(3).getId())));

        actor.tell(new MsgSaveContent(sampleContents.get(2)), ActorRef.noSender());
        actor.tell(new MsgSaveContent(sampleContents.get(0)), ActorRef.noSender());

        appContent.clear();
        appContent.add(sampleContents.get(1));
        appContent.add(sampleContents.get(2));
        appContent.add(sampleContents.get(3));

        actor.tell(new MsgResponseShownContents(
                new MsgAppContentList(contentToContentMetadataWithoutThumbnailAndSplash(appContent), 1),
                new ArrayList<>(0),
                1), ActorRef.noSender());

        msgSpreadContentIds = probeRefs.get(LOCAL_COMMUNICATION_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgSpreadContentIds.class);
        System.out.println(msgSpreadContentIds);
        assertTrue(CollectionUtils.isEqualCollection(msgSpreadContentIds.getToBeReceived(), Collections.singleton(sampleContents.get(1).getId())));
        assertTrue(CollectionUtils.isEqualCollection(msgSpreadContentIds.getToBeSent(), Collections.singleton(sampleContents.get(0).getId())));

        actor.tell(new MsgResponseShownContents(
                new MsgAppContentList(contentToContentMetadataWithoutThumbnailAndSplash(appContent), 1),
                new ArrayList<>(0),
                1), ActorRef.noSender());
        msgSpreadContentIds = probeRefs.get(LOCAL_COMMUNICATION_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgSpreadContentIds.class);
        assertTrue(msgSpreadContentIds.getToBeReceived().isEmpty());
        assertTrue(CollectionUtils.isEqualCollection(msgSpreadContentIds.getToBeSent(), Collections.singleton(sampleContents.get(0).getId())));

        Thread.sleep(SysKB.SPREAD_CACHE_TIMEOUT_MILLIS);
        actor.tell(new MsgResponseShownContents(
                new MsgAppContentList(contentToContentMetadataWithoutThumbnailAndSplash(appContent), 1),
                new ArrayList<>(0),
                1), ActorRef.noSender());

        msgSpreadContentIds = probeRefs.get(LOCAL_COMMUNICATION_MANAGER_KEY).expectMsgClass(duration(TEST_TIMEOUT), MsgSpreadContentIds.class);
        System.out.println(msgSpreadContentIds);
        assertTrue(CollectionUtils.isEqualCollection(msgSpreadContentIds.getToBeReceived(), Collections.singleton(sampleContents.get(1).getId())));
        assertTrue(CollectionUtils.isEqualCollection(msgSpreadContentIds.getToBeSent(), Collections.singleton(sampleContents.get(0).getId())));
    }

    @Override
    public Class<? extends UntypedActor> getTestActorClass() {
        return LocalContentManager.class;
    }
}
