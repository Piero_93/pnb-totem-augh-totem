package pnb.totem.tests;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.testkit.TestActor;
import akka.testkit.javadsl.TestKit;
import com.google.gson.*;
import io.swagger.client.model.ContentMetaData;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import pnb.totem.actors.LocalCommunicationManager;
import pnb.totem.internetcommunication.FTPClient;
import pnb.totem.internetcommunication.SwaggerClient;
import pnb.totem.localcommunication.LocalUserDataBundle;
import pnb.totem.localcommunication.LocalUserIncomingPullRequest;
import pnb.totem.localcommunication.LocalUserIncomingSpreadBundle;
import pnb.totem.messages.*;
import pnb.totem.model.*;
import pnb.totem.utilities.SysKB;
import pnb.totem.utilities.Utilities;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.zip.ZipFile;

import static org.junit.Assert.*;

public class LocalCommunicationManagerTest extends AbstractActorTester {

    private static final String TEST_TIMEOUT = "20 seconds";

    @Override
    public void test(ActorRef actor, Map<String, TestKit> probeRefs) throws Exception {
        //Prepare test
        List<Content> testContents = new ArrayList<>();
        testContents.add(createSampleContent());
        int testFrameNumber = (int) ((Math.random() * 4.0) + testContents.size());

        String localServerIp = Utilities.readIpFromFile();
        int localServerPort = Utilities.readPortFromFile();
        LocalUserDataBundle userDataBundle = new LocalUserDataBundle();
        userDataBundle.setUserId(SAMPLE_USER_NAME);
        userDataBundle.setTag(new HashSet<>(SAMPLE_USER_TAGS));

        //Execute test (N contents)
        testWithInputs(localServerIp, localServerPort, userDataBundle,
                probeRefs, testContents, testFrameNumber);

        //Execute test (0 contents)
        testContents.clear();
        testWithInputs(localServerIp, localServerPort, userDataBundle,
                probeRefs, testContents, testFrameNumber);

        //Execute test with spreading
        testSpreadingFromAppToTotem(localServerIp, localServerPort, probeRefs, testContents);
        testSpreadingFromTotemToApp(localServerIp, localServerPort, probeRefs, testContents);
    }

    private void testWithInputs(String localServerIp,
                                int localServerPort,
                                LocalUserDataBundle userDataBundle,
                                Map<String, TestKit> probeRefs,
                                List<Content> testContents,
                                int testFrameNumber) throws IOException {
        //Set date time format
        TimeZone tz = TimeZone.getTimeZone("UTC");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setTimeZone(tz);

        //Create content metadata from Content
        List<ContentMetadataWithoutThumbnailAndSplash> testContentsMetadata = testContents.stream().map((c) -> {
            ContentMetadataWithoutThumbnailAndSplash metaData = new ContentMetadataWithoutThumbnailAndSplash();
            metaData.setContentId(c.getId());
            metaData.setUrl(c.getUrl());
            metaData.setExpirationDate(df.format(c.getExpirationDate().getTime()));
            metaData.setProviderId(c.getProviderId());
            metaData.setTag(c.getTags());
            metaData.setRev(c.getRevNumber());

            //metaData.setThumbnail(extractBytes(c.getBundle().getThumbnail()));
            //metaData.setSplashScreen(extractBytes(c.getBundle().getSplashScreen()));

            return metaData;
        }).collect(Collectors.toList());

        //The gui manager will reply with the testContentsMetadata
        probeRefs.get(SysKB.GUI_MANAGER_KEY).setAutoPilot(new TestActor.AutoPilot() {
            @Override
            public TestActor.AutoPilot run(ActorRef actorRef, Object o) {
                actorRef.tell(
                        new MsgResponseShownContents(testContents, testFrameNumber),
                        probeRefs.get(SysKB.GUI_MANAGER_KEY).getRef());
                return this;
            }
        });

        //The gui manager will reply with the testContentsMetadata
        probeRefs.get(SysKB.CONTENT_MANAGER_KEY).setAutoPilot(new TestActor.AutoPilot() {
            @Override
            public TestActor.AutoPilot run(ActorRef actorRef, Object o) {
                MsgGetContentToSpread msg = ((MsgGetContentToSpread) o);
                Content tobeSent = testContents.stream().filter(c -> c.getId() == msg.getId()).findFirst().get();
                actorRef.tell(
                        new MsgSendContentToSpread(tobeSent, msg.getToken()),
                        probeRefs.get(SysKB.CONTENT_MANAGER_KEY).getRef());
                return this;
            }
        });

        //Deserialize response from the HTTP json body
        ResponseHandler<ResponseToUserAdvertise> advertiseResponseHandler = response -> {
            int status = response.getStatusLine().getStatusCode();
            assertTrue(status >= 200 && status < 300);

            return customGson.fromJson(EntityUtils.toString(response.getEntity()), ResponseToUserAdvertise.class);
        };

        //Read image from HTTP body
        ResponseHandler<byte[]> imageResponseHandler = response -> {
            int status = response.getStatusLine().getStatusCode();
            assertTrue(status >= 200 && status < 300);


            return EntityUtils.toByteArray(response.getEntity());
        };

        //Execute the HTTP interaction (advertise)
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost("http://" + localServerIp + ":" + localServerPort + "/");
            httpPost.setHeader("Content-Type", "application/json");
            StringEntity xmlEntity = new StringEntity(customGson.toJson(userDataBundle));
            httpPost.setEntity(xmlEntity);

            ResponseToUserAdvertise responseBody = httpclient.execute(httpPost, advertiseResponseHandler);
            assertNotNull(responseBody);

            //Must have asked the Gui Manager for current contents on screen
            probeRefs.get(SysKB.GUI_MANAGER_KEY).expectMsgClass(Duration.Zero(), MsgRequestShownContents.class);

            //Check expected contents
            assertEquals(testFrameNumber, responseBody.getMaxContentsOnScreen());
            assertTrue(CollectionUtils.isEqualCollection(testContentsMetadata, responseBody.getContents()));

            //Must have sent the user tags to the user manager
            MsgSaveTagsForUser tagsForUser = probeRefs.get(SysKB.USER_MANAGER_KEY)
                    .expectMsgClass(FiniteDuration.apply(1, TimeUnit.SECONDS), MsgSaveTagsForUser.class);
            assertEquals(SAMPLE_USER_NAME, tagsForUser.getUsername());
            assertTrue(CollectionUtils.isEqualCollection(SAMPLE_USER_TAGS, tagsForUser.getTags()));

            //No more messages
            probeRefs.get(SysKB.GUI_MANAGER_KEY).expectNoMsg(FiniteDuration.apply(500, TimeUnit.MILLISECONDS));
            probeRefs.get(SysKB.USER_MANAGER_KEY).expectNoMsg(FiniteDuration.apply(500, TimeUnit.MILLISECONDS));
            probeRefs.get(SysKB.CONTENT_MANAGER_KEY).expectNoMsg(FiniteDuration.apply(500, TimeUnit.MILLISECONDS));
        }

        for (Content testContent : testContents) {
            LocalUserIncomingPullRequest imageRequest = new LocalUserIncomingPullRequest();
            imageRequest.setContentId(testContent.getId());
            imageRequest.setUserId(userDataBundle.getUserId());

            //Fetch thumbnail and splash screen
            try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
                HttpPost httpPost = new HttpPost("http://" + localServerIp + ":" + localServerPort + "/pullthumbnail");
                httpPost.setHeader("Content-Type", "application/json");
                StringEntity xmlEntity = new StringEntity(customGson.toJson(imageRequest));
                httpPost.setEntity(xmlEntity);

                byte[] thumbnailData = httpclient.execute(httpPost, imageResponseHandler);
                assertNotNull(thumbnailData);
                assertNotEquals(0, thumbnailData.length);

                //Must have asked the Gui Manager for current contents on screen
                probeRefs.get(SysKB.CONTENT_MANAGER_KEY).expectMsgClass(Duration.Zero(), MsgGetContentToSpread.class);

                //No more messages
                probeRefs.get(SysKB.GUI_MANAGER_KEY).expectNoMsg(FiniteDuration.apply(300, TimeUnit.MILLISECONDS));
                probeRefs.get(SysKB.USER_MANAGER_KEY).expectNoMsg(FiniteDuration.apply(300, TimeUnit.MILLISECONDS));
                probeRefs.get(SysKB.CONTENT_MANAGER_KEY).expectNoMsg(FiniteDuration.apply(300, TimeUnit.MILLISECONDS));
            }

            //Fetch thumbnail and splash screen
            try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
                HttpPost httpPost = new HttpPost("http://" + localServerIp + ":" + localServerPort + "/pullsplash");
                httpPost.setHeader("Content-Type", "application/json");
                StringEntity xmlEntity = new StringEntity(customGson.toJson(imageRequest));
                httpPost.setEntity(xmlEntity);

                byte[] splashData = httpclient.execute(httpPost, imageResponseHandler);
                assertNotNull(splashData);
                assertNotEquals(0, splashData.length);

                //Must have asked the Gui Manager for current contents on screen
                probeRefs.get(SysKB.CONTENT_MANAGER_KEY).expectMsgClass(Duration.Zero(), MsgGetContentToSpread.class);

                //No more messages
                probeRefs.get(SysKB.GUI_MANAGER_KEY).expectNoMsg(FiniteDuration.apply(300, TimeUnit.MILLISECONDS));
                probeRefs.get(SysKB.USER_MANAGER_KEY).expectNoMsg(FiniteDuration.apply(300, TimeUnit.MILLISECONDS));
                probeRefs.get(SysKB.CONTENT_MANAGER_KEY).expectNoMsg(FiniteDuration.apply(300, TimeUnit.MILLISECONDS));
            }
        }
    }

    private void testSpreadingFromAppToTotem(String localServerIp,
                                             int localServerPort,
                                             Map<String, TestKit> probeRefs,
                                             List<Content> testContents) throws IOException {

        //Set date time format
        TimeZone tz = TimeZone.getTimeZone("UTC");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setTimeZone(tz);

        //Create content metadata from Content
        List<ContentMetadataWithoutThumbnailAndSplash> testContentsMetadata = testContents.stream().map((c) -> {
            ContentMetadataWithoutThumbnailAndSplash metaData = new ContentMetadataWithoutThumbnailAndSplash();
            metaData.setContentId(c.getId());
            metaData.setUrl(c.getUrl());
            metaData.setExpirationDate(df.format(c.getExpirationDate().getTime()));
            metaData.setProviderId(c.getProviderId());
            metaData.setTag(c.getTags());

            return metaData;
        }).collect(Collectors.toList());

        //The content manager will reply with the MsgSpreadContentIds
        probeRefs.get(SysKB.CONTENT_MANAGER_KEY).setAutoPilot(new TestActor.AutoPilot() {
            @Override
            public TestActor.AutoPilot run(ActorRef actorRef, Object o) {
                if (o instanceof MsgAppContentList) {
                    MsgAppContentList msg = (MsgAppContentList) o;
                    actorRef.tell(
                            new MsgSpreadContentIds(new ArrayList<>(),
                                    testContents.stream().map(Content::getId).collect(Collectors.toList()),
                                    msg.getToken()),
                            actorRef);
                }
                return this;
            }
        });

        //Deserialize response from the HTTP json body
        ResponseHandler<ResponseToUserSpread> responseToUserSpreadResponseHandler = response -> {
            int status = response.getStatusLine().getStatusCode();
            assertTrue(status >= 200 && status < 300);

            return customGson.fromJson(EntityUtils.toString(response.getEntity()), ResponseToUserSpread.class);
        };

        ResponseHandler<Boolean> responseToSentContent = response -> response.getStatusLine().getStatusCode() == 200;

        //Execute the HTTP interaction
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost("http://" + localServerIp + ":" + localServerPort + "/spread");
            httpPost.setHeader("Content-Type", "application/json");
            LocalUserIncomingSpreadBundle spreadBundle = new LocalUserIncomingSpreadBundle();
            spreadBundle.setContents(testContentsMetadata);
            spreadBundle.setUserId(SAMPLE_USER_NAME);
            StringEntity xmlEntity = new StringEntity(customGson.toJson(spreadBundle));
            httpPost.setEntity(xmlEntity);

            ResponseToUserSpread responseBody = httpclient.execute(httpPost, responseToUserSpreadResponseHandler);
            assertNotNull(responseBody);

            //Must have sent the Content Manager the list of ifs
            probeRefs.get(SysKB.CONTENT_MANAGER_KEY).expectMsgClass(Duration.Zero(), MsgAppContentList.class);

            //Check expected contents
            assertTrue(CollectionUtils.isEqualCollection(testContents.stream().map(Content::getId).collect(Collectors.toList()), responseBody.getToBeSentToTotem()));
            assertTrue(responseBody.getToBeSentToApp().isEmpty());
        }

        for (Content testContent : testContents) {
            try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
                HttpPost httpPost = new HttpPost("http://" + localServerIp + ":" + localServerPort + "/push");

                FileBody content = new FileBody(fetchContentAsFile(testContent.getUrl()));

                ContentMetadataWithoutThumbnailAndSplash data = new ContentMetadataWithoutThumbnailAndSplash();
                data.setUrl(testContent.getUrl());
                data.setContentId(testContent.getId());
                data.setProviderId(testContent.getProviderId());
                data.setExpirationDate(df.format(testContent.getExpirationDate().getTime()));
                data.setRev(testContent.getRevNumber());
                StringBody metadata = new StringBody(customGson.toJson(data), ContentType.APPLICATION_JSON);

                httpPost.setEntity(MultipartEntityBuilder.create()
                        .addPart("contentData", content)
                        .addPart("contentMetadata", metadata)
                        .build());

                assertTrue(httpClient.execute(httpPost, responseToSentContent));

                probeRefs.get(SysKB.CONTENT_MANAGER_KEY).expectMsgClass(Duration.Zero(), MsgSaveContent.class);
            }
        }
    }

    private void testSpreadingFromTotemToApp(String localServerIp,
                                             int localServerPort,
                                             Map<String, TestKit> probeRefs,
                                             List<Content> testContents) throws IOException {
        //Set date time format
        TimeZone tz = TimeZone.getTimeZone("UTC");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setTimeZone(tz);

        //Create content metadata from Content
        List<ContentMetadataWithoutThumbnailAndSplash> testContentsMetadata = testContents.stream().map((c) -> {
            ContentMetadataWithoutThumbnailAndSplash metaData = new ContentMetadataWithoutThumbnailAndSplash();
            metaData.setContentId(c.getId());
            metaData.setUrl(c.getUrl());
            metaData.setExpirationDate(df.format(c.getExpirationDate().getTime()));
            metaData.setProviderId(c.getProviderId());
            metaData.setTag(c.getTags());

            return metaData;
        }).collect(Collectors.toList());

        //The content manager will reply with the MsgSpreadContentIds
        probeRefs.get(SysKB.CONTENT_MANAGER_KEY).setAutoPilot(new TestActor.AutoPilot() {
            @Override
            public TestActor.AutoPilot run(ActorRef actorRef, Object o) {
                if (o instanceof MsgAppContentList) {
                    MsgAppContentList msg = (MsgAppContentList) o;
                    actorRef.tell(
                            new MsgSpreadContentIds(
                                    testContents.stream().map(Content::getId).collect(Collectors.toList()),
                                    new ArrayList<>(),
                                    msg.getToken()),
                            actorRef);
                } else if(o instanceof MsgGetContentToSpread) {
                    MsgGetContentToSpread msg = (MsgGetContentToSpread) o;
                    actorRef.tell(
                            new MsgSendContentToSpread(
                                    testContents.stream().filter((c) -> c.getId() == msg.getId()).findFirst().get(),
                                    msg.getToken()
                            ),
                            actorRef);
                }
                return this;
            }
        });

        //Deserialize response from the HTTP json body
        ResponseHandler<ResponseToUserSpread> responseToUserSpreadResponseHandler = response -> {
            int status = response.getStatusLine().getStatusCode();
            assertTrue(status >= 200 && status < 300);

            return customGson.fromJson(EntityUtils.toString(response.getEntity()), ResponseToUserSpread.class);
        };

        ResponseHandler<ContentMetadataWithoutThumbnailAndSplash> responseToReceivedMetadata = response ->  {
            int status = response.getStatusLine().getStatusCode();
            assertTrue(status >= 200 && status < 300);

            return customGson.fromJson(EntityUtils.toString(response.getEntity()), ContentMetadataWithoutThumbnailAndSplash.class);
        };

        ResponseHandler<Boolean> responseToReceivedContent = response -> response.getStatusLine().getStatusCode() == 200;

        //Execute the HTTP interaction
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost("http://" + localServerIp + ":" + localServerPort + "/spread");
            httpPost.setHeader("Content-Type", "application/json");
            LocalUserIncomingSpreadBundle spreadBundle = new LocalUserIncomingSpreadBundle();
            spreadBundle.setContents(testContentsMetadata);
            spreadBundle.setUserId(SAMPLE_USER_NAME);
            StringEntity xmlEntity = new StringEntity(customGson.toJson(spreadBundle));
            httpPost.setEntity(xmlEntity);

            ResponseToUserSpread responseBody = httpclient.execute(httpPost, responseToUserSpreadResponseHandler);
            assertNotNull(responseBody);

            //Must have sent the Content Manager the list of ifs
            probeRefs.get(SysKB.CONTENT_MANAGER_KEY).expectMsgClass(Duration.Zero(), MsgAppContentList.class);

            //Check expected contents
            assertTrue(CollectionUtils.isEqualCollection(testContents.stream().map(Content::getId).collect(Collectors.toList()), responseBody.getToBeSentToApp()));
            assertTrue(responseBody.getToBeSentToTotem().isEmpty());
        }

        for (Content testContent : testContents) {
            try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
                HttpPost httpPost = new HttpPost("http://" + localServerIp + ":" + localServerPort + "/pullmetadata");
                httpPost.setHeader("Content-Type", "application/json");
                LocalUserIncomingPullRequest pullRequest = new LocalUserIncomingPullRequest();
                pullRequest.setContentId(testContent.getId());
                pullRequest.setUserId(SAMPLE_USER_NAME);
                StringEntity xmlEntity = new StringEntity(customGson.toJson(pullRequest));
                httpPost.setEntity(xmlEntity);

                ContentMetadataWithoutThumbnailAndSplash responseBody = httpClient.execute(httpPost, responseToReceivedMetadata);
                assertNotNull(responseBody);

                probeRefs.get(SysKB.CONTENT_MANAGER_KEY).expectMsg(Duration.Zero(), MsgGetContentToSpread.class);
            }

            try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
                HttpPost httpPost = new HttpPost("http://" + localServerIp + ":" + localServerPort + "/pull");
                httpPost.setHeader("Content-Type", "application/json");
                LocalUserIncomingPullRequest pullRequest = new LocalUserIncomingPullRequest();
                pullRequest.setContentId(testContent.getId());
                pullRequest.setUserId(SAMPLE_USER_NAME);
                StringEntity xmlEntity = new StringEntity(customGson.toJson(pullRequest));
                httpPost.setEntity(xmlEntity);

                assertTrue(httpClient.execute(httpPost, responseToReceivedContent));

                probeRefs.get(SysKB.CONTENT_MANAGER_KEY).expectMsg(Duration.Zero(), MsgGetContentToSpread.class);
            }
        }


    }

    private static File fetchContentAsFile(String address) throws IOException {
        URL url = new URL(address);
        URLConnection con = url.openConnection();
        InputStream inputStream = con.getInputStream();
        Random r = new Random();

        File outputFile;
        do {
            outputFile = new File(SysKB.TEMP_CONTENT_DIR, + r.nextInt(Integer.MAX_VALUE) + ".zip");
            outputFile.getParentFile().mkdirs();
        } while (!outputFile.createNewFile());

        FileOutputStream outputStream = new FileOutputStream(outputFile);
        Utilities.copyStream(inputStream, outputStream);
        inputStream.close();
        outputStream.close();

        return outputFile;
    }

    @Override
    public Class<? extends UntypedActor> getTestActorClass() {
        return LocalCommunicationManager.class;
    }

    private static final Gson customGson = new GsonBuilder().registerTypeHierarchyAdapter(byte[].class,
            new ByteArrayToBase64TypeAdapter()).create();

    // Using Android's base64 libraries. This can be replaced with any base64 library.
    private static class ByteArrayToBase64TypeAdapter implements JsonSerializer<byte[]>, JsonDeserializer<byte[]> {
        public byte[] deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return Base64.getDecoder().decode(json.getAsString());
        }

        public JsonElement serialize(byte[] src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(Base64.getEncoder().encodeToString(src));
        }
    }

    private static Content createSampleContent() throws Exception {
        ContentMetaData metaData = SwaggerClient.getContentWithId("" + SAMPLE_CONTENT_ID);
        //ZipFile zipFile = FTPClient.fetchContent(metaData.getUrl());
        //ContentBundle bundle = new ContentBundleImpl(Integer.parseInt(metaData.getId()), zipFile);
        ContentBundle bundle = new ContentBundleImpl(Integer.parseInt(metaData.getId()), FTPClient.fetchContent(metaData.getUrl()));
        return new Content(
                Integer.parseInt(metaData.getId()),
                metaData.getProviderId(),
                new HashSet<>(metaData.getTag()),
                metaData.getUrl(),
                metaData.getExpirationDate().toCalendar(Locale.ITALIAN),
                metaData.getRev(),
                bundle);
    }

    private static byte[] extractBytes(BufferedImage image) {
        ByteArrayOutputStream bos = null;
        try {
            bos = new ByteArrayOutputStream();
            ImageIO.write(image, "png", bos);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bos != null)
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return bos.toByteArray();
    }
}
