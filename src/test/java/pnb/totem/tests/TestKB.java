package pnb.totem.tests;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.javadsl.TestKit;
import pnb.totem.messages.MsgActorsRef;
import pnb.totem.utilities.SysKB;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Biagini on 18/04/2017.
 */
public class TestKB {
    public static Map<String, TestKit> createActorsMap(ActorSystem system) {
        Map<String, TestKit> refs = new HashMap<>();
        refs.put(SysKB.INTERNET_MANAGER_KEY, new TestKit(system));
        refs.put(SysKB.CONTENT_MANAGER_KEY, new TestKit(system));
        refs.put(SysKB.GUI_MANAGER_KEY, new TestKit(system));
        refs.put(SysKB.USER_MANAGER_KEY, new TestKit(system));
        refs.put(SysKB.LOCAL_COMMUNICATION_MANAGER_KEY, new TestKit(system));
        return refs;
    }

    public static ActorRef createActorForTest(ActorSystem system, Class actorClass, Map<String, TestKit> probeRefs) {
        final ActorRef actor = system.actorOf(Props.create(actorClass), "testedactor");

        Map<String, ActorRef> refs = new HashMap<>();
        for (Map.Entry<String, TestKit> entry : probeRefs.entrySet()) {
            refs.put(entry.getKey(), entry.getValue().getRef());
        }

        actor.tell(new MsgActorsRef(refs), ActorRef.noSender());
        return actor;
    }
}
