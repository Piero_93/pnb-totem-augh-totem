package pnb.totem.tests;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.UntypedActor;
import akka.testkit.javadsl.TestKit;
import com.rabbitmq.client.ConnectionFactory;
import io.swagger.client.model.Position;
import io.swagger.client.model.RegistrationErrors;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;
import org.scalatest.junit.JUnitSuite;
import pnb.totem.internetcommunication.ConnectionException;
import pnb.totem.internetcommunication.SwaggerClient;
import pnb.totem.utilities.SysKB;
import pnb.totem.utilities.Utilities;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeoutException;

import static pnb.totem.utilities.SysKB.HOST;

/**
 * Created by Biagini on 18/04/2017.
 */
public abstract class AbstractActorTester extends JUnitSuite {
    protected final static String SAMPLE_USER_NAME = "username";
    protected final static Position SAMPLE_POSITION = new Position();
    protected final static String SAMPLE_TOTEM_NAME = "totem";
    protected final static String SAMPLE_TAG = "testTag";
    protected final static int SAMPLE_CONTENT_ID = 1;
    protected final static int SAMPLE_NOT_VALID_CONTENT_ID = 0;
    protected final static Set<String> SAMPLE_USER_TAGS = new HashSet<>(Collections.singletonList(SAMPLE_TAG));

    private static String oldUsername;
    private static String oldPassword;
    private static int oldFrameNumber;
    private static int oldRefreshTime;

    protected static ActorSystem system;

    static {
        SAMPLE_POSITION.setLatitude(0.0);
        SAMPLE_POSITION.setLongitude(0.0);
    }

    @BeforeClass
    public static void setup() {
        system = ActorSystem.create();

        try {
            oldUsername = Utilities.readUsernameFromFile();
            oldPassword = Utilities.readPasswordFromFile();
            oldFrameNumber = Utilities.readFrameNumberFromFile();
            oldRefreshTime = Utilities.readRefreshTimeFromFile();
        } catch (IOException ignored) {
        }

        try {
            FileUtils.deleteDirectory(new File(SysKB.PATH_PREFIX));
            FileUtils.deleteDirectory(new File(SysKB.GUI_CONTENT_DIR));
            FileUtils.deleteDirectory(new File(SysKB.TEMP_CONTENT_DIR));
            Optional<RegistrationErrors> errors = Optional.ofNullable(
                    SwaggerClient.registerNewUser(SAMPLE_USER_NAME, SAMPLE_USER_NAME));
            Assume.assumeFalse(errors.map(err ->
                err.getTooShortPassword() || err.getTooShortUsername()
            ).orElse(false));

            Assume.assumeTrue(SwaggerClient.loginUser(SAMPLE_USER_NAME, SAMPLE_USER_NAME) == 200);
            SwaggerClient.setMyTags(SAMPLE_USER_NAME, SAMPLE_USER_TAGS);

            errors = SwaggerClient.registerNewTotem(SAMPLE_TOTEM_NAME, SAMPLE_TOTEM_NAME, 1, SAMPLE_POSITION);
            Assume.assumeFalse(errors.map(err ->
                    err.getTooShortPassword() || err.getTooShortUsername()
            ).orElse(false));

            Assume.assumeTrue(SwaggerClient.loginTotem(SAMPLE_TOTEM_NAME, SAMPLE_TOTEM_NAME));

            Assume.assumeTrue(SwaggerClient.getIdsForTag(SAMPLE_TAG, 10).size() == 0);

            SwaggerClient.getContentWithId(SAMPLE_CONTENT_ID + "");
            SwaggerClient.getContentWithId(SAMPLE_NOT_VALID_CONTENT_ID + "");

            Utilities.readIpFromFile();//Prerequisito test local communication
            Utilities.readPortFromFile();//Prerequisito test local communication
            
            Utilities.changeRefreshTime(20);
            Utilities.changeFrameNumber(1);
            
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(HOST);
            factory.setConnectionTimeout(1000);
            factory.newConnection().close();
        } catch (IOException | ConnectionException | TimeoutException e) {
            e.printStackTrace();
            System.out.println("Error while initializing test.");
            Assume.assumeNoException(e);
        }
    }

    @AfterClass
    public static void teardown() {
        TestKit.shutdownActorSystem(system);
        system = null;

        try {
            Utilities.changeUserPw(oldUsername, oldPassword);
            Utilities.changeFrameNumber(oldFrameNumber);
            Utilities.changeRefreshTime(oldRefreshTime);
        } catch (IOException ignored) {
        }
    }

    @Test
    public final void testFramework() throws Exception {
        new TestKit(system) {
            {
                Map<String, TestKit> probeRefs = TestKB.createActorsMap(system);
                ActorRef actor = TestKB.createActorForTest(system, getTestActorClass(), probeRefs);

                test(actor, probeRefs);
            }
        };
    }

    public abstract void test(ActorRef actor, Map<String, TestKit> probeRefs) throws Exception;

    public abstract Class<? extends UntypedActor> getTestActorClass();
}
