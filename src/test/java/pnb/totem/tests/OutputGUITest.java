package pnb.totem.tests;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.testkit.javadsl.TestKit;
import io.swagger.client.model.ContentMetaData;
import org.apache.commons.collections4.CollectionUtils;
import pnb.totem.actors.OutputGUI;
import pnb.totem.internetcommunication.FTPClient;
import pnb.totem.internetcommunication.SwaggerClient;
import pnb.totem.messages.*;
import pnb.totem.model.Content;
import pnb.totem.model.ContentBundle;
import pnb.totem.model.ContentBundleImpl;
import pnb.totem.utilities.Utilities;

import java.util.*;


import static akka.testkit.JavaTestKit.duration;
import static pnb.totem.utilities.SysKB.*;
import static org.junit.Assert.*;

/**
 * Created by Marco on 19/04/2017.
 */
public class OutputGUITest extends AbstractActorTester {
    @Override
    public void test(ActorRef actor, Map<String, TestKit> probeRefs) throws Exception {
        Set<String> set = new HashSet<>();
        set.add("cibo");
        {
            //first TimeoutFrame Control
            MsgFetchTopTags msgCheckContentsForTags = probeRefs.get(USER_MANAGER_KEY).expectMsgClass(duration("20 seconds"), MsgFetchTopTags.class);
            assertTrue(msgCheckContentsForTags.getNotToTake().isEmpty());
            //MsgRanking message arrives
            actor.tell(new MsgRanking(set), ActorRef.noSender());
            MsgRequestSingleContentForTag msgRequestSingleContentForTag = probeRefs.get(CONTENT_MANAGER_KEY).expectMsgClass(duration("20 seconds"),MsgRequestSingleContentForTag.class);
            assertTrue(msgRequestSingleContentForTag.getTag().equals("cibo"));
            //MsgResponseSingleContentForTag message arrives
            ContentMetaData metaData = SwaggerClient.getContentWithId("" + SAMPLE_CONTENT_ID);
            ContentBundle bundle = null;
            try {
                //ZipFile zipFile = FTPClient.fetchContent(metaData.getUrl());
                //bundle = new ContentBundleImpl(Integer.parseInt(metaData.getId()), zipFile);
                bundle = new ContentBundleImpl(Integer.parseInt(metaData.getId()), FTPClient.fetchContent(metaData.getUrl()));
            } catch (Exception e) { //Zip package not valid or not found on the server
            }
            if(bundle != null) {
                System.out.println(metaData);
                Content content = new Content(
                        Integer.parseInt(metaData.getId()),
                        metaData.getProviderId(),
                        new HashSet<>(metaData.getTag()),
                        metaData.getUrl(),
                        metaData.getExpirationDate().toCalendar(Locale.ITALIAN),
                        metaData.getRev(),
                        bundle);
                actor.tell(new MsgResponseSingleContentForTag(content), ActorRef.noSender());

                //MsgRequestShownContents message arrives
                List<Content> list = new ArrayList<>();
                list.add(content);
                actor.tell(new MsgRequestShownContents(), ActorRef.noSender());
                MsgResponseShownContents msgResponseShownContents = probeRefs.get(LOCAL_COMMUNICATION_MANAGER_KEY).expectMsgClass(duration("5 seconds"), MsgResponseShownContents.class);
                assertTrue(CollectionUtils.isEqualCollection(msgResponseShownContents.getCurrentShownContents(), list));
                assertTrue(msgResponseShownContents.getTotal() == Utilities.readFrameNumberFromFile());

                //second TimeoutFrame Control
                msgCheckContentsForTags = probeRefs.get(USER_MANAGER_KEY).expectMsgClass(duration("20 seconds"), MsgFetchTopTags.class);
                assertTrue(msgCheckContentsForTags.getNotToTake().isEmpty());
                //MsgRanking message arrives EMPTY
                actor.tell(new MsgRanking(set), ActorRef.noSender());
                msgRequestSingleContentForTag = probeRefs.get(CONTENT_MANAGER_KEY).expectMsgClass(duration("20 seconds"), MsgRequestSingleContentForTag.class);
                assertTrue(msgRequestSingleContentForTag.getTag().equals("cibo"));
                //MsgContentNotPresent arrives
                actor.tell(new MsgContentNotPresent(msgRequestSingleContentForTag.getTag()), ActorRef.noSender());
                //MsgRequestShownContents message arrives
                actor.tell(new MsgRequestShownContents(), ActorRef.noSender());
                msgResponseShownContents = probeRefs.get(LOCAL_COMMUNICATION_MANAGER_KEY).expectMsgClass(duration("5 seconds"), MsgResponseShownContents.class);
                assertTrue(CollectionUtils.isEqualCollection(msgResponseShownContents.getCurrentShownContents(), list));
                assertTrue(msgResponseShownContents.getTotal() == Utilities.readFrameNumberFromFile());
            }

        }
    }

    @Override
    public Class<? extends UntypedActor> getTestActorClass() {
        return OutputGUI.class;
    }
}
