package pnb.totem.server.tests;

import io.swagger.client.model.ContentMetaData;
import io.swagger.client.model.ContentRegistrationData;
import io.swagger.client.model.Position;
import io.swagger.client.model.RegistrationErrors;
import org.apache.commons.collections4.CollectionUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import pnb.totem.internetcommunication.ConnectionException;
import pnb.totem.internetcommunication.SwaggerClient;

import java.sql.*;
import java.util.*;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assume.*;
import static org.junit.Assert.*;


/**
 * Created by Marco on 19/04/2017.
 */
public class ServerProviderTests {

    protected final static String EMPTY_TAG = "testTag";
    protected final static String USED_TAG = "territorio";
    protected final static String SAMPLE_PROVIDER = "provider";
    protected final static String SAMPLE_URL = "url";
    protected final static String SAMPLE_USER_NAME = "username";
    protected final static String SAMPLE_NOT_VALID_USER_NAME = "ImNotValid";
    protected final static Set<String> SAMPLE_USER_TAGS = new HashSet<>(Arrays.asList(EMPTY_TAG, USED_TAG));

    private Random r;
    private int contentId = -1;
    @Before
    public void before() {
        try {
            Assume.assumeTrue(SwaggerClient.loginUser(SAMPLE_USER_NAME, SAMPLE_USER_NAME) == 200);
            SwaggerClient.setMyTags(SAMPLE_USER_NAME, SAMPLE_USER_TAGS);
        } catch (ConnectionException e) {
            assumeTrue("Error while registering the user", false);
        }

        assumeTrue("Server is offline or sample provider does not exist", SwaggerClient.loginProvider(SAMPLE_PROVIDER, SAMPLE_PROVIDER));
        r = new Random(System.nanoTime());
    }

    @After
    public void tearDown() throws Exception {
        if(contentId > -1){
            SwaggerClient.deleteContentWithId(contentId+"");
            try {
                SwaggerClient.getContentWithId(contentId + "");
            } catch (ConnectionException e){
                assertTrue("Content not eliminated: "+contentId, e.getErrorCode()==404);
            }
        }
    }

    @Test
    public void testProviderRegistrationAndLogin() throws ConnectionException {
        String randomUsername = "Provider" + r.nextInt();
        int i = 0;
        while (SwaggerClient.loginProvider(randomUsername, randomUsername) && (i < 10)) {
            randomUsername = "Provider" + r.nextInt();
            i++;
        }
        assertNotEquals("Login procedure is broken", 10, i);

        System.out.println("Testing with username " + randomUsername);

        //Too Short Username
        Optional<RegistrationErrors> expectTooShortUsername = SwaggerClient.registerNewProvider(randomUsername.substring(0, 3), randomUsername);
        assertTrue("Error is not present", expectTooShortUsername.isPresent());
        assertTrue("Wrong error", expectTooShortUsername.get().getTooShortUsername());
        assertFalse("Wrong error", expectTooShortUsername.get().getTooShortPassword());
        assertFalse("Wrong error", expectTooShortUsername.get().getDuplicatedUsername());

        //Too Short Password
        Optional<RegistrationErrors> expectTooShortPassword = SwaggerClient.registerNewProvider(randomUsername, randomUsername.substring(0, 3));
        assertTrue("Error is not present", expectTooShortPassword.isPresent());
        assertFalse("Wrong error", expectTooShortPassword.get().getTooShortUsername());
        assertTrue("Wrong error", expectTooShortPassword.get().getTooShortPassword());
        assertFalse("Wrong error", expectTooShortPassword.get().getDuplicatedUsername());

        //Correct Registration
        Optional<RegistrationErrors> expectNoError = SwaggerClient.registerNewProvider(randomUsername, randomUsername);
        assertFalse("Error while registering a valid user", expectNoError.isPresent());

        //Correct Login
        assertTrue("Valid login not working", SwaggerClient.loginProvider(randomUsername, randomUsername));

        //Duplicated username
        Optional<RegistrationErrors> expectDuplicatedUsername = SwaggerClient.registerNewProvider(randomUsername, randomUsername);
        assertTrue("Error is not present", expectDuplicatedUsername.isPresent());
        assertFalse("Wrong error", expectDuplicatedUsername.get().getTooShortUsername());
        assertFalse("Wrong error", expectDuplicatedUsername.get().getTooShortPassword());
        assertTrue("Wrong error", expectDuplicatedUsername.get().getDuplicatedUsername());
    }

    @Test
    public void testProviderContentsManagement() throws ConnectionException {
        //tests getContentsForProvider
        int id = SwaggerClient.setContentForProvider(SAMPLE_PROVIDER, Arrays.asList(USED_TAG), new DateTime(DateTimeZone.UTC), SAMPLE_URL);
        contentId = id;
        Collection<Integer> idsForProvider = SwaggerClient.getContentsForProvider(SAMPLE_PROVIDER);
        assertTrue("Provider doesn't contain the content "+ id,idsForProvider.contains(id));

        //tests modifyContentWithId
        ContentRegistrationData data = new ContentRegistrationData();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        DateTime newTime = new DateTime(DateTimeZone.UTC);
        data.setTag(Arrays.asList(USED_TAG));
        data.setExpirationDate(newTime);
        data.setUrl(SAMPLE_URL);

        ContentMetaData metaData = SwaggerClient.getContentWithId(id+"");
        assertEquals("Rev number does not start from 0", (Integer) 0, metaData.getRev());

        SwaggerClient.modifyContentWithId(id+"", data);
        metaData = SwaggerClient.getContentWithId(id+"");
        DateTime catchedDate = metaData.getExpirationDate().toDateTime(DateTimeZone.UTC);
        assertTrue("Content not modified: "+id, catchedDate.equals(newTime));
        assertEquals("Rev number was not incremented", (Integer) 1, metaData.getRev());


    }

    private void wrongLogin() throws ConnectionException {
        SwaggerClient.loginUser(SAMPLE_NOT_VALID_USER_NAME, SAMPLE_USER_NAME);
    }
}
