package pnb.totem.server.tests;

import io.swagger.client.model.Position;
import io.swagger.client.model.RegistrationErrors;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;
import pnb.totem.internetcommunication.ConnectionException;
import pnb.totem.internetcommunication.SwaggerClient;

import java.util.*;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

/**
 * Created by Biagini on 19/04/2017.
 */
public class ServerTotemTests {
    protected final static Position SAMPLE_POSITION = new Position();
    protected final static String EMPTY_TAG = "testTag";
    protected final static String USED_TAG = "territorio";
    protected final static String SAMPLE_TOTEM = "totem";
    protected final static int SAMPLE_CONTENT_ID = 1;
    protected final static int SAMPLE_NOT_VALID_CONTENT_ID = 0;
    protected final static int SAMPLE_NOT_EXISTING_CONTENT_ID = -1;
    protected final static String SAMPLE_USER_NAME = "username";
    protected final static String SAMPLE_NOT_VALID_USER_NAME = "u";
    protected final static Set<String> SAMPLE_USER_TAGS = new HashSet<>(Arrays.asList(EMPTY_TAG, USED_TAG));

    private Random r;

    static {
        SAMPLE_POSITION.setLatitude(0.0);
        SAMPLE_POSITION.setLongitude(0.0);
    }

    @Before
    public void before() {
        try {
            Assume.assumeTrue(SwaggerClient.loginUser(SAMPLE_USER_NAME, SAMPLE_USER_NAME) == 200);
            SwaggerClient.setMyTags(SAMPLE_USER_NAME, SAMPLE_USER_TAGS);
            SwaggerClient.setMyPos(SAMPLE_USER_NAME, SAMPLE_POSITION.getLatitude(), SAMPLE_POSITION.getLongitude());
        } catch (ConnectionException e) {
            assumeTrue("Error while checking pre-requisites", false);
        }

        assumeTrue("Server is offline or sample totem does not exist", SwaggerClient.loginTotem(SAMPLE_TOTEM, SAMPLE_TOTEM));
        r = new Random();
    }

    @Test
    public void testTotemRegistrationAndLogin() throws ConnectionException {
        String randomUsername = "Totem" + r.nextInt();
        int i = 0;
        while (SwaggerClient.loginTotem(randomUsername, randomUsername) && (i < 10)) {
            randomUsername = "Totem" + r.nextInt();
            i++;
        }
        assertNotEquals("Login procedure is broken", 10, i);

        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName() + " with username " + randomUsername);

        //Too Short Username
        Optional<RegistrationErrors> expectTooShortUsername = SwaggerClient.registerNewTotem(randomUsername.substring(0, 3), randomUsername, 4, SAMPLE_POSITION);
        assertTrue("Error is not present", expectTooShortUsername.isPresent());
        assertTrue("Wrong error", expectTooShortUsername.get().getTooShortUsername());
        assertFalse("Wrong error", expectTooShortUsername.get().getTooShortPassword());
        assertFalse("Wrong error", expectTooShortUsername.get().getDuplicatedUsername());

        //Too Short Password
        Optional<RegistrationErrors> expectTooShortPassword = SwaggerClient.registerNewTotem(randomUsername, randomUsername.substring(0, 3), 4, SAMPLE_POSITION);
        assertTrue("Error is not present", expectTooShortPassword.isPresent());
        assertFalse("Wrong error", expectTooShortPassword.get().getTooShortUsername());
        assertTrue("Wrong error", expectTooShortPassword.get().getTooShortPassword());
        assertFalse("Wrong error", expectTooShortPassword.get().getDuplicatedUsername());

        //Correct Registration
        Optional<RegistrationErrors> expectNoError = SwaggerClient.registerNewTotem(randomUsername, randomUsername, 4, SAMPLE_POSITION);
        assertFalse("Error while registering a valid user", expectNoError.isPresent());

        //Correct Login
        assertTrue("Valid login not working", SwaggerClient.loginTotem(randomUsername, randomUsername));

        //Duplicated username
        Optional<RegistrationErrors> expectDuplicatedUsername = SwaggerClient.registerNewTotem(randomUsername, randomUsername, 4, SAMPLE_POSITION);
        assertTrue("Error is not present", expectDuplicatedUsername.isPresent());
        assertFalse("Wrong error", expectDuplicatedUsername.get().getTooShortUsername());
        assertFalse("Wrong error", expectDuplicatedUsername.get().getTooShortPassword());
        assertTrue("Wrong error", expectDuplicatedUsername.get().getDuplicatedUsername());
    }

    @Test
    public void testGetIdsForTag() throws ConnectionException {
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());

        final int limit = 5;
        assertEquals("Wrong number of content", 0, SwaggerClient.getIdsForTag(EMPTY_TAG, limit).size());

        final int response = SwaggerClient.getIdsForTag(USED_TAG, limit).size();
        assertTrue("Wrong number of contents", response > 0 && response <= limit);
    }

    @Test
    public void testGetContentWithId() throws ConnectionException {
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());

        SwaggerClient.getContentWithId(SAMPLE_CONTENT_ID + "");

        try {
            SwaggerClient.getContentWithId(SAMPLE_NOT_EXISTING_CONTENT_ID + "");
            assertTrue("Missing expected exception", false);
        } catch (ConnectionException e) {
            assertEquals("Wrong error", 404, e.getErrorCode());
        }
    }

    @Test
    public void testGetTagsForUser() throws ConnectionException {
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());

        assertTrue("Wrong response", CollectionUtils.isEqualCollection(SwaggerClient.getTagsForUser(SAMPLE_USER_NAME), SAMPLE_USER_TAGS));

        try {
            SwaggerClient.getTagsForUser(SAMPLE_NOT_VALID_USER_NAME);
            assertTrue("Missing expected exception", false);
        } catch (ConnectionException e) {
            assertEquals("Wrong error", 404, e.getErrorCode());
        }

        wrongLogin();
        try {
            SwaggerClient.getTagsForUser(SAMPLE_USER_NAME);
            assertTrue("Missing expected exception", false);
        } catch (ConnectionException e) {
            assertEquals("Wrong error", 401, e.getErrorCode());
        }
    }

    @Test
    public void testGetPositionForUser() throws ConnectionException {
        System.out.println(Thread.currentThread().getStackTrace()[1].getMethodName());

        assertEquals("Wrong response", SAMPLE_POSITION, SwaggerClient.getPositionForUser(SAMPLE_USER_NAME));

        try {
            SwaggerClient.getPositionForUser(SAMPLE_NOT_VALID_USER_NAME);
        } catch (ConnectionException e) {
            assertEquals("Wrong error", 404, e.getErrorCode());
        }

        wrongLogin();
        try {
            SwaggerClient.getPositionForUser(SAMPLE_USER_NAME);
            assertTrue("Missing expected exception", false);
        } catch (ConnectionException e) {
            assertEquals("Wrong error", 401, e.getErrorCode());
        }
    }

    private void wrongLogin() throws ConnectionException {
        SwaggerClient.loginUser(SAMPLE_NOT_VALID_USER_NAME, SAMPLE_USER_NAME);
    }
}
