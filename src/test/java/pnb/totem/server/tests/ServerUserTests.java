package pnb.totem.server.tests;

import io.swagger.client.model.Position;
import io.swagger.client.model.RegistrationErrors;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import pnb.totem.internetcommunication.ConnectionException;
import pnb.totem.internetcommunication.SwaggerClient;

import java.util.*;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.*;
import static org.junit.Assume.assumeTrue;

/**
 * Created by Lorenzo on 19/04/2017.
 */
public class ServerUserTests {

    protected final static String EMPTY_TAG = "testTag";
    protected final static String USED_TAG = "territorio";
    protected final static String SAMPLE_USER_NAME = "testUser1";
    protected final static String SAMPLE_USER_NAME_OTHER = "testUser2";
    protected final static Set<String> SAMPLE_USER_TAGS = new HashSet<>(Arrays.asList(EMPTY_TAG, USED_TAG));

    private Random r;

    @Before
    public void before() {
        try {
            Optional<RegistrationErrors> regErrors = Optional.ofNullable(
                    SwaggerClient.registerNewUser(SAMPLE_USER_NAME, SAMPLE_USER_NAME));
            regErrors.ifPresent(err -> {
                assertTrue(err.getDuplicatedUsername());
                assertFalse(err.getTooShortPassword());
                assertFalse(err.getTooShortUsername());
            });

            regErrors = Optional.ofNullable(
                    SwaggerClient.registerNewUser(SAMPLE_USER_NAME_OTHER, SAMPLE_USER_NAME_OTHER));
            regErrors.ifPresent(err -> {
                assertTrue(err.getDuplicatedUsername());
                assertFalse(err.getTooShortPassword());
                assertFalse(err.getTooShortUsername());
            });
        } catch (ConnectionException e) {
            assumeTrue("Error while registering the user", false);
        }

        assumeTrue(SwaggerClient.loginUser(SAMPLE_USER_NAME, SAMPLE_USER_NAME) == 200);

        r = new Random();
    }

    @Test
    public void testUserRegistrationAndLogin() throws ConnectionException {
        String randomUsername = "User" + r.nextInt();
        int i = -1;
        int response = 200;
        while (response >= 200 && response < 300 && (i < 10)) {
            response = SwaggerClient.loginUser(randomUsername, randomUsername);
            randomUsername = "User" + r.nextInt();
            i++;
        }

        assertNotEquals("Login procedure is broken", 10, i);

        System.out.println("Testing with username " + randomUsername);

        //Too Short Username
        Optional<RegistrationErrors> expectTooShortUsername = Optional.ofNullable(
                SwaggerClient.registerNewUser(randomUsername.substring(0, 3), randomUsername));

        assertTrue("Error is not present", expectTooShortUsername.isPresent());
        assertTrue("Wrong error", expectTooShortUsername.get().getTooShortUsername());
        assertFalse("Wrong error", expectTooShortUsername.get().getTooShortPassword());
        assertFalse("Wrong error", expectTooShortUsername.get().getDuplicatedUsername());

        //Too Short Password
        Optional<RegistrationErrors> expectTooShortPassword = Optional.ofNullable(
                SwaggerClient.registerNewUser(randomUsername, randomUsername.substring(0, 3)));
        assertTrue("Error is not present", expectTooShortPassword.isPresent());
        assertFalse("Wrong error", expectTooShortPassword.get().getTooShortUsername());
        assertTrue("Wrong error", expectTooShortPassword.get().getTooShortPassword());
        assertFalse("Wrong error", expectTooShortPassword.get().getDuplicatedUsername());

        //Correct Registration
        Optional<RegistrationErrors> expectNoError = Optional.ofNullable(
                SwaggerClient.registerNewUser(randomUsername, randomUsername));
        assertFalse("Error while registering a valid user", expectNoError.isPresent());

        //Correct Login
        response = SwaggerClient.loginUser(randomUsername, randomUsername);
        assertEquals("Valid login not working", 200, response);

        //Duplicated username
        Optional<RegistrationErrors> expectDuplicatedUsername = Optional.ofNullable(
                SwaggerClient.registerNewUser(randomUsername, randomUsername));
        assertTrue("Error is not present", expectDuplicatedUsername.isPresent());
        assertFalse("Wrong error", expectDuplicatedUsername.get().getTooShortUsername());
        assertFalse("Wrong error", expectDuplicatedUsername.get().getTooShortPassword());
        assertTrue("Wrong error", expectDuplicatedUsername.get().getDuplicatedUsername());
    }

    @Test
    public void testUserGetAndSetInterests() throws ConnectionException {
        SwaggerClient.setMyTags(SAMPLE_USER_NAME, SAMPLE_USER_TAGS);

        Set<String> fetchedTags = SwaggerClient.getTagsForUser(SAMPLE_USER_NAME);
        assertTrue("Different set of tags", CollectionUtils.isEqualCollection(SAMPLE_USER_TAGS, fetchedTags));

        //Test twice
        fetchedTags = SwaggerClient.getTagsForUser(SAMPLE_USER_NAME);
        assertTrue("Different set of tags", CollectionUtils.isEqualCollection(SAMPLE_USER_TAGS, fetchedTags));

        SwaggerClient.setMyTags(SAMPLE_USER_NAME, Collections.emptySet());

        fetchedTags = SwaggerClient.getTagsForUser(SAMPLE_USER_NAME);
        assertTrue("Different set of tags", CollectionUtils.isEqualCollection(Collections.emptySet(), fetchedTags));

        //Try inserting a bad tag
        String badTag = "BadTag" + r.nextInt();

        try {
            SwaggerClient.setMyTags(SAMPLE_USER_NAME, Collections.singleton(badTag));
            fail("Server accepts a list containing a bad tag");
        } catch (ConnectionException e) {
            assertEquals("Wrong HTTP status code for wrong tag", 422, e.getErrorCode());
        }

        SwaggerClient.setMyTags(SAMPLE_USER_NAME, SAMPLE_USER_TAGS);

        fetchedTags = SwaggerClient.getTagsForUser(SAMPLE_USER_NAME);
        assertTrue("Different set of tags", CollectionUtils.isEqualCollection(SAMPLE_USER_TAGS, fetchedTags));
    }

    @Test
    public void testUserGetAndSetPosition() throws ConnectionException {
        double latitude = 4.0;
        double longitude = 8.0;
        SwaggerClient.setMyPos(SAMPLE_USER_NAME, latitude, longitude);

        Position p = SwaggerClient.getPositionForUser(SAMPLE_USER_NAME);

        assertEquals("Wrong latitude", latitude, p.getLatitude(), 0.001);
        assertEquals("Wrong longitude", longitude, p.getLongitude(), 0.001);

        latitude = 4.1;
        longitude = 8.0;
        SwaggerClient.setMyPos(SAMPLE_USER_NAME, latitude, longitude);

        p = SwaggerClient.getPositionForUser(SAMPLE_USER_NAME);

        assertEquals("Wrong latitude", latitude, p.getLatitude(), 0.001);
        assertEquals("Wrong longitude", longitude, p.getLongitude(), 0.001);
    }

    @Test
    public void testBadLogin() throws ConnectionException {
        int result = SwaggerClient.loginUser(SAMPLE_USER_NAME, SAMPLE_USER_NAME + "wrong");
        assertTrue("Should fail login", result >= 300 || result < 200);
    }

    @Test
    public void testBadGetInterests() throws ConnectionException {
        //Try getting the interests of another user
        assertEquals("Should login succesfully", 200, SwaggerClient.loginUser(SAMPLE_USER_NAME_OTHER, SAMPLE_USER_NAME_OTHER));
        try {
            SwaggerClient.getTagsForUser(SAMPLE_USER_NAME);
            fail("Should fail");
        } catch (ConnectionException e) {
            assertEquals("Wrong HTTP status code", 401, e.getErrorCode());
        }
    }

    @Test
    public void testBadGetPosition() throws ConnectionException {
        //Try getting the position of another user
        assertEquals("Should login succesfully", 200, SwaggerClient.loginUser(SAMPLE_USER_NAME_OTHER, SAMPLE_USER_NAME_OTHER));
        try {
            SwaggerClient.getPositionForUser(SAMPLE_USER_NAME);
            fail("Should fail");
        } catch (ConnectionException e) {
            assertEquals("Wrong HTTP status code", 401, e.getErrorCode());
        }
    }
}
