package pnb.mock.localadvertiser;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * LocalAdvertiser.
 */
public class LocalAdvertiser {
    private static final String ADDRESS = "127.0.0.1:8080";
    private static final String PATH = "";

    private static HttpClient httpClient;

    private static void send(String username, final Set<String> tags) throws IOException, JSONException {
        if (httpClient == null) {
            httpClient = HttpClientBuilder.create().build();
        }

        JSONObject obj = new JSONObject();
        obj.put("userId", username);
        obj.put("tag", tags);

        System.out.println(obj.toString());

        HttpPost request = new HttpPost("http://" + ADDRESS + "/" + PATH);
        StringEntity body = new StringEntity(obj.toString());

        request.addHeader("content-type", "application/json");
        request.setEntity(body);

        HttpResponse response = httpClient.execute(request);
        System.out.println(response);

    }

    public static void main(String[] args) throws IOException, JSONException {
        if (args.length < 1) {
            throw new IllegalArgumentException("Insert at least an username (and some tags, if needed).");
        }

        String username = args[0];

        Set<String> tags = new HashSet<String>();
        for (int i = 1; i < args.length; i++) {
            tags.add(args[i]);
        }

        send(username, tags);
    }
}
