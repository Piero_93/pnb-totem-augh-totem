package pnb.mock.remoteadvertiser;

import pnb.totem.internetcommunication.ConnectionException;
import pnb.totem.internetcommunication.SwaggerClient;

/**
 * RemoteAdvertiser.
 */
public class RemoteAdvertiser {

    public static void main(String[] args) throws ConnectionException {
        if (args.length < 4) {
            throw new IllegalArgumentException("Insert username, password, latitude and longitude");
        }

        String username = args[0];
        String password = args[1];

        if (SwaggerClient.loginUser(username, password) != 200) {
            System.out.println("Wrong username or password");
            return;
        }

        Double latitude = Double.parseDouble(args[2]);
        Double longitude = Double.parseDouble(args[3]);

        SwaggerClient.setMyPos(username, latitude, longitude);

    }
}
