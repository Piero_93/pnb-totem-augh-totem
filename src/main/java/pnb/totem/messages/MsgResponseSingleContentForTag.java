package pnb.totem.messages;

import org.jetbrains.annotations.NotNull;
import pnb.totem.model.Content;

/**
 * MsgResponseSingleContentForTag:
 * Save a fetched content with a specified tag.
 */
public final class MsgResponseSingleContentForTag {
    private final Content content;

    public MsgResponseSingleContentForTag(@NotNull Content content) {
        if (content == null) {
            throw new IllegalArgumentException("Bundle can't be null");
        }
        this.content = content;
    }

    public Content getContent() {
        return content;
    }

    @Override
    public String toString() {
        return "MsgResponseSingleContentForTag{" +
                "content=" + content +
                '}';
    }
}
