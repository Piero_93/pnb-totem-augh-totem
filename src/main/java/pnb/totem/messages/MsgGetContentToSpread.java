package pnb.totem.messages;

import org.jetbrains.annotations.NotNull;

/**
 * MsgGetContentToSpread:
 * Request a content to be sent to the app
 */
public class MsgGetContentToSpread {
    private final int id;
    private final Object token;

    /**
     * Constructor
     *
     * @param id    the content id
     * @param token the request identifier
     */
    public MsgGetContentToSpread(int id, @NotNull Object token) {
        if (token == null) {
            throw new IllegalArgumentException("Null datas");
        }

        this.id = id;
        this.token = token;
    }

    /**
     * Get the content id
     *
     * @return the content id
     */
    public int getId() {
        return id;
    }

    /**
     * Get the token
     *
     * @return the token
     */
    public Object getToken() {
        return token;
    }

    @Override
    public String toString() {
        return "MsgGetContentToSpread{" +
                "id=" + id +
                ", token=" + token +
                '}';
    }
}
