package pnb.totem.messages;

import io.swagger.client.model.Position;
import org.jetbrains.annotations.NotNull;

/**
 * MsgNearUser:
 * This user is walking towards the Totem.
 */
public final class MsgNearUser {
    private final String username;
    private final Position position;

    public MsgNearUser(@NotNull String username, @NotNull Position position) {
        if (username == null || position == null) {
            throw new IllegalArgumentException("Username or position are null");
        }
        this.username = username;
        this.position = position;
    }

    public String getUsername() {
        return username;
    }

    public Position getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return "MsgNearUser{" +
                "username='" + username + '\'' +
                ", position=" + position +
                '}';
    }
}
