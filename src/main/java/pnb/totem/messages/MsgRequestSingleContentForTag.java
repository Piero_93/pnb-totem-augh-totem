package pnb.totem.messages;

import org.jetbrains.annotations.NotNull;

/**
 * MsgRequestSingleContentForTag:
 * Request a content with a specified tag.
 */
public final class MsgRequestSingleContentForTag {

    private final String tag;

    public MsgRequestSingleContentForTag(@NotNull String tag) {
        if (tag == null) {
            throw new IllegalArgumentException("Tag is null");
        }
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    @Override
    public String toString() {
        return "MsgRequestSingleContentForTag{" +
                "tag='" + tag + '\'' +
                '}';
    }
}
