package pnb.totem.messages;

import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Set;

/**
 * MsgSaveContent
 */
public final class MsgSaveContentIds {
    private final String tag;
    private final Set<Integer> contents;

    public MsgSaveContentIds(@NotNull String tag, @NotNull Set<Integer> contents) {
        if (tag == null || contents == null) {
            throw new IllegalArgumentException("Tag or Contents are null");
        }
        this.tag = tag;
        this.contents = contents;
    }

    public String getTag() {
        return tag;
    }

    public Set<Integer> getContents() {
        return new HashSet<>(contents);
    }

    @Override
    public String toString() {
        return "MsgSaveContentIds{" +
                "tag='" + tag + '\'' +
                ", contents=" + contents +
                '}';
    }
}
