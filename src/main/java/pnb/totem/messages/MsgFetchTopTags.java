package pnb.totem.messages;

import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Set;

/**
 * MsgFetchTopTags:
 * Fetch the most common tags in the current pool of near user.
 */
public final class MsgFetchTopTags {
    private final int n;
    private final Set<String> notToTake;

    public MsgFetchTopTags(int n, @NotNull Set<String> notToTake) {
        if (n < 0) {
            throw new IllegalArgumentException("Number is less than zero");
        }
        this.n = n;
        if (notToTake == null) {
            throw new IllegalArgumentException("Tags is null");
        }
        this.notToTake = notToTake;
    }

    public int getN() {
        return n;
    }

    public Set<String> getNotToTake() {
        return new HashSet<>(notToTake);
    }

    @Override
    public String toString() {
        return "MsgFetchTopTags{" +
                "n=" + n +
                ", notToTake=" + notToTake +
                '}';
    }
}
