package pnb.totem.messages;

import org.jetbrains.annotations.NotNull;
import pnb.totem.model.ContentMetadataWithoutThumbnailAndSplash;

import java.util.Collection;

/**
 * MsgAppContentList:
 * List of contents owned by an app
 */
public class MsgAppContentList {
    private final Collection<ContentMetadataWithoutThumbnailAndSplash> contents;
    private final Object token;

    /**
     * Constructor
     * @param contents list of contents
     * @param token request identifier
     */
    public MsgAppContentList(@NotNull Collection<ContentMetadataWithoutThumbnailAndSplash> contents, @NotNull Object token) {
        if (contents == null || token == null) {
            throw new IllegalArgumentException("Content set or token is null");
        }
        this.contents = contents;
        this.token = token;
    }

    /**
     * Get the list of contents
     * @return the list of contents
     */
    public Collection<ContentMetadataWithoutThumbnailAndSplash> getContents() {
        return contents;
    }

    /**
     * Get the token
     * @return the token
     */
    public Object getToken() {
        return token;
    }

    @Override
    public String toString() {
        return "MsgAppContentList{" +
                "contents=" + contents +
                ", token=" + token +
                '}';
    }
}
