package pnb.totem.messages;

/**
 * MsgContentNotPresent:
 * No content with specific tag.
 */
public class MsgContentNotPresent {
    private final String tag;

    public MsgContentNotPresent(String tag) {
        if (tag == null) {
            throw new IllegalArgumentException("Tag is null");
        }
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    @Override
    public String toString() {
        return "MsgContentNotPresent{" +
                "tag='" + tag + '\'' +
                '}';
    }
}
