package pnb.totem.messages;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * MsgSpreadContentIds:
 * Response to app spreading advertise.
 */
public class MsgSpreadContentIds {
    private final Collection<Integer> toBeSent;
    private final Collection<Integer> toBeReceived;
    private final Object token;

    /**
     * Constructor
     * @param toBeSent the list of content ids to be sent to the app
     * @param toBeReceived the list of content ids to be received from the app
     * @param token the request identifier
     */
    public MsgSpreadContentIds(@NotNull Collection<Integer> toBeSent, @NotNull Collection<Integer> toBeReceived, @NotNull Object token) {
        if (toBeSent == null || toBeReceived == null || token == null) {
            throw new IllegalArgumentException("Null datas");
        }

        this.toBeSent = toBeSent;
        this.toBeReceived = toBeReceived;
        this.token = token;
    }

    /**
     * Get the list of content ids to be sent to the app
     * @return the list of contents id
     */
    public Collection<Integer> getToBeSent() {
        return toBeSent;
    }

    /**
     * Get the list of content ids to be received from the app
     * @return the list of content ids
     */
    public Collection<Integer> getToBeReceived() {
        return toBeReceived;
    }

    /**
     * Get the token
     * @return the token
     */
    public Object getToken() {
        return token;
    }

    @Override
    public String toString() {
        return "MsgSpreadContentIds{" +
                "toBeSent=" + toBeSent +
                ", toBeReceived=" + toBeReceived +
                ", token=" + token +
                '}';
    }
}
