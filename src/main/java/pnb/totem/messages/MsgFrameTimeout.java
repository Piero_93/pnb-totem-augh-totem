package pnb.totem.messages;

import pnb.totem.utilities.Utilities;

import java.io.IOException;

/**
 * MsgFrameTimeout:
 * The specified frame has reached timeout.
 */
public final class MsgFrameTimeout {
    private final int frameNumber;

    public MsgFrameTimeout(int frameNumber) throws IOException {
        if (frameNumber < 0 || frameNumber >= Utilities.readFrameNumberFromFile()) {
            throw new IllegalArgumentException("Frame number is not valid");
        }
        this.frameNumber = frameNumber;
    }

    public int getFrameNumber() {
        return frameNumber;
    }

    @Override
    public String toString() {
        return "MsgFrameTimeout{" +
                "frameNumber=" + frameNumber +
                '}';
    }
}
