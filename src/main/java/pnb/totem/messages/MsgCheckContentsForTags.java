package pnb.totem.messages;

import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Set;

/**
 * MsgCheckContentsForTags:
 * Check if contents with a specific tags are already present.
 */
public final class MsgCheckContentsForTags {
    private final Set<String> tags;

    public MsgCheckContentsForTags(@NotNull Set<String> tags) {
        if (tags == null) {
            throw new IllegalArgumentException("Tags is null");
        }
        this.tags = tags;
    }

    public Set<String> getTags() {
        return new HashSet<>(tags);
    }

    @Override
    public String toString() {
        return "MsgCheckContentsForTags{" +
                "tags=" + tags +
                '}';
    }
}
