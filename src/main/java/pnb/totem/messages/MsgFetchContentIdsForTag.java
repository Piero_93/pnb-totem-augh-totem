package pnb.totem.messages;

import org.jetbrains.annotations.NotNull;

/**
 * MsgFetchContentIdsForTag:
 * Fetch all contents tagged with a specific tag.
 */
public final class MsgFetchContentIdsForTag {
    private final String tag;

    public MsgFetchContentIdsForTag(@NotNull String tag) {
        if (tag == null) {
            throw new IllegalArgumentException("Tag is null");
        }
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    @Override
    public String toString() {
        return "MsgFetchContentIdsForTag{" +
                "tag='" + tag + '\'' +
                '}';
    }
}
