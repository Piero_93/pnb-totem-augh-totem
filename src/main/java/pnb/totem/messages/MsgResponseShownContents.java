package pnb.totem.messages;

import org.jetbrains.annotations.NotNull;
import pnb.totem.model.Content;

import java.util.List;

/**
 * MsgResponseShownContents.
 * Contents currently shown on the screen.
 */
public class MsgResponseShownContents {
    private MsgAppContentList msg;
    private final List<Content> currentShownContents;
    private final int total;

    public MsgResponseShownContents(@NotNull List<Content> currentShownContents, int total) {
        if (currentShownContents == null) {
            throw new IllegalArgumentException("Current show contents is null");
        }
        if (total <= 0) {
            throw new IllegalArgumentException("Number of visualizable advertisment is invalid");
        }
        this.currentShownContents = currentShownContents;
        this.total = total;
    }

    public MsgResponseShownContents(MsgAppContentList msg, List<Content> currentShownContents, int total) {
        this(currentShownContents, total);
        this.msg = msg;
    }

    public List<Content> getCurrentShownContents() {
        return currentShownContents;
    }

    public int getTotal() {
        return total;
    }

    public MsgAppContentList getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return "MsgResponseShownContents{" +
                "msg=" + msg +
                ", currentShownContents=" + currentShownContents +
                ", total=" + total +
                '}';
    }
}
