package pnb.totem.messages;

import org.jetbrains.annotations.NotNull;
import pnb.totem.model.Content;

import java.util.Optional;

/**
 * MsgSendContentToSpread:
 * Content to be sent to the totem
 */
public class MsgSendContentToSpread {
    private final Content content;
    private final Object token;

    /**
     * Constructor
     *
     * @param content the content
     * @param token   the request identifier
     */
    public MsgSendContentToSpread(Content content, @NotNull Object token) {
        if (token == null) {
            throw new IllegalArgumentException("Null datas");
        }

        this.content = content;
        this.token = token;
    }

    /**
     * Get the content
     *
     * @return the content
     */
    public Optional<Content> getContent() {
        return Optional.ofNullable(content);
    }

    /**
     * Get the token
     *
     * @return the token
     */
    public Object getToken() {
        return token;
    }

    @Override
    public String toString() {
        return "MsgSendContentToSpread{" +
                "content=" + content +
                ", token=" + token +
                '}';
    }
}
