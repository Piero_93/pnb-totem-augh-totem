package pnb.totem.messages;

import org.jetbrains.annotations.NotNull;

/**
 * MsgCheckTagsForUser:
 * Check if tags for a specific user are already present.
 */
public final class MsgCheckTagsForUser {
    private final String username;

    public MsgCheckTagsForUser(@NotNull String username) {
        if (username == null) {
            throw new IllegalArgumentException("Username is null");
        }
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public String toString() {
        return "MsgCheckTagsForUser{" +
                "username='" + username + '\'' +
                '}';
    }
}