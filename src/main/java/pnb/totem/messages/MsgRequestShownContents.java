package pnb.totem.messages;

import java.util.Optional;

/**
 * MsgRequestShownContents:
 * Request currently shown contents.
 */
public final class MsgRequestShownContents {
    private MsgAppContentList msg;

    public MsgRequestShownContents() {
    }

    public MsgRequestShownContents(MsgAppContentList msg) {
        this.msg = msg;
    }

    public Optional<MsgAppContentList> getMsg() {
        return Optional.ofNullable(msg);
    }

    @Override
    public String toString() {
        return "MsgRequestShownContents{" +
                "msg=" + msg +
                '}';
    }
}
