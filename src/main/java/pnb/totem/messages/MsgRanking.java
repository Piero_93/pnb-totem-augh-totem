package pnb.totem.messages;

import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Set;

/**
 * MsgRanking:
 * Current tags ranking.
 */
public final class MsgRanking {
    private final Set<String> ranking;

    public MsgRanking(@NotNull Set<String> ranking) {
        if (ranking == null) {
            throw new IllegalArgumentException("Ranking can't be null");
        }
        this.ranking = ranking;
    }

    public Set<String> getRanking() {
        return new HashSet<>(ranking);
    }

    @Override
    public String toString() {
        return "MsgRanking{" +
                "ranking=" + ranking +
                '}';
    }
}
