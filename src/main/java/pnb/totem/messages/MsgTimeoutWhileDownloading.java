package pnb.totem.messages;

/**
 * MsgTimeoutWhileDownloading.
 * The content could not be downloaded, timeout was reached.
 */
public class MsgTimeoutWhileDownloading {
    private int contentId;

    public MsgTimeoutWhileDownloading(int contentId) {
        this.contentId = contentId;
    }

    public int getContentId() {
        return contentId;
    }
}
