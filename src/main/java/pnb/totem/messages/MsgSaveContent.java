package pnb.totem.messages;

import pnb.totem.model.Content;

/**
 * MsgSaveContent:
 * Save a fetched content.
 */
public final class MsgSaveContent {
    private final Content content;

    public MsgSaveContent(Content content) {
        this.content = content;
    }

    public Content getContent() {
        return content;
    }

    @Override
    public String toString() {
        return "MsgSaveContent{" +
                "content=" + content +
                '}';
    }
}
