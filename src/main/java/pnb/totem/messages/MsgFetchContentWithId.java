package pnb.totem.messages;

/**
 * MsgFetchContentWithId:
 * Fetch a content with a specified ID.
 */
public final class MsgFetchContentWithId {
    private final int id;

    public MsgFetchContentWithId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "MsgFetchContentWithId{" +
                "id=" + id +
                '}';
    }
}
