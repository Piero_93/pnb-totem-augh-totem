package pnb.totem.messages;

import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Set;

/**
 * MsgSaveTagsForUser:
 * Save fetched tags for a specified user.
 */
public final class MsgSaveTagsForUser {
    private final String username;
    private final Set<String> tags;

    public MsgSaveTagsForUser(@NotNull String username, @NotNull Set<String> tags) {
        if (username == null || tags == null) {
            throw new IllegalArgumentException("Username or Tags are null");
        }
        this.username = username;
        this.tags = tags;
    }

    public String getUsername() {
        return username;
    }

    public Set<String> getTags() {
        return new HashSet<>(tags);
    }

    @Override
    public String toString() {
        return "MsgSaveTagsForUser{" +
                "username='" + username + '\'' +
                ", tags=" + tags +
                '}';
    }
}
