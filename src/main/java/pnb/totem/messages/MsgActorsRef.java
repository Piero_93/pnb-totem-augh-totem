package pnb.totem.messages;

import akka.actor.ActorRef;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

/**
 * MsgActorRef:
 * Contains all references for the main actors.
 */
public final class MsgActorsRef {
    private final Map<String, ActorRef> refs;

    public MsgActorsRef(@NotNull Map<String, ActorRef> refs) {
        if (refs == null) {
            throw new IllegalArgumentException("Tags is null");
        }
        this.refs = refs;
    }

    public Map<String, ActorRef> getRefs() {
        return new HashMap<>(refs);
    }

    @Override
    public String toString() {
        return "MsgActorsRef{" +
                "refs=" + refs +
                '}';
    }
}
