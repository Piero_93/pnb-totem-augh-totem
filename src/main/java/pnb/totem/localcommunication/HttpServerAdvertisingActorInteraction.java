package pnb.totem.localcommunication;

import org.jetbrains.annotations.NotNull;

/**
 * Definisce i dati scambiati tra il server embedded e l'attore {@link pnb.totem.actors.LocalCommunicationManager}.
 * </p>
 *
 * I dati passati all'attore riguardano l'username e gli interessi dell'utente connesso.
 * Il token serve al server embedded per tenere traccia della relativa richiesta HTTP.
 */
public final class HttpServerAdvertisingActorInteraction {

    private final LocalUserDataBundle userData;
    private final Object token;

    public HttpServerAdvertisingActorInteraction(@NotNull LocalUserDataBundle userData,
                                                 @NotNull Object token) {
        if (userData == null || token == null) {
            throw new IllegalArgumentException("User data can't be null");
        }

        this.userData = userData;
        this.token = token;
    }

    public LocalUserDataBundle getUserData() {
        return userData;
    }

    public Object getToken() {
        return token;
    }
}
