package pnb.totem.localcommunication;

import org.jetbrains.annotations.NotNull;

public final class HttpServerContentPullActorInteraction {

    private final LocalUserIncomingPullRequest contentIdAndUserId;
    private final Object token;

    public HttpServerContentPullActorInteraction(LocalUserIncomingPullRequest contentIdAndUserId,
                                                 @NotNull Object token) {
        if (contentIdAndUserId == null || token == null) {
            throw new IllegalArgumentException("Token can't be null");
        }

        this.contentIdAndUserId = contentIdAndUserId;
        this.token = token;
    }

    public LocalUserIncomingPullRequest getContentIdAndUserId() {
        return contentIdAndUserId;
    }

    public Object getToken() {
        return token;
    }
}
