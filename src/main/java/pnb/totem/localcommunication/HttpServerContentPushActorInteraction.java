package pnb.totem.localcommunication;

import org.jetbrains.annotations.NotNull;
import pnb.totem.model.ContentMetadataWithoutThumbnailAndSplash;

public final class HttpServerContentPushActorInteraction {

    private final byte[] contentData;
    private final ContentMetadataWithoutThumbnailAndSplash metadata;
    private final Object token;

    public HttpServerContentPushActorInteraction(@NotNull byte[] contentData,
                                                 @NotNull ContentMetadataWithoutThumbnailAndSplash metadata,
                                                 @NotNull Object token) {
        if (contentData == null || metadata == null || token == null) {
            throw new IllegalArgumentException("Null parameters");
        }

        this.contentData = contentData;
        this.metadata = metadata;
        this.token = token;
    }

    public byte[] getContentData() {
        return contentData;
    }

    public ContentMetadataWithoutThumbnailAndSplash getMetadata() {
        return metadata;
    }

    public Object getToken() {
        return token;
    }
}
