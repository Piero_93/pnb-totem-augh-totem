package pnb.totem.localcommunication;

import java.util.Set;

/**
 * LocalUserDataBundle:
 * Models the user data
 */
public class LocalUserDataBundle {
    private String userId;
    private Set<String> tag;

    public LocalUserDataBundle() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Set<String> getTag() {
        return tag;
    }

    public void setTag(Set<String> tag) {
        this.tag = tag;
    }
}
