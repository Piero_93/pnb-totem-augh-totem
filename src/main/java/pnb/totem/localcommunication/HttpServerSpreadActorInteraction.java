package pnb.totem.localcommunication;

import org.jetbrains.annotations.NotNull;

public final class HttpServerSpreadActorInteraction {

    private final LocalUserIncomingSpreadBundle contentsOwnedByApp;
    private final Object token;

    public HttpServerSpreadActorInteraction(@NotNull LocalUserIncomingSpreadBundle contentsOwnedByApp,
                                            @NotNull Object token) {
        if (contentsOwnedByApp == null || token == null) {
            throw new IllegalArgumentException("Spread data can't be null");
        }

        this.contentsOwnedByApp = contentsOwnedByApp;
        this.token = token;
    }

    public LocalUserIncomingSpreadBundle getContentsOwnedByApp() {
        return contentsOwnedByApp;
    }

    public Object getToken() {
        return token;
    }
}
