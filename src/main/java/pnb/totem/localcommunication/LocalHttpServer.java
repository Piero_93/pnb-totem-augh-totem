package pnb.totem.localcommunication;

import akka.actor.ActorRef;
import com.google.gson.*;
import fi.iki.elonen.NanoHTTPD;
import org.apache.commons.io.FileUtils;
import pnb.totem.model.Content;
import pnb.totem.model.ContentBundle;
import pnb.totem.model.ContentMetadataWithoutThumbnailAndSplash;
import pnb.totem.model.ResponseToUserAdvertise;
import pnb.totem.utilities.Utilities;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * LocalHTTPServer:
 * Implements an HTTP Server using NanoHTTPD
 */
public class LocalHttpServer extends NanoHTTPD {
    private static final Gson customGson = new GsonBuilder().registerTypeHierarchyAdapter(byte[].class,
            new ByteArrayToBase64TypeAdapter()).create();
    private final ActorRef creator;
    private final Map<Object, Semaphore> onDone = new ConcurrentHashMap<>();
    private final Map<Object, Object> results = new ConcurrentHashMap<>();

    /**
     * Constructor
     *
     * @param localIp local ip address
     * @param port    local tcp port
     * @param creator actor that manages the server
     * @throws IOException If port already in use
     */
    public LocalHttpServer(String localIp, int port, ActorRef creator) throws IOException {
        super(localIp, port);
        if (creator == null) {
            throw new IllegalArgumentException("Creator ref can't be null");
        }
        this.creator = creator;
        start(NanoHTTPD.SOCKET_READ_TIMEOUT, true);
        System.out.println("\nRunning! Point your browsers to http://localhost:8080/ \n");
    }

    @Override
    public Response serve(IHTTPSession session) {
        try {
            final String uri = session.getUri();
            if("/advertise".equals(uri) || "/".equals(uri)) {
                return manageAdvertise(session, extractStringBody(session));
            } else if("/spread".equals(uri)) {
                return manageSpread(session, extractStringBody(session));
            } else if("/pull".equals(uri)) {
                return manageContentPull(session, extractStringBody(session));
            } else if("/pullmetadata".equals(uri)) {
                return manageMetadataPull(session, extractStringBody(session));
            } else if("/pullthumbnail".equals(uri)) {
                return manageThumbnailPull(session, extractStringBody(session));
            } else if("/pullsplash".equals(uri)) {
                return manageSplashScreenPull(session, extractStringBody(session));
            } else if("/push".equals(uri)) {
                return manageContentPush(session);
            } else {
                return newFixedLengthResponse(Response.Status.NOT_FOUND, "application/json", "");
            }
        }  catch (JsonSyntaxException | ResponseException e) {
            e.printStackTrace();
            return newFixedLengthResponse(Response.Status.BAD_REQUEST, "application/json", "");
        } catch (Exception e) {
            e.printStackTrace();
            return newFixedLengthResponse(Response.Status.INTERNAL_ERROR, "application/json", "");
        } finally {
            afterRequest(session);
        }
    }

    private NanoHTTPD.Response manageAdvertise(IHTTPSession session,
                                String json) throws InterruptedException {
        LocalUserDataBundle bundle = customGson.fromJson(json, LocalUserDataBundle.class);

        //Interact with actor
        Semaphore waitHere = new Semaphore(0);
        onDone.put(session, waitHere);
        creator.tell(new HttpServerAdvertisingActorInteraction(bundle, session), null);

        waitHere.acquire();
        ResponseToUserAdvertise result = (ResponseToUserAdvertise) results.get(session);

        return newFixedLengthResponse(Response.Status.OK, "application/json", customGson.toJson(result));
    }

    private NanoHTTPD.Response manageThumbnailPull(IHTTPSession session,
                                                   String json) throws InterruptedException, IOException{
        LocalUserIncomingPullRequest bundle = customGson.fromJson(json, LocalUserIncomingPullRequest.class);

        //Interact with actor
        Semaphore waitHere = new Semaphore(0);
        onDone.put(session, waitHere);
        creator.tell(new HttpServerContentPullActorInteraction(bundle, session), null);

        waitHere.acquire();
        @SuppressWarnings("unchecked")
        Optional<Content> result = (Optional<Content>) results.get(session);

        if(result.isPresent()) {
            return newChunkedResponse(
                    Response.Status.OK,
                    "image/png",
                    imageToByteStream(result.get().getBundle().getThumbnail())
            );
        } else {
            return newFixedLengthResponse(
                    Response.Status.NOT_FOUND,
                    "image/png",
                    ""
            );
        }
    }

    private NanoHTTPD.Response manageSplashScreenPull(IHTTPSession session,
                                                      String json) throws InterruptedException, IOException{
        LocalUserIncomingPullRequest bundle = customGson.fromJson(json, LocalUserIncomingPullRequest.class);

        //Interact with actor
        Semaphore waitHere = new Semaphore(0);
        onDone.put(session, waitHere);
        creator.tell(new HttpServerContentPullActorInteraction(bundle, session), null);

        waitHere.acquire();
        @SuppressWarnings("unchecked")
        Optional<Content> result = (Optional<Content>) results.get(session);

        if(result.isPresent()) {
            return newChunkedResponse(
                    Response.Status.OK,
                    "image/png",
                    imageToByteStream(result.get().getBundle().getSplashScreen())
            );
        } else {
            return newFixedLengthResponse(
                    Response.Status.NOT_FOUND,
                    "image/png",
                    ""
            );
        }
    }

    private NanoHTTPD.Response manageSpread(IHTTPSession session,
                                            String json) throws InterruptedException {
        LocalUserIncomingSpreadBundle bundle = customGson.fromJson(json, LocalUserIncomingSpreadBundle.class);

        //Interact with actor
        Semaphore waitHere = new Semaphore(0);
        onDone.put(session, waitHere);
        creator.tell(new HttpServerSpreadActorInteraction(bundle, session), null);

        waitHere.acquire();
        Object result = results.get(session);

        return newFixedLengthResponse(Response.Status.OK, "application/json", customGson.toJson(result));
    }

    private NanoHTTPD.Response manageContentPull(IHTTPSession session,
                                   String json) throws InterruptedException, IOException {
        LocalUserIncomingPullRequest bundle = customGson.fromJson(json, LocalUserIncomingPullRequest.class);

        //Interact with actor
        Semaphore waitHere = new Semaphore(0);
        onDone.put(session, waitHere);
        creator.tell(new HttpServerContentPullActorInteraction(bundle, session), null);

        waitHere.acquire();
        @SuppressWarnings("unchecked")
        Optional<Content> result = (Optional<Content>) results.get(session);

        if(result.isPresent()) {
            ContentBundle contentBundle = result.get().getBundle();
            contentBundle.getSplashScreen();
            return newChunkedResponse(
                    Response.Status.OK,
                    "application/octet-stream",
                    new ByteArrayInputStream(result.get().getBundle().getContentBytes())
            );
        } else {
            return newFixedLengthResponse(
                    Response.Status.NOT_FOUND,
                    "application/octet-stream",
                    ""
            );
        }
    }

    private NanoHTTPD.Response manageMetadataPull(IHTTPSession session,
                                                 String json) throws InterruptedException {
        LocalUserIncomingPullRequest bundle = customGson.fromJson(json, LocalUserIncomingPullRequest.class);

        //Interact with actor
        Semaphore waitHere = new Semaphore(0);
        onDone.put(session, waitHere);
        creator.tell(new HttpServerContentPullActorInteraction(bundle, session), null);

        waitHere.acquire();
        @SuppressWarnings("unchecked")
        Optional<Content> result = (Optional<Content>) results.get(session);

        if(result.isPresent()) {
            Content content = result.get();
            ContentMetadataWithoutThumbnailAndSplash metadata = new ContentMetadataWithoutThumbnailAndSplash();
            metadata.setContentId(content.getId());
            metadata.setRev(content.getRevNumber());
            metadata.setProviderId(content.getProviderId());
            metadata.setTag(content.getTags());
            metadata.setUrl(content.getUrl());
            metadata.setExpirationDate(Utilities.dateToString(content.getExpirationDate()));
            return newFixedLengthResponse(
                    Response.Status.OK,
                    "application/json",
                    customGson.toJson(metadata)
            );
        } else {
            return newFixedLengthResponse(
                    Response.Status.NOT_FOUND,
                    "application/json",
                    ""
            );
        }
    }

    private NanoHTTPD.Response manageContentPush(IHTTPSession session)
            throws ResponseException, IOException, InterruptedException {
        if (!(Method.POST.equals(session.getMethod()) || Method.PUT.equals(session.getMethod()))) {
            return newFixedLengthResponse(Response.Status.METHOD_NOT_ALLOWED, "application/json", "");
        }

        Map<String, String> files = new HashMap<>();
        session.parseBody(files);
        File contentDataFile = new File(files.get("contentData"));
        File contentMetadataFile = new File(files.get("contentMetadata1"));

        byte[] contentBinary = FileUtils.readFileToByteArray(contentDataFile);
        byte[] contentMetadataBinary = FileUtils.readFileToByteArray(contentMetadataFile);

        cleanIncomingFiles(files);

        ContentMetadataWithoutThumbnailAndSplash metadata = customGson.fromJson(
                new String(contentMetadataBinary, "UTF-8"),
                ContentMetadataWithoutThumbnailAndSplash.class);

        //Interact with actor
        Semaphore waitHere = new Semaphore(0);
        onDone.put(session, waitHere);
        creator.tell(new HttpServerContentPushActorInteraction(contentBinary, metadata, session), null);

        waitHere.acquire();
        Boolean isContentCorrect = (Boolean) results.get(session);

        if(isContentCorrect) {
            return newFixedLengthResponse(Response.Status.OK, "application/json", "");
        } else {
            return newFixedLengthResponse(Response.Status.BAD_REQUEST, "application/json", "");
        }
    }

    /**
     * Reply to an advertising user
     *
     * @param token  HTTP Request Identificator
     * @param result response
     */
    public void replyToUser(Object token, Object result) {
        Semaphore doneSemaphore = onDone.get(token);
        if (doneSemaphore != null) {
            results.put(token, result);
            doneSemaphore.release();
        }
    }

    private String extractStringBody(IHTTPSession session)
            throws ResponseException, IOException {
        final HashMap<String, String> map = new HashMap<>();
        session.parseBody(map);
        final String json = map.get("postData");
        if (json == null) {
               throw new JsonSyntaxException("Received json is null");
        }

        return json;
    }

    private void afterRequest(IHTTPSession session) {
        onDone.remove(session);
        results.remove(session);
    }

    private InputStream imageToByteStream(BufferedImage img) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ImageIO.write(img, "png", baos);
        } finally {
            try {
                baos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return new ByteArrayInputStream(baos.toByteArray());
    }

    private InputStream contentToByteArray(ZipFile zipFile) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ZipOutputStream zipOutputStream = new ZipOutputStream(bos);
        Enumeration<? extends ZipEntry> enumeration = zipFile.entries();
        while(enumeration.hasMoreElements()) {
            ZipEntry e = enumeration.nextElement();
            e.setCompressedSize(-1);
            zipOutputStream.putNextEntry(e);
            zipOutputStream.closeEntry();
        }
        zipOutputStream.close();

        return new ByteArrayInputStream(bos.toByteArray());
    }

    private void cleanIncomingFiles(Map<String, String> files) {
        for (Map.Entry<String, String> entry : files.entrySet()) {
            File tempFile = new File(entry.getValue());
            tempFile.delete();
        }
    }

    // Using Android's base64 libraries. This can be replaced with any base64 library.
    private static class ByteArrayToBase64TypeAdapter implements JsonSerializer<byte[]>, JsonDeserializer<byte[]> {
        public byte[] deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return Base64.getDecoder().decode(json.getAsString());
        }

        public JsonElement serialize(byte[] src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(Base64.getEncoder().encodeToString(src));
        }
    }
}