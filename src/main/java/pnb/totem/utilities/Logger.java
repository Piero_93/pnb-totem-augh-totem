package pnb.totem.utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Logger:
 * Prints log on the Standard Output.
 */
public class Logger {
    public static boolean verbose = true;
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm:ss");

    public static void log(String name, String text) {
        if (verbose) {
            System.out.println(DATE_FORMAT.format(new Date()) + " - [ " + name + " ] " + text);
        }
    }
}
