package pnb.totem.utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * SysKB:
 * All constants
 */
public class SysKB {

    //Json Keys
    public static final String USERNAME_JSON_TEXT = "username";
    public static final String PASSWORD_JSON_TEXT = "password";
    public static final String REFRESH_TIME_TEXT = "refreshtime";
    public static final String LOCAL_IP_TEXT = "host";
    public static final String LOCAL_PORT_TEXT = "port";
    public static final String BROWSER_CONF_TEXT = "browserconf";

    //Actor System Keys
    public static final String INTERNET_MANAGER_KEY = "imk";
    public static final String LOCAL_COMMUNICATION_MANAGER_KEY = "lcmk";
    public static final String USER_MANAGER_KEY = "umk";
    public static final String CONTENT_MANAGER_KEY = "cmk";
    public static final String GUI_MANAGER_KEY = "gmk";

    public static final String CONFIGURATION_FILE = "credentials.totemconf";
    public static final String CONTENT_MEMORY = "contents.pnb";

    //RabbitMQ Message Keys
    public static final String USER_ID_KEY = "userId";
    public static final String POSITION_KEY = "position";
    public static final String LATITUDE_KEY = "latitude";
    public static final String LONGITUDE_KEY = "longitude";

    //RabbitMQ
    public static final String QUEUE_PREFIX = "totem-";

    //File managing
    public static final String PATH_PREFIX = "contents/";
    public static final String SPLASH_SCREEN = "splashScreen.png";
    public static final String THUMBNAIL = "thumbnail.png";
    public static final String TOTEM_CONTENT = "totemContent.zip";
    public static final String APP_CONTENT = "appContent.zip";
    public static final String COMPLETE_CONTENT = "content.zip";

    //Miscellaneous
    public static final int USER_CACHE_TIMEOUT_MILLIS = 10000;
    public static final int CONTENT_LIMIT = 10;
    public static final String FRAME_NUMBER_TEXT = "framenum";
    public static final int SPREAD_CACHE_TIMEOUT_MILLIS = 10000;
    public final static String HOST = "192.168.10.13";//"biaginirouter.ddns.net";

    //GUI
    public static final String TOTEM_VIEW_HTML = "totemview.html";
    public static final String TOTEM_VIEW_JQUERY = "jquery-3.1.1.min.js";
    public static final String GUI_URL = "http://127.0.0.1:8081/";
    public static final String GUI_CONTENT_DIR = "currentContents";
    public static final String TEMP_CONTENT_DIR = "temp";

    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    static {
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

}
