package pnb.totem.utilities;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import pnb.totem.model.Content;
import pnb.totem.model.ContentMetadataWithoutThumbnailAndSplash;

import java.io.*;
import java.nio.file.Files;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Collection;
import java.util.stream.Collectors;

import static pnb.totem.utilities.SysKB.DATE_FORMAT;

/**
 * Utilities:
 * All utilities.
 */
public class Utilities {

    private static String username;
    private static String password;
    private static String ip;
    private static Integer port;
    private static Integer nFrames;
    private static Integer refreshTime;
    private static String browserConf;

    /**
     * Read username from the configuration file
     *
     * @return the username
     * @throws IOException something bad happened while reading/writing the file
     */
    public static String readUsernameFromFile() throws IOException {
        if (username != null) {
            return username;
        } else {
            loadFullConfiguration();
            return username;
        }
    }

    /**
     * Read password from the configuration file
     *
     * @return the password
     * @throws IOException something bad happened while reading/writing the file
     */
    public static String readPasswordFromFile() throws IOException {
        if (password != null) {
            return password;
        } else {
            loadFullConfiguration();
            return password;
        }
    }

    /**
     * Read ip address from the configuration file
     *
     * @return the ip address
     * @throws IOException something bad happened while reading/writing the file
     */
    public static String readIpFromFile() throws IOException {
        if (ip != null) {
            return ip;
        } else {
            loadFullConfiguration();
            return ip;
        }
    }

    /**
     * Read port from configuration file
     *
     * @return the port
     * @throws IOException something bad happened while reading/writing the file
     */
    public static int readPortFromFile() throws IOException {
        if (port != null) {
            return port;
        } else {
            loadFullConfiguration();
            return port;
        }
    }

    /**
     * Read frame number from configuration file
     *
     * @return the number of frames
     * @throws IOException
     */
    public static int readFrameNumberFromFile() throws IOException {
        if (nFrames != null) {
            return nFrames;
        } else {
            loadFullConfiguration();
            return nFrames;
        }
    }

    /**
     * Read refresh time from configuration file
     *
     * @return the refresh time
     * @throws IOException something bad happened while reading/writing the file
     */
    public static int readRefreshTimeFromFile() throws IOException {
        if (refreshTime != null) {
            return refreshTime;
        } else {
            loadFullConfiguration();
            return refreshTime;
        }
    }

    /**
     * Read browserconf from the configuration file
     *
     * @return the username
     * @throws IOException something bad happened while reading/writing the file
     */
    public static String readBrowserConfFromFile() throws IOException {
        if (browserConf != null) {
            return browserConf;
        } else {
            loadFullConfiguration();
            return browserConf;
        }
    }

    /**
     * Copies an input stream on an output stream
     *
     * @param input  the source
     * @param output the destination
     * @throws IOException
     */
    public static void copyStream(InputStream input, OutputStream output) throws IOException {
        /*int nRead;
        byte[] data = new byte[1024];
        while ((nRead = input.read(data, 0, data.length)) != -1) {
            output.write(data, 0, nRead);
        }*/
        IOUtils.copy(input, output);
    }

    /**
     * Updates username and password, then updates the totemconf
     *
     * @param user the username
     * @param pw   the password
     * @throws IOException something bad happened while reading/writing the file
     */
    public static void changeUserPw(String user, String pw) throws IOException {
        username = user;
        password = pw;
        byte[] confContent;
        try {
            confContent = Files.readAllBytes(new File(SysKB.CONFIGURATION_FILE).toPath());
            JSONObject json = new JSONObject(new String(confContent, "UTF-8"));
            json.put(SysKB.USERNAME_JSON_TEXT, user);
            json.put(SysKB.PASSWORD_JSON_TEXT, pw);

            try (FileWriter file = new FileWriter(SysKB.CONFIGURATION_FILE)) {
                file.write(json.toString());
            }
        } catch (JSONException e) {
            throw new IOException(e);
        }
    }

    /**
     * Load the whole configuration from file (used to lazy-initialize)
     *
     * @throws IOException something bad happened while reading/writing the file
     */
    private static void loadFullConfiguration() throws IOException {
        byte[] confContent;
        try {
            confContent = Files.readAllBytes(new File(SysKB.CONFIGURATION_FILE).toPath());
            JSONObject json = new JSONObject(new String(confContent, "UTF-8"));
            username = json.getString(SysKB.USERNAME_JSON_TEXT);
            password = json.getString(SysKB.PASSWORD_JSON_TEXT);
            ip = json.getString(SysKB.LOCAL_IP_TEXT);
            port = json.getInt(SysKB.LOCAL_PORT_TEXT);
            nFrames = json.getInt(SysKB.FRAME_NUMBER_TEXT);
            refreshTime = json.getInt(SysKB.REFRESH_TIME_TEXT);
            browserConf = json.getString(SysKB.BROWSER_CONF_TEXT);
        } catch (JSONException e) {
            throw new IOException(e);
        }
    }

    /**
     * Replace the frame number on the configuration file
     *
     * @param n the new frame number
     * @throws IOException something bad happened while reading/writing the file
     */
    public static void changeFrameNumber(int n) throws IOException {
        nFrames = n;
        byte[] confContent;
        try {
            confContent = Files.readAllBytes(new File(SysKB.CONFIGURATION_FILE).toPath());
            JSONObject json = new JSONObject(new String(confContent, "UTF-8"));
            json.put(SysKB.FRAME_NUMBER_TEXT, n);

            try (FileWriter file = new FileWriter(SysKB.CONFIGURATION_FILE)) {
                file.write(json.toString());
            }
        } catch (JSONException e) {
            throw new IOException(e);
        }
    }

    /**
     * Replace the refresh time on the configuration file
     *
     * @param refreshTime the new refresh time
     * @throws IOException something bad happened while reading/writing the file
     */
    public static void changeRefreshTime(int refreshTime) throws IOException {
        Utilities.refreshTime = refreshTime;
        byte[] confContent;
        try {
            confContent = Files.readAllBytes(new File(SysKB.CONFIGURATION_FILE).toPath());
            JSONObject json = new JSONObject(new String(confContent, "UTF-8"));
            json.put(SysKB.REFRESH_TIME_TEXT, refreshTime);

            try (FileWriter file = new FileWriter(SysKB.CONFIGURATION_FILE)) {
                file.write(json.toString());
            }
        } catch (JSONException e) {
            throw new IOException(e);
        }
    }

    public static Calendar stringToDate(String string) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(DATE_FORMAT.parse(string));
        return calendar;
    }

    public static String dateToString(Calendar calendar) {
        return DATE_FORMAT.format(calendar.getTime());
    }


    public static Collection<ContentMetadataWithoutThumbnailAndSplash> contentToContentMetadataWithoutThumbnailAndSplash(Collection<Content> collection) {
        return collection.stream().map(c -> {
            ContentMetadataWithoutThumbnailAndSplash res = new ContentMetadataWithoutThumbnailAndSplash();
            res.setTag(c.getTags());
            res.setContentId(c.getId());
            res.setProviderId(c.getProviderId());
            res.setRev(c.getRevNumber());
            res.setExpirationDate(Utilities.dateToString(c.getExpirationDate()));
            res.setUrl(c.getUrl());
            return res;
        }).collect(Collectors.toList());
    }

}
