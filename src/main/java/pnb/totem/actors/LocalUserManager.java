package pnb.totem.actors;

import javafx.util.Pair;
import pnb.totem.messages.*;
import pnb.totem.utilities.SysKB;

import java.util.*;
import java.util.stream.Collectors;

/**
 * LocalUserManager:
 * Manages the Users walking towards the Totem or currently connected.
 */
public class LocalUserManager extends AbstractMainActor {

    private Map<String, Pair<Set<String>, Calendar>> users;
    private Map<String, Integer> tagsCount;

    @Override
    public void preStart() throws Exception {
        super.preStart();
        users = new HashMap<>();
        tagsCount = new HashMap<>();
    }

    @Override
    public void onReceive(Object o) throws Throwable {
        super.onReceive(o);
        if (this.refs != null) {
            if (o instanceof MsgSaveTagsForUser) {
                users.put(((MsgSaveTagsForUser) o).getUsername(), new Pair<>(((MsgSaveTagsForUser) o).getTags(), Calendar.getInstance()));
                updateTagsCount(((MsgSaveTagsForUser) o).getTags(), true);
                refs.get(SysKB.CONTENT_MANAGER_KEY).tell(new MsgCheckContentsForTags(((MsgSaveTagsForUser) o).getTags()), getSelf());
            } else if (o instanceof MsgCheckTagsForUser) {
                String username = ((MsgCheckTagsForUser) o).getUsername();
                if (!users.containsKey(username)) {
                    refs.get(SysKB.INTERNET_MANAGER_KEY)
                            .tell(new MsgFetchTagsForUser(username), getSelf());
                } else {
                    if (this.cleanUsers(username) > 0) {
                        refs.get(SysKB.INTERNET_MANAGER_KEY).tell(new MsgFetchTagsForUser(username), getSelf());
                    } else {
                        refs.get(SysKB.CONTENT_MANAGER_KEY).tell(new MsgCheckContentsForTags(users.get(username).getKey()), getSelf());
                    }
                }
            } else if (o instanceof MsgFetchTopTags) {
                MsgRanking msg = new MsgRanking(generateRanking(((MsgFetchTopTags) o).getN(), ((MsgFetchTopTags) o).getNotToTake()));
                refs.get(SysKB.GUI_MANAGER_KEY).tell(msg, getSelf());
            }
        }
    }

    private void updateTagsCount(Set<String> tags, boolean adding) {
        if (adding) {
            for (String tag : tags) {
                if (!tagsCount.containsKey(tag)) {
                    tagsCount.put(tag, 0);
                }
                tagsCount.put(tag, tagsCount.get(tag) + 1);
            }
        } else {
            for (String tag : tags) {
                if (tagsCount.containsKey(tag)) {
                    tagsCount.put(tag, tagsCount.get(tag) - 1);
                    if (tagsCount.get(tag) == 0) {
                        tagsCount.remove(tag);
                    }
                }
            }
        }
    }

    private int cleanUsers(String username) {
        int count = 0;
        if (username == null) {
            Collection<String> toBeRemoved = new LinkedList<>();
            for (String user : this.users.keySet()) {
                if (System.currentTimeMillis() > users.get(user).getValue().getTimeInMillis() + SysKB.USER_CACHE_TIMEOUT_MILLIS) {
                    updateTagsCount(users.get(user).getKey(), false);
                    toBeRemoved.add(user);
                    count++;
                }
            }

            for (String user : toBeRemoved) {
                this.users.remove(user);
            }
        } else {
            if (System.currentTimeMillis() > users.get(username).getValue().getTimeInMillis() + SysKB.USER_CACHE_TIMEOUT_MILLIS) {
                updateTagsCount(users.get(username).getKey(), false);
                users.remove(username);
                count++;
            }
        }

        return count;
    }

    private Set<String> generateRanking(int topN, Set<String> notToTake) {
        cleanUsers(null);
        List<String> possibleTags = new ArrayList<>(tagsCount.keySet());
        possibleTags.removeAll(notToTake);
        List<Pair<String, Integer>> ranking = possibleTags.stream().map(s -> new Pair<>(s, tagsCount.get(s))).collect(Collectors.toList());
        ranking.sort(Comparator.comparingInt(Pair::getValue));
        return ranking.stream().limit(topN).map(Pair::getKey).collect(Collectors.toSet());
    }
}
