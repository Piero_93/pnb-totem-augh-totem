package pnb.totem.actors;

import akka.actor.UntypedActorWithStash;
import pnb.totem.utilities.Logger;

/**
 * AbstractLoggingActor:
 * An actor that logs received messages on a Logger.
 */
public abstract class AbstractLoggingActor extends UntypedActorWithStash {
    @Override
    public void onReceive(Object o) throws Throwable {
        log("Received " + o.toString() + "\n");
    }

    protected void log(String s) {
        Logger.log(this.getClass().getSimpleName(), s);
    }
}
