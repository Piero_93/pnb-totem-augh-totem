package pnb.totem.actors;

import pnb.totem.localcommunication.*;
import pnb.totem.messages.*;
import pnb.totem.model.*;
import pnb.totem.utilities.SysKB;
import pnb.totem.utilities.Utilities;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * LocalCommunicationManager:
 * Manages the local connections, to mobile devices.
 */
public class LocalCommunicationManager extends AbstractMainActor {

    private Map<String, Collection<HttpServerAdvertisingActorInteraction>> localAdvertising = new HashMap<>();
    private boolean waitingForGuiResponse = false;
    private LocalHttpServer server = null;

    private static byte[] extractBytes(BufferedImage image) throws IOException {
        ByteArrayOutputStream bos = null;
        try {
            bos = new ByteArrayOutputStream();
            ImageIO.write(image, "png", bos);
        } finally {
            if (bos != null)
                bos.close();
        }

        return bos.toByteArray();
    }

    @Override
    public void onReceive(Object o) throws Throwable {
        super.onReceive(o);

        if (this.refs == null) {
            return;
        }

        if (o instanceof AdvertiseReceived) {
            AdvertiseReceived msg = ((AdvertiseReceived) o);
            System.out.println("Received advertise for user " + msg.getUserId());
            refs.get(SysKB.USER_MANAGER_KEY).tell(new MsgSaveTagsForUser(msg.getUserId(), msg.getTags()), getSelf());
            if (!waitingForGuiResponse) {
                waitingForGuiResponse = true;
                refs.get(SysKB.GUI_MANAGER_KEY).tell(new MsgRequestShownContents(), getSelf());
            }
        } else if (o instanceof HttpServerAdvertisingActorInteraction) {
            HttpServerAdvertisingActorInteraction interaction = ((HttpServerAdvertisingActorInteraction) o);
            String userId = interaction.getUserData().getUserId();
            Set<String> tags = interaction.getUserData().getTag();

            localAdvertising.computeIfAbsent(userId, (k) -> new LinkedList<>()).add(interaction);
            getSelf().tell(new AdvertiseReceived(userId, tags), getSelf());
        } else if (o instanceof MsgResponseShownContents) {
            waitingForGuiResponse = false;
            List<Content> contents = ((MsgResponseShownContents) o).getCurrentShownContents();

            ResponseToUserAdvertise result = new ResponseToUserAdvertise();
            result.setMaxContentsOnScreen(((MsgResponseShownContents) o).getTotal());
            List<ContentMetadataWithoutThumbnailAndSplash> contentData = new ArrayList<>(contents.size());

            for (Content content : contents) {
                ContentMetadataWithoutThumbnailAndSplash metadata = new ContentMetadataWithoutThumbnailAndSplash();
                String isoTime = Utilities.dateToString(content.getExpirationDate());

                metadata.setContentId(content.getId());
                metadata.setExpirationDate(isoTime);
                metadata.setUrl(content.getUrl());
                metadata.setTag(content.getTags());
                metadata.setProviderId(content.getProviderId());
                //metadata.setSplashScreen(extractBytes(content.getBundle().getSplashScreen()));
                //metadata.setThumbnail(extractBytes(content.getBundle().getThumbnail()));

                contentData.add(metadata);
            }

            result.setContents(contentData);

            replyAllAdvertises(result);
        } else if (o instanceof HttpServerSpreadActorInteraction) {
            HttpServerSpreadActorInteraction interaction = ((HttpServerSpreadActorInteraction) o);
            refs.get(SysKB.CONTENT_MANAGER_KEY).tell(
                    new MsgAppContentList(
                            interaction.getContentsOwnedByApp().getContents(),
                            interaction.getToken()),
                    getSelf());
        } else if (o instanceof MsgSpreadContentIds) {
            MsgSpreadContentIds contentManagerSpreadResponse = ((MsgSpreadContentIds) o);
            Object interactionToken = contentManagerSpreadResponse.getToken();

            ResponseToUserSpread result = new ResponseToUserSpread();
            result.setToBeSentToApp(new ArrayList<>(contentManagerSpreadResponse.getToBeSent()));
            result.setToBeSentToTotem(new ArrayList<>(contentManagerSpreadResponse.getToBeReceived()));

            server.replyToUser(interactionToken, result);
        } else if (o instanceof HttpServerContentPullActorInteraction) {
            HttpServerContentPullActorInteraction interaction = ((HttpServerContentPullActorInteraction) o);
            refs.get(SysKB.CONTENT_MANAGER_KEY).tell(
                    new MsgGetContentToSpread(
                            interaction.getContentIdAndUserId().getContentId(),
                            interaction.getToken()),
                    getSelf());
        } else if (o instanceof MsgSendContentToSpread) {
            MsgSendContentToSpread contentManagerPullResponse = ((MsgSendContentToSpread) o);
            Object interactionToken = contentManagerPullResponse.getToken();

            server.replyToUser(interactionToken, contentManagerPullResponse.getContent());
        } else if (o instanceof HttpServerContentPushActorInteraction) {
            HttpServerContentPushActorInteraction interaction = ((HttpServerContentPushActorInteraction) o);

            ContentMetadataWithoutThumbnailAndSplash metadata = interaction.getMetadata();
            int contentId = metadata.getContentId();
            String providerId = metadata.getProviderId();
            Set<String> tags = metadata.getTag();
            String url = metadata.getUrl();
            Calendar expirationDate = Calendar.getInstance();
            expirationDate.setTime(Utilities.stringToDate(metadata.getExpirationDate()).getTime());

            int rev = metadata.getRev();
            boolean ok;
            try {
                /*ContentBundle bundle = new ContentBundleImpl(
                        contentId,
                        FTPClient.convertBinaryToZipFile(interaction.getContentData()));*/
                ContentBundle bundle = new ContentBundleImpl(contentId, interaction.getContentData());
                Content content = new Content(contentId, providerId, tags, url, expirationDate, rev, bundle);

                refs.get(SysKB.CONTENT_MANAGER_KEY).tell(
                        new MsgSaveContent(content),
                        getSelf());
                ok = true;
            } catch (Exception e) {
                e.printStackTrace();
                ok = false;
            }

            if(ok) {
                server.replyToUser(interaction.getToken(), true);
            } else {
                server.replyToUser(interaction.getToken(), false);
            }
        } else if (o instanceof MsgActorsRef) {
            waitForAdvertises();
        }
    }

    private void replyAllAdvertises(Object response) {
        for (Map.Entry<String, Collection<HttpServerAdvertisingActorInteraction>> entry : localAdvertising.entrySet()) {
            for (HttpServerAdvertisingActorInteraction interaction : entry.getValue()) {
                replyAdvertise(entry.getKey(), interaction, response);
            }
        }

        localAdvertising.clear();
    }

    @Override
    public void postStop() {
        localAdvertising.clear();
        waitingForGuiResponse = false;
        server.stop();
        server = null;
        super.postStop();
    }

    private void replyAdvertise(String userId, HttpServerAdvertisingActorInteraction interaction, Object response) {
        System.out.println("Replying user " + userId);
        server.replyToUser(interaction.getToken(), response);
    }

    private void waitForAdvertises() throws IOException {
        server = new LocalHttpServer(Utilities.readIpFromFile(), Utilities.readPortFromFile(), getSelf());
    }

    public class AdvertiseReceived {
        private final String userId;
        private final Set<String> tags;

        AdvertiseReceived(String userId, Set<String> tags) {
            this.userId = userId;
            this.tags = tags;
        }

        String getUserId() {
            return userId;
        }

        Set<String> getTags() {
            return tags;
        }
    }
}
