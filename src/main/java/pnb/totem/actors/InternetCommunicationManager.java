package pnb.totem.actors;

import akka.actor.ActorRef;
import akka.actor.Props;
import io.swagger.client.model.Position;
import org.json.JSONException;
import org.json.JSONObject;
import pnb.totem.internetcommunication.AMQPClient;
import pnb.totem.messages.*;
import pnb.totem.utilities.Utilities;

import java.net.ConnectException;

import static pnb.totem.utilities.SysKB.*;

/**
 * InternetCommunicationManager:
 * Manages the internet connections, e.g. to AMQP Server and to to Web Service.
 */
public class InternetCommunicationManager extends AbstractMainActor {

    private volatile int requestNumber = 0;

    @Override
    public void preStart() throws Exception {
        super.preStart();

        try {
            AMQPClient.register(QUEUE_PREFIX + Utilities.readUsernameFromFile(), (message) -> {
                try {
                    JSONObject data = new JSONObject(message);
                    String userId = data.getString(USER_ID_KEY);
                    Position position = new Position();
                    position.setLatitude(data.getJSONObject(POSITION_KEY).getDouble(LATITUDE_KEY));
                    position.setLongitude(data.getJSONObject(POSITION_KEY).getDouble(LONGITUDE_KEY));
                    InternetCommunicationManager.this.getSelf().tell(new MsgNearUser(userId, position), ActorRef.noSender());
                } catch (JSONException | IllegalArgumentException e) {
                    throw new IllegalArgumentException("Cannot parse the message!");
                }

                return null;
            });
        } catch (ConnectException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onReceive(Object o) throws Throwable {
        super.onReceive(o);

        if (refs == null) {
            return;
        }

        if (o instanceof MsgNearUser) { //From RabbitMQ
            MsgNearUser msg = (MsgNearUser) o;
            refs.get(USER_MANAGER_KEY).tell(new MsgCheckTagsForUser(msg.getUsername()), getSelf());
        } else if (o instanceof MsgFetchTagsForUser ||  //From Local User Manager
                o instanceof MsgFetchContentIdsForTag || //From Local Content Manager
                o instanceof MsgFetchContentWithId) {
            ActorRef requester = getContext().system().actorOf(Props.create(InternetRequester.class), "requester" + (++requestNumber));
            requester.tell(o, getSelf()); //To Internet Requester (demand to send asynchronous request)
        } else if (o instanceof MsgSaveTagsForUser) { //From Internet Requester
            refs.get(USER_MANAGER_KEY).tell(o, getSelf()); //To Local User Manager
        } else if (o instanceof MsgSaveContentIds || //From Internet Requester
                o instanceof MsgSaveContent ||  //From Internet Requester
                o instanceof MsgTimeoutWhileDownloading) { //From Internet Requester
            refs.get(CONTENT_MANAGER_KEY).tell(o, getSelf()); //To Local Content Manager
        }
    }
}