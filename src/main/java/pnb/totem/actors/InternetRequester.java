package pnb.totem.actors;

import io.swagger.client.model.ContentMetaData;
import org.joda.time.DateTimeZone;
import pnb.totem.internetcommunication.FTPClient;
import pnb.totem.internetcommunication.SwaggerClient;
import pnb.totem.messages.*;
import pnb.totem.model.Content;
import pnb.totem.model.ContentBundle;
import pnb.totem.model.ContentBundleImpl;
import pnb.totem.utilities.Utilities;

import java.util.HashSet;
import java.util.Locale;
import java.util.stream.Collectors;

import static pnb.totem.utilities.SysKB.CONTENT_LIMIT;

/**
 * InternetRequester:
 * Manages asynchronous requests to the Web Service.
 */
public class InternetRequester extends AbstractLoggingActor {
    private static final boolean INTERNET = true; //true se si vuole Internet, altrimenti false

    @Override
    public void onReceive(Object o) throws Throwable {
        super.onReceive(o);

        Object res = null;

        if (o instanceof MsgFetchTagsForUser) {
            MsgFetchTagsForUser msg = (MsgFetchTagsForUser) o;
            res = new MsgSaveTagsForUser(
                    msg.getUsername(),
                    SwaggerClient.getTagsForUser(msg.getUsername())
            );
        } else if (o instanceof MsgFetchContentIdsForTag) {
            MsgFetchContentIdsForTag msg = (MsgFetchContentIdsForTag) o;
            res = new MsgSaveContentIds(
                    msg.getTag(),
                    SwaggerClient.getIdsForTag(msg.getTag(), CONTENT_LIMIT).stream().map(Integer::parseInt).collect(Collectors.toSet())
            );
        } else if (o instanceof MsgFetchContentWithId) {
            MsgFetchContentWithId msg = (MsgFetchContentWithId) o;
            ContentMetaData metaData = SwaggerClient.getContentWithId("" + msg.getId());
            ContentBundle bundle = null;
            try {
                //ZipFile zipFile = FTPClient.fetchContent(metaData.getUrl());
                //bundle = new ContentBundleImpl(Integer.parseInt(metaData.getId()), zipFile);
                if(INTERNET) {
                    bundle = new ContentBundleImpl(Integer.parseInt(metaData.getId()), FTPClient.fetchContent(metaData.getUrl()));
                } else {
                    bundle = null;
                    throw new Exception();
                }

            } catch (Exception e) { //Zip package not valid or not found on the server
                res = new MsgTimeoutWhileDownloading(msg.getId());
            }
            if (bundle != null) {
                System.out.println(metaData);
                Content content = new Content(
                        Integer.parseInt(metaData.getId()),
                        metaData.getProviderId(),
                        new HashSet<>(metaData.getTag()),
                        metaData.getUrl(),
                        metaData.getExpirationDate().toCalendar(Locale.ITALIAN),
                        metaData.getRev(),
                        bundle);
                res = new MsgSaveContent(content);
            }
        }

        if (res != null) {
            getSender().tell(res, getSelf());
            context().stop(getSelf());
        }
    }
}
