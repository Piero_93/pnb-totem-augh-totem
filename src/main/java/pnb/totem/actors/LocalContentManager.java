package pnb.totem.actors;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.map.PassiveExpiringMap;
import pnb.totem.messages.*;
import pnb.totem.model.Content;
import pnb.totem.model.ContentBundleImpl;
import pnb.totem.model.ContentMetadataWithLocalPath;
import pnb.totem.model.ContentMetadataWithoutThumbnailAndSplash;
import pnb.totem.utilities.SysKB;
import pnb.totem.utilities.Utilities;

import javax.rmi.CORBA.Util;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

import static pnb.totem.utilities.SysKB.GUI_MANAGER_KEY;
import static pnb.totem.utilities.SysKB.LOCAL_COMMUNICATION_MANAGER_KEY;
import static pnb.totem.utilities.SysKB.SPREAD_CACHE_TIMEOUT_MILLIS;
import static pnb.totem.utilities.Utilities.contentToContentMetadataWithoutThumbnailAndSplash;

/**
 * LocalContentManager:
 * Manages the content and the metadata downloaded on the Totem.
 */
public class LocalContentManager extends AbstractMainActor {

    private Map<String, Set<Integer>> idsForTag;
    private Map<Integer, Content> contents;
    private Random r;

    private Set<String> requestedTags;

    private PassiveExpiringMap<Integer, ContentMetadataWithoutThumbnailAndSplash> currentlyInDownload;


    @Override
    public void preStart() throws Exception {
        super.preStart();
        idsForTag = new HashMap<>();
        contents = new HashMap<>();
        r = new Random(System.nanoTime());
        requestedTags = new HashSet<>();
        currentlyInDownload = new PassiveExpiringMap<>(SPREAD_CACHE_TIMEOUT_MILLIS, new HashMap<>());

        Gson gson = new Gson();
        try (FileReader fr = new FileReader(SysKB.CONTENT_MEMORY)) {
            List<ContentMetadataWithLocalPath> metadata = gson.fromJson(fr, new TypeToken<List<ContentMetadataWithLocalPath>>() {
            }.getType());
            metadata.forEach((c) -> {
                try {
                    contents.put(c.getContentId(), new Content(
                            c.getContentId(),
                            c.getProviderId(),
                            c.getTag(),
                            c.getUrl(),
                            Utilities.stringToDate(c.getExpirationDate()),
                            c.getRev(),
                            new ContentBundleImpl(c.getLocalPath())
                    ));
                    for (String tag : c.getTag()) {
                        idsForTag.computeIfAbsent(tag, (k) -> new HashSet<>()).add(c.getContentId());
                    }
                    this.log("Loaded from disk content " + c.getContentId());
                    System.out.println(c);
                } catch (Exception ignored) {
                    this.log("Error while loading from disk content " + c.getContentId());
                }
            });

        } catch (Exception ignored) {}

        System.out.println("Successully loaded from disk: " + contents.keySet());
    }

    private void updateFile() {
        System.out.println("Updating cache file");
        Gson gson = new Gson();
        try (FileOutputStream fos = new FileOutputStream(SysKB.CONTENT_MEMORY);
             OutputStreamWriter fw = new OutputStreamWriter(fos, StandardCharsets.UTF_8)) {
            fw.write(
                    gson.toJson(contents.entrySet().stream().map((e) -> {
                        Content c = e.getValue();
                        ContentMetadataWithLocalPath out = new ContentMetadataWithLocalPath();
                        out.setContentId(c.getId());
                        out.setProviderId(c.getProviderId());
                        out.setTag(c.getTags());
                        out.setUrl(c.getUrl());
                        out.setRev(c.getRevNumber());
                        out.setExpirationDate(Utilities.dateToString(c.getExpirationDate()));
                        out.setLocalPath(c.getBundle().getLocalPath());
                        return out;
                    }).collect(Collectors.toList())));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onReceive(Object o) throws Throwable {
        super.onReceive(o);
        if (this.refs != null) {
            if (o instanceof MsgCheckContentsForTags) {
                MsgCheckContentsForTags msg = (MsgCheckContentsForTags) o;
                for (String tag : msg.getTags()) {
                    if (!idsForTag.containsKey(tag)) {
                        this.idsForTag.put(tag, new HashSet<>());
                        this.refs.get(SysKB.INTERNET_MANAGER_KEY)
                                .tell(new MsgFetchContentIdsForTag(tag), getSelf());
                    } else {
                        //cleaning obsolete contents
                        this.idsForTag.put(tag, cleanOldContents(this.idsForTag.get(tag)));
                        if (this.idsForTag.get(tag).isEmpty()) {
                            this.refs.get(SysKB.INTERNET_MANAGER_KEY)
                                    .tell(new MsgFetchContentIdsForTag(tag), getSelf());
                        }
                    }
                }
            } else if (o instanceof MsgSaveContentIds) {
                MsgSaveContentIds msg = (MsgSaveContentIds) o;
                if (!msg.getContents().isEmpty()) {
                    for (Integer id : ((MsgSaveContentIds) o).getContents()) {
                        this.refs.get(SysKB.INTERNET_MANAGER_KEY)
                                .tell(new MsgFetchContentWithId(id), getSelf());
                    }
                }
            } else if (o instanceof MsgSaveContent) {
                MsgSaveContent msg = (MsgSaveContent) o;
                for (String tag : msg.getContent().getTags()) {
                    idsForTag.computeIfAbsent(tag, (k) -> new HashSet<>()).add(msg.getContent().getId());
                    //System.out.println(idsForTag.get(tag).size());
                }
                contents.put(msg.getContent().getId(), msg.getContent());
                Set<String> temp = new HashSet<>(requestedTags);
                temp.retainAll(msg.getContent().getTags());
                if (!temp.isEmpty()) {
                    this.requestedTags.removeAll(temp);
                }
                currentlyInDownload.remove(msg.getContent().getId());
                updateFile();
            } else if (o instanceof MsgRequestSingleContentForTag) {
                MsgRequestSingleContentForTag msg = (MsgRequestSingleContentForTag) o;
                List<Integer> possibleIds = new ArrayList<>(idsForTag.get(msg.getTag()));
                //System.out.println(possibleIds);
                if (possibleIds.size() > 0) {
                    int pick = r.nextInt(possibleIds.size());
                    if (contents.get(possibleIds.get(pick)) != null) {
                        this.refs.get(GUI_MANAGER_KEY).tell(new MsgResponseSingleContentForTag(contents.get(possibleIds.get(pick))), getSelf());
                    } else {
                        requestedTags.add(msg.getTag());
                    }
                } else {
                    this.refs.get(GUI_MANAGER_KEY).tell(new MsgContentNotPresent(msg.getTag()), getSelf());
                }
            } else if (o instanceof MsgTimeoutWhileDownloading) {
                idsForTag.forEach((s, integers) -> integers.remove(((MsgTimeoutWhileDownloading) o).getContentId()));
            } else if (o instanceof MsgAppContentList) {
                MsgAppContentList msg = (MsgAppContentList) o;
                this.refs.get(GUI_MANAGER_KEY).tell(new MsgRequestShownContents(msg), getSelf());
            } else if (o instanceof MsgResponseShownContents) {
                MsgResponseShownContents msg = (MsgResponseShownContents) o;
                Collection<Integer> toBeRequested = selectToBeRequested(msg.getMsg().getContents());
                Collection<Integer> toBeSent = selectToBeSent(msg.getMsg().getContents(), msg.getCurrentShownContents());
                this.refs.get(LOCAL_COMMUNICATION_MANAGER_KEY).tell(new MsgSpreadContentIds(toBeSent, toBeRequested, msg.getMsg().getToken()), getSelf());
            } else if (o instanceof MsgGetContentToSpread) {
                MsgGetContentToSpread msg = (MsgGetContentToSpread) o;
                this.refs.get(LOCAL_COMMUNICATION_MANAGER_KEY).tell(new MsgSendContentToSpread(contents.get(msg.getId()), msg.getToken()), getSelf());
            }
        }
    }

    private static Optional<Integer> selectContentWithPriority(Collection<ContentMetadataWithoutThumbnailAndSplash> availableContents,
                                                          Collection<ContentMetadataWithoutThumbnailAndSplash> alreadyPresentContents) {

        List<ContentMetadataWithoutThumbnailAndSplash> availableContentsCopy = new ArrayList<>(availableContents);
        availableContentsCopy.removeAll(alreadyPresentContents);

        Set<String> availableContentsTags = availableContentsCopy.stream().flatMap(c -> c.getTag().stream()).collect(Collectors.toSet());
        Set<String> alreadyPresentContentsTags = alreadyPresentContents.stream().flatMap(c -> c.getTag().stream()).collect(Collectors.toSet());

        if(availableContentsTags.isEmpty()) {
            return Optional.empty();
        }

        if(CollectionUtils.containsAll(alreadyPresentContentsTags, availableContentsTags)) { //Non ci sono nuovi tag
            Map<String, Set<Integer>> alreadyPresentIdsForTag = availableContentsTags.stream().map(tag ->
                    //Crea una coppia tag -> lista di id di contenuti che lo contengono
                new AbstractMap.SimpleImmutableEntry<>(tag, //Il tag
                        availableContentsCopy.stream()
                                .filter(content -> content.getTag().contains(tag))//Prendi i contentuti che contengono il tag
                                .map(ContentMetadataWithoutThumbnailAndSplash::getContentId))//Mappa contenuto -> id
            ).collect(Collectors.toMap(AbstractMap.SimpleImmutableEntry::getKey, c -> c.getValue().collect(Collectors.toSet())));

            String chosenTag = alreadyPresentIdsForTag.entrySet().stream().min(Comparator.comparing(entry -> entry.getValue().size())).get().getKey();

            List<Integer> possibleContents = new ArrayList<>(alreadyPresentIdsForTag.get(chosenTag));
            return Optional.of(possibleContents.get((int) (Math.random() * possibleContents.size())));
        } else { //Ci sono nuovi tag
            return Optional.of(availableContentsCopy.stream().filter(c -> !CollectionUtils.containsAll(alreadyPresentContentsTags, c.getTag()))
                    .findAny().get().getContentId());
        }
    }

    /**
     * TAGGED WITHOUT DUPLICATES
     *
     * @param userContents
     * @param currentShownContents
     * @return
     */
    private Collection<Integer> selectToBeSent(Collection<ContentMetadataWithoutThumbnailAndSplash> userContents, List<Content> currentShownContents) {
        Set<Integer> result = new HashSet<>();

        selectContentWithPriority(contentToContentMetadataWithoutThumbnailAndSplash(contents.values()), userContents).ifPresent(result::add);

        result.addAll(
                contentToContentMetadataWithoutThumbnailAndSplash(currentShownContents).stream()
                        .filter(content -> !userContents.contains(content))
                        .map(ContentMetadataWithoutThumbnailAndSplash::getContentId)
                        .collect(Collectors.toSet()));
        return result;
    }

    /**
     * TAGGED WITHOUT DUPLICATES
     *
     * @param userContents
     * @return
     */
    private Collection<Integer> selectToBeRequested(Collection<ContentMetadataWithoutThumbnailAndSplash> userContents) {
        List<Integer> result = new ArrayList<>(1);

        Collection<ContentMetadataWithoutThumbnailAndSplash> myContents = contentToContentMetadataWithoutThumbnailAndSplash(contents.values());
        myContents.addAll(currentlyInDownload.values());

        System.out.println("user contents: ");
        userContents.forEach(c -> System.out.println(c));
        System.out.println("\nmy contents: ");
        myContents.forEach(c -> System.out.println(c));
        System.out.println("\ncurr in download: ");
        System.out.println(currentlyInDownload.values());;
        selectContentWithPriority(userContents, myContents).ifPresent(result::add);

        if(!result.isEmpty()) { //Se c'è,aggiunge alla lista dei contenuti in download il contenuto selezionato
            currentlyInDownload.put(result.get(0), userContents.stream().filter(c -> c.getContentId() == result.get(0)).findFirst().get());
        }

        return result;
    }

    private Set<Integer> cleanOldContents(Set<Integer> ids) {
        Set<Integer> cleanedIds = new HashSet<>();
        for (int id : ids) {
            if (contents.get(id).getExpirationDate().getTimeInMillis() < System.currentTimeMillis()) {
                contents.remove(id);
            } else {
                cleanedIds.add(id);
            }
        }

        return cleanedIds;
    }
}
