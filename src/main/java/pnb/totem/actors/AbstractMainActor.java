package pnb.totem.actors;

import akka.actor.ActorRef;
import pnb.totem.messages.MsgActorsRef;

import java.util.Map;

/**
 * AbstractMainActor:
 * A logging actor that waits for a MsgActorsRef before responding to other messages.
 */
public abstract class AbstractMainActor extends AbstractLoggingActor {
    protected Map<String, ActorRef> refs;

    @Override
    public void onReceive(Object o) throws Throwable {
        super.onReceive(o);

        if (o instanceof MsgActorsRef) {
            refs = ((MsgActorsRef) o).getRefs();
            unstashAll();
        } else if (refs == null) {
            stash();
        }
    }
}
