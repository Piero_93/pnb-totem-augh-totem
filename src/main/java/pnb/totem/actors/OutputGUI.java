package pnb.totem.actors;

import pnb.totem.messages.*;
import pnb.totem.model.Content;
import pnb.totem.utilities.Utilities;
import pnb.totem.view.ContentView;
import pnb.totem.view.ContentViewImpl;
import scala.concurrent.duration.Duration;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static pnb.totem.utilities.SysKB.*;

/**
 * OutputGUI:
 * Manages the Totem view.
 */
public class OutputGUI extends AbstractMainActor {
    private ContentView view;

    private Map<Integer, Content> currentShownContent;

    private Set<Integer> framesWaitingForContent;

    private Set<String> currentlyRequestedTags;

    private Random r;

    @Override
    public void preStart() throws Exception {
        super.preStart();

        this.currentShownContent = new HashMap<>();

        this.framesWaitingForContent = new HashSet<>();

        this.currentlyRequestedTags = new HashSet<>();

        this.r = new Random();

        this.view = new ContentViewImpl(
                Utilities.readFrameNumberFromFile(),
                (url) -> new ProcessBuilder(Utilities.readBrowserConfFromFile(), url).start()
        );
        this.view.show();
    }

    @Override
    public void postStop() {
        super.postStop();

        if (this.view != null) {
            if (this.view.isVisible()) {
                this.view.hide();
            }
            this.view = null;
        }
    }

    @Override
    public void onReceive(Object o) throws Throwable {
        super.onReceive(o);

        if (this.refs == null) {
            return;
        }

        if (o instanceof MsgActorsRef) { //1st message: only now the actor starts
            for (int i = 0; i < Utilities.readFrameNumberFromFile(); i++) {
                restartRefreshTimer(i, 0);
            }
        } else if (o instanceof MsgFrameTimeout) { //A frame needs a new content
            MsgFrameTimeout msg = (MsgFrameTimeout) o;
            int timeoutFrame = msg.getFrameNumber();
            framesWaitingForContent.add(timeoutFrame); //Add the frame to the waiting list
            restartRefreshTimer(timeoutFrame, Utilities.readRefreshTimeFromFile()); //Restart its timer without change the content


            Set<String> currentTags = currentShownContent.keySet().stream()
                    .filter((i) -> i != timeoutFrame)
                    .map((i) -> currentShownContent.get(i).getTags())
                    .reduce(new HashSet<>(), (a, b) -> {
                        a.addAll(b);
                        return a;
                    });

            refs.get(USER_MANAGER_KEY).tell(new MsgFetchTopTags( //Ask for a new ranking
                    Utilities.readFrameNumberFromFile(),
                    currentTags //Contains the tag that are currently shown on the screen
            ), getSelf());
        } else if (o instanceof MsgRanking) { //New ranking arrived
            MsgRanking msg = (MsgRanking) o;
            Set<String> topTags = msg.getRanking();
            topTags.removeAll(currentlyRequestedTags);
            if (topTags.isEmpty()) { //No users are near or all the tags have already been requested: the frame must be turned off
                int frame = pickRandomFromSet(framesWaitingForContent);
                //restartRefreshTimer(frame, Utilities.readRefreshTimeFromFile()); //Restart its timer without change the content
                currentShownContent.remove(frame);
                this.view.setContent(null, frame);
            } else { //Some tags can be shown: request a content for one of them
                String selectedTag = pickRandomFromSet(topTags);
                this.currentlyRequestedTags.add(selectedTag);
                this.refs.get(CONTENT_MANAGER_KEY).tell(
                        new MsgRequestSingleContentForTag(selectedTag),
                        getSelf()
                );
            }
        } else if (o instanceof MsgResponseSingleContentForTag) { //A new content arrives
            MsgResponseSingleContentForTag msg = (MsgResponseSingleContentForTag) o;
            currentlyRequestedTags.removeAll(msg.getContent().getTags());
            int frame = -1;
            System.err.println("Contenuti correntemente visualizzati: " + currentShownContent.values().stream().map(Content::getId).collect(Collectors.toList()));
            System.err.println("Contenuto attualemtne ottenuto: " + msg.getContent().getId());
            if (currentShownContent.containsValue(msg.getContent())) { //If that content is already shown
                frame = currentShownContent.entrySet().stream() //Get the index of that frame
                        .filter(entry -> entry.getValue() == msg.getContent())
                        .findFirst()
                        .map(Map.Entry::getKey)
                        .get();
                framesWaitingForContent.remove(frame);
            } else { //If that content is not currently shown
                frame = pickRandomFromSet(framesWaitingForContent);
            }
            System.err.println("Setting" + msg.getContent().getId() + " to frame " + frame);
            this.view.setContent(msg.getContent(), frame);
            currentShownContent.put(frame, msg.getContent());
            System.err.println("Dopo aggiunta: " + currentShownContent.values().stream().map(Content::getId).collect(Collectors.toList()));
        } else if (o instanceof MsgRequestShownContents) {
            MsgRequestShownContents msg = ((MsgRequestShownContents) o);
            if(msg.getMsg().isPresent()) {
                this.refs.get(CONTENT_MANAGER_KEY).tell(
                        new MsgResponseShownContents(msg.getMsg().get(), new ArrayList<>(this.currentShownContent.values()), Utilities.readFrameNumberFromFile()), getSelf()
                );
            } else {
                this.refs.get(LOCAL_COMMUNICATION_MANAGER_KEY).tell(
                        new MsgResponseShownContents(new ArrayList<>(this.currentShownContent.values()), Utilities.readFrameNumberFromFile()), getSelf()
                );
            }
        } else if (o instanceof MsgContentNotPresent) {
            this.currentlyRequestedTags.remove(((MsgContentNotPresent) o).getTag());
        }
    }

    private void restartRefreshTimer(int frameNumber, int time) throws IOException {
        getContext().system().scheduler().scheduleOnce(
                Duration.create(time, TimeUnit.SECONDS),
                getSelf(),
                new MsgFrameTimeout(frameNumber),
                getContext().system().dispatcher(),
                getSelf()
        );
    }

    private <T> T pickRandomFromSet(Set<T> set) {
        int index = r.nextInt(set.size());
        Iterator<T> iter = set.iterator();
        for (int i = 0; i < index; i++) {
            iter.next();
        }
        T selected = iter.next();
        set.remove(selected);
        return selected;
    }
}
