package pnb.totem.main;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import pnb.totem.actors.*;
import pnb.totem.messages.MsgActorsRef;
import pnb.totem.utilities.SysKB;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Main.
 * Application Starter.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        /*try {
            String username = Utilities.readUsernameFromFile();
            String password = Utilities.readPasswordFromFile();
            if (!SwaggerClient.loginTotem(username, password)) {
                new LoginAndRegistrationView(Main::generateEnvironment);
            } else {
                generateEnvironment();
            }
        } catch (IOException e) {
            new LoginAndRegistrationView(Main::generateEnvironment);
        }*/
        generateEnvironment();
    }

    private static void generateEnvironment() {
        ActorSystem system = ActorSystem.create("TotemSystem");

        //actors creation
        ActorRef internetCommActor = system.actorOf(Props.create(InternetCommunicationManager.class), "internetmanager");
        ActorRef localCommActor = system.actorOf(Props.create(LocalCommunicationManager.class), "localcommmanager");
        ActorRef localUserActor = system.actorOf(Props.create(LocalUserManager.class), "usermanager");
        ActorRef localContentActor = system.actorOf(Props.create(LocalContentManager.class), "contentmanager");
        ActorRef guiActor = system.actorOf(Props.create(OutputGUI.class), "outputgui");

        Map<String, ActorRef> refs = new HashMap<>();
        refs.put(SysKB.INTERNET_MANAGER_KEY, internetCommActor);
        refs.put(SysKB.CONTENT_MANAGER_KEY, localContentActor);
        refs.put(SysKB.GUI_MANAGER_KEY, guiActor);
        refs.put(SysKB.USER_MANAGER_KEY, localUserActor);
        refs.put(SysKB.LOCAL_COMMUNICATION_MANAGER_KEY, localCommActor);

        MsgActorsRef msg = new MsgActorsRef(refs);

        for (ActorRef act : refs.values()) {
            act.tell(msg, ActorRef.noSender());
        }
    }
}
