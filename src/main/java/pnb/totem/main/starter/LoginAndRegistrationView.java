package pnb.totem.main.starter;

import io.swagger.client.model.Position;
import io.swagger.client.model.RegistrationErrors;
import pnb.totem.internetcommunication.ConnectionException;
import pnb.totem.internetcommunication.SwaggerClient;
import pnb.totem.utilities.SysKB;
import pnb.totem.utilities.Utilities;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.io.IOException;
import java.util.Optional;

/**
 * LoginAndRegistrationView.
 * Manages the view to log in and register.
 */
public class LoginAndRegistrationView extends JFrame {
    private final JButton registrationButton;
    private final JButton loginButton;
    private JTextField usernameField;
    private JTextField passwordField;
    private JTextField visualizableAnnField;
    private JTextField latField;
    private JTextField longField;
    private JLabel posLabel;
    private JLabel visAnnLabel;

    private JLabel logLabel = new JLabel("");

    private JPanel textPanel;

    private boolean registering = false;

    private ViewObserver observer;

    public LoginAndRegistrationView(ViewObserver observer) throws HeadlessException {
        this.setLayout(new BorderLayout());
        this.setTitle("Autenticazione Totem");
        this.observer = observer;

        registrationButton = new JButton("Registrazione");
        registrationButton.setForeground(Color.black);
        registrationButton.setBackground(Color.white);
        loginButton = new JButton("Login");
        loginButton.setForeground(Color.WHITE);
        loginButton.setBackground(Color.black);
        JButton accessButton = new JButton("Accedi");

        usernameField = new JTextField(12);
        passwordField = new JPasswordField(12);
        visualizableAnnField = new JFormattedTextField(3);
        latField = new JFormattedTextField(12);
        longField = new JFormattedTextField(12);

        logLabel = new JLabel("");
        logLabel.setForeground(Color.RED);

        JPanel buttonPanel = new JPanel(new FlowLayout());
        textPanel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        JPanel southPanel = new JPanel(new FlowLayout());

        buttonPanel.add(registrationButton);
        buttonPanel.add(loginButton);

        registrationButton.addActionListener(e -> {
            registering = true;
            refresh(true);
        });

        loginButton.addActionListener(e -> {
            registering = false;
            refresh(false);
        });

        accessButton.addActionListener(e -> new Thread(this::tryAccess).start());

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 0;
        textPanel.add(new JLabel(SysKB.USERNAME_JSON_TEXT), c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 0;
        textPanel.add(usernameField, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 1;
        textPanel.add(new JLabel(SysKB.PASSWORD_JSON_TEXT), c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 1;
        textPanel.add(passwordField, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 2;
        visAnnLabel = new JLabel("Annunci Visualizzabili:");
        textPanel.add(visAnnLabel, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 2;
        textPanel.add(visualizableAnnField, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 3;
        posLabel = new JLabel("Posizione:");
        textPanel.add(posLabel, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 4;
        textPanel.add(latField, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 4;
        textPanel.add(longField, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 5;
        textPanel.add(accessButton, c);

        textPanel.setBorder(new EmptyBorder(10, 20, 10, 20));

        southPanel.add(logLabel);

        this.add(buttonPanel, BorderLayout.NORTH);
        this.add(textPanel, BorderLayout.CENTER);
        this.add(southPanel, BorderLayout.SOUTH);

        textPanel.setVisible(false);
        this.setMinimumSize(new Dimension(400, 400));
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        this.setResizable(false);
        this.setVisible(true);
    }

    private void tryAccess() {
        if (registering) {
            try {
                Position position = new Position();
                position.setLatitude(Double.parseDouble(latField.getText()));
                position.setLongitude(Double.parseDouble(longField.getText()));
                Optional<RegistrationErrors> errors = SwaggerClient.registerNewTotem(
                        usernameField.getText(),
                        passwordField.getText(),
                        Integer.parseInt(visualizableAnnField.getText()),
                        position);
                if (!errors.isPresent()) {
                    try {
                        Utilities.changeUserPw(usernameField.getText(), passwordField.getText());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    if (SwaggerClient.loginTotem(usernameField.getText(), passwordField.getText())) {
                        SwingUtilities.invokeLater(() -> {
                            observer.userLogged();
                            this.setVisible(false);
                        });
                    }
                } else {
                    StringBuilder sb = new StringBuilder();
                    if (errors.get().getDuplicatedUsername()) {
                        sb.append("USERNAME DUPLICATO.   ");
                    }
                    if (errors.get().getTooShortUsername()) {
                        sb.append("USERNAME TROPPO CORTO.   ");
                    }
                    if (errors.get().getTooShortPassword()) {
                        sb.append("PASSWORD TROPPO CORTA.   ");
                    }
                    SwingUtilities.invokeLater(() -> logLabel.setText(sb.toString()));
                }

            } catch (ConnectionException e) {
                SwingUtilities.invokeLater(() -> logLabel.setText("CONNECTION EXCEPTION: CODICE " + e.getErrorCode()));
            }
        } else {

            try {
                if (SwaggerClient.loginTotem(usernameField.getText(), passwordField.getText())) {
                    Utilities.changeUserPw(usernameField.getText(), passwordField.getText());
                    SwingUtilities.invokeLater(() -> {
                        observer.userLogged();
                        this.setVisible(false);
                    });
                } else {
                    SwingUtilities.invokeLater(() -> logLabel.setText("LOGIN FALLITO"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void refresh(boolean registration) {
        if (registration) {
            registrationButton.setBackground(Color.RED);
            registrationButton.setForeground(Color.white);
            loginButton.setBackground(Color.white);
            loginButton.setForeground(Color.black);
            textPanel.setVisible(true);
            visAnnLabel.setVisible(true);
            posLabel.setVisible(true);
            visualizableAnnField.setVisible(true);
            latField.setVisible(true);
            longField.setVisible(true);
        } else {
            registrationButton.setBackground(Color.white);
            registrationButton.setForeground(Color.black);
            loginButton.setBackground(Color.red);
            loginButton.setForeground(Color.white);
            textPanel.setVisible(true);
            visAnnLabel.setVisible(false);
            posLabel.setVisible(false);
            visualizableAnnField.setVisible(false);
            latField.setVisible(false);
            longField.setVisible(false);
        }
    }

    /**
     * ViewObserver.
     * Observer for the LoginAndRegistrationView.
     */
    public interface ViewObserver {
        void userLogged();
    }
}

