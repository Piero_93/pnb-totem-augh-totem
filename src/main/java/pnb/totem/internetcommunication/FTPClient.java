package pnb.totem.internetcommunication;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import pnb.totem.utilities.SysKB;
import pnb.totem.utilities.Utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;
import java.util.zip.ZipFile;


/**
 * FTPClient:
 * Manages the FTP Connections to download the contents for the provider's services.
 */
public class FTPClient {
    //private final String HOST = "biaginirouter.myddns.me";
    //private final String USERNAME = "pnb-totem";
    //private final String PASSWORD = "q1w2e3r4t5y6u7i8o9p0";

    private FTPClient() {
    }

    /**
     * Fetch a zipFile from the specified IP Address
     *
     * @param address the file address
     * @return the read zipFile
     * @throws IOException
     */
    /*public static ZipFile fetchContent(String address) throws IOException {
        URL url = new URL(address);
        URLConnection con = url.openConnection();
        InputStream inputStream = con.getInputStream();
        Random r = new Random();

        File outputFile;
        do {
            outputFile = new File(SysKB.TEMP_CONTENT_DIR, r.nextInt(Integer.MAX_VALUE) + ".zip");
            outputFile.getParentFile().mkdirs();
        } while (!outputFile.createNewFile());

        FileOutputStream outputStream = new FileOutputStream(outputFile);
        Utilities.copyStream(inputStream, outputStream);
        inputStream.close();
        outputStream.close();

        return new ZipFile(outputFile);
    }*/

    public static byte[] fetchContent(String address) throws IOException {
        URL url = new URL(address);
        URLConnection con = url.openConnection();
        return IOUtils.toByteArray(con.getInputStream());
    }

    public static ZipFile fetchContent(InputStream inputStream) throws IOException {
        Random r = new Random();

        File outputFile;
        do {
            outputFile = new File(SysKB.TEMP_CONTENT_DIR, r.nextInt(Integer.MAX_VALUE) + ".zip");
            outputFile.getParentFile().mkdirs();
        } while (!outputFile.createNewFile());

        FileOutputStream outputStream = new FileOutputStream(outputFile);
        Utilities.copyStream(inputStream, outputStream);
        inputStream.close();
        outputStream.close();

        return new ZipFile(outputFile);
    }

    public static ZipFile convertBinaryToZipFile(byte[] bytes) throws IOException {
        Random r = new Random();

        File outputFile;
        do {
            outputFile = new File(SysKB.TEMP_CONTENT_DIR, r.nextInt(Integer.MAX_VALUE) + ".zip");
            outputFile.getParentFile().mkdirs();
        } while (!outputFile.createNewFile());

        FileOutputStream outputStream = new FileOutputStream(outputFile);
        outputStream.write(bytes);
        outputStream.close();

        return new ZipFile(outputFile);
    }
}
