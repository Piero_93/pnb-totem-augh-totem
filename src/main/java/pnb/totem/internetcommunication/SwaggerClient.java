package pnb.totem.internetcommunication;

import io.swagger.client.ApiException;
import io.swagger.client.api.DefaultApi;
import io.swagger.client.model.*;
import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;
import java.util.stream.Collectors;

/**
 * SwaggerClient:
 * Client Wrapper for Swagger API.
 */
public final class SwaggerClient {
    private static DefaultApi instance;

    private SwaggerClient() {
    }

    private static void checkInstance() {
        if (instance == null) {
            instance = new DefaultApi();
        }
    }

    private static Optional<RegistrationErrors> manageRegistrationError(ApiException e) throws ConnectionException {
        if (e.getCode() == 422) {
            RegistrationErrors registrationError = new RegistrationErrors();
            try {
                JSONObject o = new JSONObject(e.getResponseBody());
                registrationError.setDuplicatedUsername(o.optBoolean("duplicatedUsername"));
                registrationError.setTooShortPassword(o.optBoolean("tooShortPassword"));
                registrationError.setTooShortUsername(o.optBoolean("tooShortUsername"));
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
            return Optional.of(registrationError);
        } else {
            throw new ConnectionException(e.getCode(), e.getMessage());
        }
    }

    /*
     * TOTEM
     */

    /**
     * Login
     *
     * @param username the username
     * @param password the password
     * @return true if the totem is logged, false if credentials are wrong
     */
    public static boolean loginTotem(String username, String password) {
        checkInstance();

        instance.getApiClient().addDefaultHeader("Entity", "Totem");
        instance.getApiClient().setUsername(username);
        instance.getApiClient().setPassword(password);

        //The totem is not logged only if the status code is 401.
        //If code is 200 the totem is logged (and the user exists).
        //If code is 404 the totem is logged (but the user does not exists).
        try {
            instance.userUserIdInterestsGET(0 + "");
        } catch (ApiException e) {
            return e.getCode() == 404;
        }
        return true;
    }

    /**
     * Get Ids for a specified tag
     *
     * @param tag   the tag
     * @param limit the max number of contents
     * @return a set of Content Ids
     * @throws ConnectionException something bad happened while requesting
     */
    public static Set<String> getIdsForTag(String tag, int limit) throws ConnectionException, IllegalArgumentException {
        checkInstance();

        if (limit <= 0) {
            throw new IllegalArgumentException("Invalid limit number");
        }

        try {
            return instance.indexTagGET(tag, limit).stream().map(ContentMetaData::getId).collect(Collectors.toSet());
        } catch (ApiException e) {
            throw new ConnectionException(e.getCode(), e.getMessage());
        }
    }

    /**
     * Get the metadata for a content with a specified Id
     *
     * @param id the content id
     * @return the metadata for the content with the specified Id
     * @throws ConnectionException something bad happened while requesting
     */
    public static ContentMetaData getContentWithId(String id) throws ConnectionException {
        checkInstance();

        try {
            return instance.contentContentIdGET(id);
        } catch (ApiException e) {
            throw new ConnectionException(e.getCode(), e.getMessage());
        }
    }

    /**
     * Get Tags for a specific user
     *
     * @param id the user id
     * @return a set containing the tags for the specified user
     * @throws ConnectionException something bad happened while requesting
     */
    public static Set<String> getTagsForUser(String id) throws ConnectionException {
        checkInstance();

        try {
            return new HashSet<>(instance.userUserIdInterestsGET(id));
        } catch (ApiException e) {
            throw new ConnectionException(e.getCode(), e.getMessage());
        }
    }

    /**
     * Get the Position for a specific user
     *
     * @param id the user id
     * @return the position
     * @throws ConnectionException something bad happened while requesting
     */
    public static Position getPositionForUser(String id) throws ConnectionException {
        checkInstance();

        try {
            return instance.userUserIdPositionGET(id);
        } catch (ApiException e) {
            throw new ConnectionException(e.getCode(), e.getMessage());
        }
    }

    /**
     * Register a new totem
     *
     * @param name                  totem name (id)
     * @param password              password
     * @param visualizableAnnounces number of announces visualizable at the same time on the screen
     * @param position              latitude and longitude
     * @return an empty optional if the request was successful, a RegistrationError if not
     * @throws ConnectionException something bad happened while requesting
     */
    public static Optional<RegistrationErrors> registerNewTotem(String name, String password, int visualizableAnnounces, Position position) throws ConnectionException {
        checkInstance();

        TotemData cred = new TotemData();
        cred.setTotemId(name);
        cred.setPassword(password);
        cred.setVisualizableAnnounces(visualizableAnnounces);
        cred.setPosition(position);

        try {
            instance.totemPOST(cred);
            return Optional.empty();
        } catch (ApiException e) {
            return manageRegistrationError(e);
        }
    }

    /*
     * USER
     */

    /**
     * Login
     *
     * @param username the username
     * @param password the password
     * @return the Response HTTP Code
     */
    public static int loginUser(String username, String password) {
        checkInstance();

        instance.getApiClient().addDefaultHeader("Entity", "");
        instance.getApiClient().setUsername(username);
        instance.getApiClient().setPassword(password);

        //The user is not logged only if the status code is 401.
        //If code is 200 the user is logged (and the user exists).
        //If code is 404 the user is logged (but the user does not exists).
        try {
            instance.userUserIdInterestsGET(username);
            return 200;
        } catch (ApiException e) {
            return e.getCode();
        }
    }

    /**
     * Register a new user
     *
     * @param name     username (id)
     * @param password password
     * @return null if the registration was successful, a RegistrationError if not
     * @throws ConnectionException
     */
    public static RegistrationErrors registerNewUser(String name, String password) throws ConnectionException {
        checkInstance();

        Credentials cred = new Credentials();
        cred.setUserId(name);
        cred.setPassword(password);

        try {
            instance.userPOST(cred);
            return null;
        } catch (ApiException e) {
            if (e.getCode() == 422) {
                RegistrationErrors registrationError = new RegistrationErrors();
                try {
                    JSONObject o = new JSONObject(e.getResponseBody());
                    registrationError.setDuplicatedUsername(o.optBoolean("duplicatedUsername"));
                    registrationError.setTooShortPassword(o.optBoolean("tooShortPassword"));
                    registrationError.setTooShortUsername(o.optBoolean("tooShortUsername"));
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                return registrationError;
            } else {
                throw new ConnectionException(e.getCode(), e.getMessage());
            }
        }
    }

    /**
     * Fetch all tags
     *
     * @return the set of tags present on the server
     * @throws ConnectionException something bad happened while requesting
     */
    public static Set<String> fetchAllTags() throws ConnectionException {
        checkInstance();

        try {
            return new HashSet<>(instance.tagsGET());
        } catch (ApiException e) {
            throw new ConnectionException(e.getCode(), e.getMessage());
        }
    }

    /**
     * Fetch all tags for a specified user
     *
     * @param myUserId the user id
     * @return the set of tags selected by the user
     * @throws ConnectionException something bad happened while requesting
     */
    public static Set<String> fetchMyTags(String myUserId) throws ConnectionException {
        checkInstance();

        try {
            return new HashSet<>(instance.userUserIdInterestsGET(myUserId));
        } catch (ApiException e) {
            throw new ConnectionException(e.getCode(), e.getMessage());
        }
    }

    /**
     * Set tags for a specified user
     *
     * @param myUserId the user id
     * @param tags     the new set of tags
     * @throws ConnectionException something bad happened while requesting
     */
    public static void setMyTags(String myUserId, Set<String> tags) throws ConnectionException {
        checkInstance();

        try {
            instance.userUserIdInterestsPOST(myUserId, new ArrayList<>(tags));
        } catch (ApiException e) {
            throw new ConnectionException(e.getCode(), e.getMessage());
        }
    }

    /**
     * Set position for a specified user
     *
     * @param myUserId  the user id
     * @param latitude  the latitude
     * @param longitude the longitude
     * @throws ConnectionException something bad happened while requesting
     */
    public static void setMyPos(String myUserId, double latitude, double longitude) throws ConnectionException {
        checkInstance();

        Position position = new Position();
        position.setLatitude(latitude);
        position.setLongitude(longitude);

        try {
            instance.userUserIdPositionPOST(myUserId, position);
        } catch (ApiException e) {
            throw new ConnectionException(e.getCode(), e.getMessage());
        }
    }

    /**
     * Get all existing interests
     *
     * @return a set containing all interests
     * @throws ConnectionException something bad happened while requesting
     */
    public static Set<String> getAllTags() throws ConnectionException {
        checkInstance();

        try {
            return new HashSet<>(instance.tagsGET());
        } catch (ApiException e) {
            throw new ConnectionException(e.getCode(), e.getMessage());
        }
    }

    /*
     * PROVIDER
     */

    /**
     * Login
     *
     * @param username the username
     * @param password the password
     * @return true if the provider is logged, false if credentials are wrong
     */
    public static boolean loginProvider(String username, String password) {
        checkInstance();

        instance.getApiClient().setUsername(username);
        instance.getApiClient().setPassword(password);

        //The provider is not logged only if the status code is 401.
        //If code is 200 the provider is logged (and the content exists).
        //If code is 404 the provider is logged (but the content does not exists).
        try {
            instance.providerProviderIdContentsGET(username);
        } catch (ApiException e) {
            return e.getCode() == 404;
        }
        return true;
    }

    /**
     * Register a new provider
     *
     * @param name     provider name (id)
     * @param password provider password
     * @return an empty optional if the request was successful, a RegistrationError if not
     * @throws ConnectionException something bad happened while requesting
     */
    public static Optional<RegistrationErrors> registerNewProvider(String name, String password) throws ConnectionException {
        checkInstance();

        Credentials cred = new Credentials();
        cred.setUserId(name);
        cred.setPassword(password);

        try {
            instance.providerPOST(cred);
            return Optional.empty();
        } catch (ApiException e) {
            return manageRegistrationError(e);
        }
    }

    /**
     * Get a list of contents for a certain provider
     *
     * @param name provider name (id)
     * @return the list of registered contents (may be empty)
     * @throws ConnectionException something bad happened while requesting
     */
    public static Collection<Integer> getContentsForProvider(String name) throws ConnectionException {
        checkInstance();

        try {
            return instance.providerProviderIdContentsGET(name);
        } catch (ApiException e) {
            throw new ConnectionException(e.getCode(), e.getMessage());
        }
    }

    /**
     * Creates a new content for a certain provider
     *
     * @param name           provider name (id)
     * @param tags           list of tags
     * @param expirationDate content expiration date
     * @return the zip id
     * @throws ConnectionException something bad happened while requesting
     */
    public static int setContentForProvider(String name, List<String> tags, DateTime expirationDate, String url) throws ConnectionException {
        checkInstance();

        ContentRegistrationData content = new ContentRegistrationData();
        content.setTag(tags);
        content.setExpirationDate(expirationDate);
        content.setUrl(url);

        try {
            return instance.providerProviderIdContentsPOST(name, content);
        } catch (ApiException e) {
            throw new ConnectionException(e.getCode(), e.getMessage());
        }
    }

    /**
     * Set the new metadata for a content with a specified Id
     *
     * @param id       the content id
     * @param metadata the new metadata
     * @throws ConnectionException something bad happened while requesting
     */
    public static void modifyContentWithId(String id, ContentRegistrationData metadata) throws ConnectionException {
        checkInstance();

        try {
            instance.contentContentIdPOST(id, metadata);
        } catch (ApiException e) {
            throw new ConnectionException(e.getCode(), e.getMessage());
        }
    }

    /**
     * Delete a content with a specified Id
     *
     * @param id the content id
     * @throws ConnectionException something bad happened while requesting
     */
    public static void deleteContentWithId(String id) throws ConnectionException {
        checkInstance();

        try {
            instance.contentContentIdDELETE(id);
        } catch (ApiException e) {
            throw new ConnectionException(e.getCode(), e.getMessage());
        }
    }
}