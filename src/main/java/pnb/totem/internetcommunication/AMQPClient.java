package pnb.totem.internetcommunication;

import com.rabbitmq.client.*;
import pnb.totem.utilities.SysKB;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

/**
 * AMQPClient:
 * Client for the AMQP Server
 */
public class AMQPClient {
    private static String queueName;
    //private final static String HOST = "192.168.10.11";

    private AMQPClient() {
    }

    /**
     * Register as subscriber on an AMQP Queue
     *
     * @param queueName the name of the queue
     * @param callback  the callback function
     * @throws IOException
     * @throws TimeoutException
     */
    public static void register(String queueName, Function<String, Void> callback) throws IOException, TimeoutException {
        AMQPClient.queueName = queueName;

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(SysKB.HOST);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(queueName, false, false, false, null);

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                super.handleDelivery(consumerTag, envelope, properties, body);
                String message = new String(body);
                callback.apply(message);
            }
        };
        channel.basicConsume(queueName, true, consumer);
        System.out.println("Registered on RabbitMQ | Host: \'" + SysKB.HOST + "\' - Queue: \'" + queueName + "\'");
    }
}
