package pnb.totem.view;

import fi.iki.elonen.NanoHTTPD;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import pnb.totem.utilities.SysKB;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

/**
 * ViewHttpServer:
 * Implements a Web Server using NanoHTTPD
 */
public class ViewHttpServer extends NanoHTTPD {

    private final ViewModel viewModel;

    public ViewHttpServer(@NotNull String localIp, int port, @NotNull ViewModel viewModel) throws IOException {
        super(localIp, port);
        if (viewModel == null) {
            throw new IllegalArgumentException("View model can't be null");
        }

        this.viewModel = viewModel;
        start(NanoHTTPD.SOCKET_READ_TIMEOUT, true);
        System.out.println("View server running at http://" + localIp + ":" + port + "/");
    }

    private static byte[] readAllBytes(String filename) throws IOException {
        return Files.readAllBytes(new File(filename).toPath());
    }

    @Override
    public Response serve(IHTTPSession session) {
        String mimeType = "text/html";
        if (session.getMethod() != Method.GET) {
            return newFixedLengthResponse(Response.Status.METHOD_NOT_ALLOWED, mimeType, "");
        }
        try {
            Response response;

            if ("/slots".equals(session.getUri())) {
                mimeType = "application/json";
                try (ViewModelSnapshot snapshot = viewModel.getSnapshot()) {
                    List<SlotViewModel> slots = snapshot.getSlots();
                    JSONObject result = new JSONObject();
                    result.put("slots", viewModel.getSlotsNumber());

                    JSONArray slotsArray = new JSONArray();
                    for (SlotViewModel slot : slots) {
                        JSONObject slotObj = new JSONObject();
                        slotObj.put("url", slot.getUrl().orElse(""));
                        slotObj.put("position", slot.getPosition());
                        slotsArray.put(slotObj);
                    }

                    result.put("contents", slotsArray);

                    response = newFixedLengthResponse(Response.Status.OK, mimeType, result.toString());
                }
            } else if ("/".equals(session.getUri())) {
                mimeType = "text/html";
                response = newFixedLengthResponse(Response.Status.OK, mimeType, new String(readAllBytes(SysKB.TOTEM_VIEW_HTML)));
            } else if ("/jquery.js".equals(session.getUri())) {
                mimeType = "text/javascript";
                response = newFixedLengthResponse(Response.Status.OK, mimeType, new String(readAllBytes(SysKB.TOTEM_VIEW_JQUERY), "UTF-8"));
            } else {
                mimeType = "text/html";
                response = newFixedLengthResponse(Response.Status.NOT_FOUND, mimeType, "");
            }

            response.addHeader("Access-Control-Allow-Origin", "*");

            return response;
        } catch (IOException | JSONException e) {
            System.err.println("Error serving uri " + session.getUri());
            e.printStackTrace();
            return newFixedLengthResponse(Response.Status.INTERNAL_ERROR, mimeType, "");
        }
    }
}
