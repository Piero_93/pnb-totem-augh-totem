package pnb.totem.view;

import org.jetbrains.annotations.Nullable;
import pnb.totem.model.Content;

import java.io.IOException;

public interface ContentView {
    void setContent(@Nullable Content content, int slotNumber) throws IOException, IllegalStateException;

    void show() throws IOException, IllegalStateException;

    void hide() throws IllegalStateException;

    boolean isVisible();
}
