package pnb.totem.view;

import org.jetbrains.annotations.Nullable;

import java.util.Optional;

/**
 * ImmutableSlowViewModel:
 * An immutable implementation for SlotViewModel
 */
public final class ImmutableSlotViewModel implements SlotViewModel {

    private final int position;
    private final String url;

    public ImmutableSlotViewModel(int position, @Nullable String url) {
        if (position < 0) {
            throw new IllegalArgumentException("Position can't be a negative number");
        }

        this.position = position;
        this.url = url;
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public Optional<String> getUrl() {
        return Optional.ofNullable(url);
    }
}
