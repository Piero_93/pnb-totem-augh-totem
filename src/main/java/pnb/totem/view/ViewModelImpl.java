package pnb.totem.view;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ViewModelImpl:
 * Implementation for the ViewModel
 */
public class ViewModelImpl implements ViewModel {

    private final UnpackedContentRefCounter.NoRefsCallback<String> noRefsCallback;
    private final Map<String, UnpackedContentRefCounter<String>> refs = new HashMap<>();
    private final int slotsNumber;
    private volatile List<SlotViewModel> model;

    public ViewModelImpl(int slotsNumber,
                         @NotNull List<SlotViewModel> initial,
                         @NotNull UnpackedContentRefCounter.NoRefsCallback<String> urlUnusedCallback) {
        if (slotsNumber <= 0) {
            throw new IllegalArgumentException("Slots number must be a positive number");
        }

        if (initial == null) {
            throw new IllegalArgumentException("Initial viewmodel snapshot can't be null");
        }

        if (urlUnusedCallback == null) {
            throw new IllegalArgumentException("Url reference counter callback can't be null");
        }

        this.slotsNumber = slotsNumber;

        this.model = new ArrayList<>(initial);
        this.noRefsCallback = urlUnusedCallback;

        incrementAllRefs(model);
    }

    @Override
    public int getSlotsNumber() {
        return slotsNumber;
    }

    @Override
    @NotNull
    public synchronized ViewModelSnapshot getSnapshot() {
        List<SlotViewModel> snap = new ArrayList<>(model);
        incrementAllRefs(snap);

        return new ViewModelSnapshot() {
            @Override
            public List<SlotViewModel> getSlots() {
                return snap;
            }

            @Override
            public void close() throws IOException {
                synchronized (this) {
                    decrementAllRefs(snap);
                }
            }
        };
    }

    @Override
    public synchronized void setSnapshot(@NotNull List<SlotViewModel> snapshot) {
        if (snapshot.size() != getSlotsNumber()) {
            throw new IllegalArgumentException("Snapshot size must equal slots number");
        }

        List<SlotViewModel> prev = new ArrayList<>(model);
        model = new ArrayList<>(snapshot);
        incrementAllRefs(model);
        decrementAllRefs(prev);
    }

    private synchronized void incrementAllRefs(List<SlotViewModel> toInc) {
        toInc.forEach((slot) -> slot.getUrl().ifPresent((x) -> {
                    UnpackedContentRefCounter<String> refCounter = refs.computeIfAbsent(x, (k) -> new UnpackedContentRefCounter<>(k, (c) -> {
                        refs.remove(c);
                        noRefsCallback.onNoRefs(c);
                    }));

                    if (refCounter != null) {
                        refCounter.inc();
                    }
                })
        );
    }

    private synchronized void decrementAllRefs(List<SlotViewModel> toInc) {
        toInc.forEach((slot) -> slot.getUrl().ifPresent((x) -> {
            UnpackedContentRefCounter<String> refCounter = refs.get(x);
            if (refCounter != null) {
                refCounter.dec();
            }
        }));
    }
}
