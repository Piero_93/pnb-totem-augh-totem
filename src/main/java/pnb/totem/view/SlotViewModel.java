package pnb.totem.view;

import java.util.Optional;

/**
 * SlotViewModel:
 * Models a slot, including its position and its content.
 */
public interface SlotViewModel {
    /**
     * Get the position of this slot
     *
     * @return the position
     */
    int getPosition();

    /**
     * Returns the url of the content.
     *
     * @return The url of the content to show.
     * If the returned optional is empty then the slot should be considered empty.
     */
    Optional<String> getUrl();
}
