package pnb.totem.view;

import java.io.Closeable;
import java.util.List;

interface ViewModelSnapshot extends Closeable {
    List<SlotViewModel> getSlots();
}
