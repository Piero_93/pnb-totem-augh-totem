package pnb.totem.view;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Utility utilizzata per tenere traccia dell'uso dei contenuti decompressi.
 * </p>
 * Mentre il contenuto decompresso è in uso non deve essere cancellato.
 * Chiunque voglia utilizzarlo deve incrementare il contatore dei riferimenti utilizzando
 * il metodo {@link #inc()}. Una volta completate le operazioni che richiedono il contenuto
 * è necessario richiamare {@link #dec()}. Quanto il numero di riferimenti raggiunge lo 0
 * viene richiamato un eventuale callback che si occuperà di eliminare il contenuto dal disco.
 *
 * Una volta raggiunto un numero di riferimenti pari a 0 il contatore non può essere
 * ulteriormente incrementato o decrementato.
 *
 * @param <T> Il tipo del contenuto
 */
public class UnpackedContentRefCounter<T> {

    private final T content;
    private final NoRefsCallback<T> callback;
    private volatile int counter;
    private volatile boolean done;

    /**
     * Costruisce un ref counter inizializzato a 0
     *
     * @param content Il contenuto di cui manterenre il numero di riferimenti
     * @param callback Il callback da richiamare quanto il numero di riferimenti raggiunge lo 0
     */
    public UnpackedContentRefCounter(@NotNull T content, @Nullable NoRefsCallback<T> callback) {
        if (content == null) {
            throw new IllegalArgumentException("Content can't be null");
        }

        this.content = content;
        this.callback = callback;
        this.counter = 0;
        this.done = false;
    }

    /**
     * Incrementa il numero di riferimenti
     * </p>
     * Questo metodo ha effetto solamente se non è mai stato raggiunto lo 0
     */
    public synchronized void inc() {
        if (!done) {
            counter++;
        }
    }

    /**
     * Decrementa il numero di riferimenti
     * </p>
     * Questo metodo ha effetto solamente se non è mai stato raggiunto lo 0.
     */
    public synchronized void dec() {
        if (!done) {
            counter--;
            if (counter <= 0) {
                done = true;
                if (callback != null) {
                    callback.onNoRefs(content);
                }
            }
        }
    }

    public interface NoRefsCallback<T> {
        void onNoRefs(@NotNull T content);
    }
}
