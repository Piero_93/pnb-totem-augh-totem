package pnb.totem.view;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * ViewModel:
 * Models the view.
 */
public interface ViewModel {

    /**
     * Get the number of slot
     *
     * @return the number of slot.
     */
    int getSlotsNumber();

    /**
     * Get the current snapshot
     *
     * @return the current snapshot
     */
    @NotNull
    ViewModelSnapshot getSnapshot();

    /**
     * Set the current snapshot
     *
     * @param slots the current list of slots
     */
    void setSnapshot(@NotNull List<SlotViewModel> slots);
}
