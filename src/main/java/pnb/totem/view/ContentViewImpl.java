package pnb.totem.view;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import pnb.totem.model.Content;
import pnb.totem.utilities.SysKB;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.Semaphore;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static pnb.totem.utilities.SysKB.TOTEM_VIEW_HTML;

/**
 * ContentViewImpl:
 * Implementation for a ContentView
 */
public class ContentViewImpl implements ContentView {

    private static final boolean HOMEPAGE_AS_FILE = true;

    private List<Content> contents;
    private Map<Integer, String> unpackedFiles;

    private int slots;
    private BrowserStarter browserProcessBuilder;
    private Optional<Process> browserProcess;
    private Optional<Thread> processController;
    private Optional<ViewHttpServer> server;
    private ExitFlag flag;
    private ViewModel viewModel;

    /**
     * Constructor
     *
     * @param slots                 number of slot
     * @param browserProcessBuilder
     */
    public ContentViewImpl(int slots, @NotNull BrowserStarter browserProcessBuilder) {
        if (slots <= 0) {
            throw new IllegalArgumentException("Slots must be greater than 0");
        }

        if (browserProcessBuilder == null) {
            throw new IllegalArgumentException("Process builder for browser can't be null");
        }

        this.browserProcess = Optional.empty();
        this.processController = Optional.empty();
        this.viewModel = new ViewModelImpl(slots, Collections.emptyList(), this::clearContentDirectory);
        this.flag = null;
        this.slots = slots;
        this.contents = new ArrayList<>(slots);
        this.unpackedFiles = new HashMap<>(slots);
        for (int i = 0; i < slots; i++) {
            this.contents.add(null);
        }

        this.browserProcessBuilder = browserProcessBuilder;
    }

    @Override
    public synchronized void setContent(@Nullable Content content, int slotNumber) throws IOException, IllegalStateException {
        if (slotNumber >= slots) {
            throw new IndexOutOfBoundsException("Invalid slot number " + slotNumber + ", max is " + slots);
        }

        if (content != null) {
            if (!unpackedFiles.containsKey(content.getId())) {
                unpackedFiles.put(content.getId(), unpackContent(content));
            }
        }

        contents.set(slotNumber, content);

        refreshGui();
    }

    @Override
    public void show() throws IOException, IllegalStateException {
        Semaphore sem = new Semaphore(0);
        ExitFlag threadFlag = new ExitFlag();
        Thread controller;

        synchronized (this) {
            if (isVisible()) {
                throw new IllegalStateException("Already visible");
            }

            flag = threadFlag;
            controller = new Thread(() -> {
                boolean notifyProcessCreated = true;
                ViewHttpServer createdServer;
                try {
                    createdServer = new ViewHttpServer("127.0.0.1", 8081, viewModel);
                } catch (IOException e) {
                    sem.release();
                    e.printStackTrace();
                    return;
                }

                setServer(createdServer);

                while (!threadFlag.getFlag()) {
                    try {
                        String url;
                        if (HOMEPAGE_AS_FILE) {
                            url = new File(TOTEM_VIEW_HTML).toURI().toURL().toString();
                        } else {
                            url = SysKB.GUI_URL;
                        }

                        System.out.println("Launching browser with address " + url);
                        Process createdProcess = browserProcessBuilder.showUrl(url);

                        setServer(createdServer);
                        setRunningProcess(createdProcess);
                        if (notifyProcessCreated) {
                            sem.release();
                            notifyProcessCreated = false;
                        }

                        createdProcess.waitFor();
                    } catch (InterruptedException ignored) {
                        //Ignored
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (notifyProcessCreated) {
                            sem.release();
                            return;
                        }
                    }
                }

                createdServer.stop();
                setServer(null);
                setProcessController(null);
            });
            controller.start();
        }

        try {
            sem.acquire();
        } catch (InterruptedException ignored) {
        }

        synchronized (this) {
            if (!getRunningProcess().isPresent()) {
                throw new IOException("Can't start browser");
            }

            setProcessController(controller);
        }
    }

    @Override
    public void hide() throws IllegalStateException {
        Thread controller;
        synchronized (this) {
            if (!isVisible()) {
                throw new IllegalStateException("Already hidden");
            }

            assert getProcessController().isPresent();
            controller = getProcessController().get();
            flag.setFlag();
            flag = null;

            getRunningProcess().ifPresent(Process::destroyForcibly);
            setRunningProcess(null);
        }
        //controller.interrupt();
        try {
            controller.join();
        } catch (InterruptedException ignored) {
        }
    }

    @Override
    public synchronized boolean isVisible() {
        return browserProcess.isPresent();
    }

    private synchronized boolean isReallyVisible() {
        return browserProcess.map(Process::isAlive).orElse(false);
    }

    private synchronized void refreshGui() {
        List<SlotViewModel> slotList = new ArrayList<>(contents.size());
        for (int i = 0; i < contents.size(); i++) {
            Content c = contents.get(i);
            String url = null;
            if (c != null) {
                url = unpackedFiles.get(c.getId());
            }

            slotList.add(new ImmutableSlotViewModel(i, url));
        }

        viewModel.setSnapshot(slotList);

        System.out.println("---GUI REFRESH---");
        for (int i = 0; i < contents.size(); i++) {
            Content c = contents.get(i);
            if (c != null) {
                System.out.println(i + ": [" + c.getId() + "] " + c.getUrl());
                System.out.println("> " + slotList.get(i).getUrl().orElse(""));
            } else {
                System.out.println(i + ": empty");
            }

        }
        System.out.println("-----------------");
    }

    private synchronized Optional<Process> getRunningProcess() {
        return browserProcess;
    }

    private synchronized void setRunningProcess(Process p) {
        browserProcess = Optional.ofNullable(p);
    }

    private synchronized Optional<Thread> getProcessController() {
        return processController;
    }

    private synchronized void setProcessController(Thread p) {
        processController = Optional.ofNullable(p);
    }

    private synchronized Optional<ViewHttpServer> getServer() {
        return server;
    }

    private synchronized void setServer(ViewHttpServer s) {
        server = Optional.ofNullable(s);
    }

    private synchronized void clearContentDirectory(String url) {
        int id = -1;
        for (Map.Entry<Integer, String> idUrlEntry : unpackedFiles.entrySet()) {
            if (idUrlEntry.getValue().equals(url)) {
                id = idUrlEntry.getKey();
                break;
            }
        }

        if (id == -1) {
            return;
        }

        unpackedFiles.remove(id);

        try {
            URL pathUrl = new URL(url);
            String filePath = pathUrl.getPath();
            File dir = new File(filePath);

            cleanDir(dir);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private synchronized String unpackContent(Content content) throws IOException {
        File dir = new File(SysKB.GUI_CONTENT_DIR, "Content" + content.getId());
        if (!dir.exists()) {
            if (!(dir.mkdirs())) {
                throw new IOException("Can't create directory for content " + content.getId());
            }
        } else if (!dir.isDirectory()) {
            throw new IOException("There is a file with the same name of the target dir for content " + content.getId());
        } else {
            cleanDir(dir);
        }

        ZipFile totemContent = content.getBundle().getTotemContentAsZip();
        Enumeration<? extends ZipEntry> entries = totemContent.entries();
        try {
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                if (!entry.isDirectory()) {
                    try (InputStream stream = totemContent.getInputStream(entry)) {
                        File f = new File(dir, entry.getName());
                        f.getParentFile().mkdirs();
                        Files.copy(stream, f.toPath(), REPLACE_EXISTING);
                    }
                }
            }
        } catch (IOException e) {
            cleanDir(dir);
            throw e;
        }

        return dir.toURI().toURL().toString();
    }

    private void cleanDir(File dir) {
        try {
            String[] entries = dir.list();
            if (entries != null) {
                for (String s : entries) {
                    File currentFile = new File(dir.getPath(), s);
                    currentFile.delete();
                }

                dir.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * BrowserStarter:
     * Starter for the browser.
     */
    public interface BrowserStarter {
        /**
         * Opens a browser page pointing the specified url
         *
         * @param url the url
         * @return the browser process
         * @throws IOException
         */
        @NotNull
        Process showUrl(@NotNull String url) throws IOException;
    }
}
