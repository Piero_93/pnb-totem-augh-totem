package pnb.totem.view;

/**
 * ExitFlag:
 * Flag used to close gracefully the application.
 */
public final class ExitFlag {
    private final Object cond = new Object();
    private volatile boolean flag = false;

    /**
     * Set the flag to true
     */
    public void setFlag() {
        synchronized (cond) {
            flag = true;
            cond.notifyAll();
        }
    }

    /**
     * Get the flag value
     *
     * @return the value
     */
    public boolean getFlag() {
        return flag;
    }

    public void waitFlag() throws InterruptedException {
        synchronized (cond) {
            while (!flag) {
                cond.wait();
            }
        }
    }
}
