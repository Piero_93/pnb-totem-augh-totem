package pnb.totem.model;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.jetbrains.annotations.NotNull;
import pnb.totem.utilities.Utilities;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import static pnb.totem.utilities.SysKB.*;

/**
 * ContentBundleImpl:
 * An implementation for ContentBundle.
 */
public class ContentBundleImpl implements ContentBundle {
    private File localPath;

    public ContentBundleImpl(int id, byte[] zipBytes) throws Exception {
        this.localPath = new File(PATH_PREFIX + id + "/");

        FileOutputStream completeContentOutputStream = null;
        FileOutputStream splashScreenOutputStream = null;
        FileOutputStream thumbnailOutputStream = null;

        try {
            File completeContentFile = new File(localPath, COMPLETE_CONTENT);
            completeContentFile.getParentFile().mkdirs();
            completeContentOutputStream = new FileOutputStream(completeContentFile);
            completeContentOutputStream.write(zipBytes);
            completeContentOutputStream.close();

            ZipFile zipFile = new ZipFile(completeContentFile);

            //Splash Screen
            InputStream splashScreenInputStream = zipFile.getInputStream(zipFile.getEntry("splashScreen/splashScreen.png"));
            File splashScreenFile = new File(localPath, SPLASH_SCREEN);
            splashScreenFile.getParentFile().mkdirs();
            splashScreenOutputStream = new FileOutputStream(splashScreenFile);
            IOUtils.copy(splashScreenInputStream, splashScreenOutputStream);
            splashScreenInputStream.close();
            splashScreenOutputStream.close();

            //Thumbnail
            InputStream thumbnailInputStream = zipFile.getInputStream(zipFile.getEntry("thumbnail/thumbnail.png"));
            File thumbnailFile = new File(localPath, THUMBNAIL);
            thumbnailFile.getParentFile().mkdirs();
            thumbnailOutputStream = new FileOutputStream(thumbnailFile);
            IOUtils.copy(thumbnailInputStream, thumbnailOutputStream);
            thumbnailInputStream.close();
            thumbnailOutputStream.close();

            //TotemContent && AppContent
            File totemContentFile = new File(localPath, TOTEM_CONTENT);
            totemContentFile.getParentFile().mkdirs();
            File appContentFile = new File(localPath, APP_CONTENT);
            appContentFile.getParentFile().mkdirs();
            FileOutputStream totemContentOutputStream = new FileOutputStream(totemContentFile);
            FileOutputStream appContentOutputStream = new FileOutputStream(appContentFile);

            Enumeration<? extends ZipEntry> entries = zipFile.entries();

            while (entries.hasMoreElements()) {
                ZipEntry e = entries.nextElement();
                if (e.getName().startsWith("totemContent")) {
                    Utilities.copyStream(zipFile.getInputStream(e), totemContentOutputStream);
                } else if (e.getName().startsWith("appContent")) {
                    Utilities.copyStream(zipFile.getInputStream(e), appContentOutputStream);
                }
            }

            totemContentOutputStream.close();
            appContentOutputStream.close();
            zipFile.close();
            completeContentOutputStream.close();
        } catch (NullPointerException | IOException e) {
            FileUtils.deleteDirectory(localPath);
            throw new IllegalArgumentException("Zip File content is not a valid Content for PNB-Totem");
        } finally {
            //Files.delete(Paths.get(zipFile.getName()));
        }

        isContentValid();
    }

    @Deprecated
    public ContentBundleImpl(int id, @NotNull ZipFile zipFile) throws Exception {
        this.localPath = new File(PATH_PREFIX + id + "/");

        FileOutputStream completeContentOutputStream = null;
        FileOutputStream splashScreenOutputStream = null;
        FileOutputStream thumbnailOutputStream = null;

        try {
            byte[] zipBytes = contentToByteArray(zipFile);
            File completeContentFile = new File(localPath, COMPLETE_CONTENT);
            completeContentFile.getParentFile().mkdirs();
            completeContentOutputStream = new FileOutputStream(completeContentFile);
            completeContentOutputStream.write(zipBytes);
            completeContentOutputStream.close();

            //Splash Screen
            InputStream splashScreenInputStream = zipFile.getInputStream(zipFile.getEntry("splashScreen/splashScreen.png"));
            File splashScreenFile = new File(localPath, SPLASH_SCREEN);
            splashScreenFile.getParentFile().mkdirs();
            splashScreenOutputStream = new FileOutputStream(splashScreenFile);
            int nRead;
            byte[] splashScreenBytes = new byte[1024];
            while ((nRead = splashScreenInputStream.read(splashScreenBytes, 0, splashScreenBytes.length)) != -1) {
                splashScreenOutputStream.write(splashScreenBytes, 0, nRead);
            }
            splashScreenInputStream.close();
            splashScreenOutputStream.close();

            //Thumbnail
            InputStream thumbnailInputStream = zipFile.getInputStream(zipFile.getEntry("thumbnail/thumbnail.png"));
            File thumbnailFile = new File(localPath, THUMBNAIL);
            thumbnailFile.getParentFile().mkdirs();
            thumbnailOutputStream = new FileOutputStream(new File(localPath, THUMBNAIL));
            byte[] thumbnailBytes = new byte[1024];
            while ((nRead = thumbnailInputStream.read(thumbnailBytes, 0, thumbnailBytes.length)) != -1) {
                thumbnailOutputStream.write(thumbnailBytes, 0, nRead);
            }
            thumbnailInputStream.close();
            thumbnailOutputStream.close();

            //TotemContent && AppContent
            File totemContentFile = new File(localPath, TOTEM_CONTENT);
            totemContentFile.getParentFile().mkdirs();
            File appContentFile = new File(localPath, APP_CONTENT);
            appContentFile.getParentFile().mkdirs();
            FileOutputStream totemContentOutputStream = new FileOutputStream(totemContentFile);
            FileOutputStream appContentOutputStream = new FileOutputStream(appContentFile);

            Enumeration<? extends ZipEntry> entries = zipFile.entries();

            while (entries.hasMoreElements()) {
                ZipEntry e = entries.nextElement();
                if (e.getName().startsWith("totemContent")) {
                    Utilities.copyStream(zipFile.getInputStream(e), totemContentOutputStream);
                } else if (e.getName().startsWith("appContent")) {
                    Utilities.copyStream(zipFile.getInputStream(e), appContentOutputStream);
                }
            }

            totemContentOutputStream.close();
            appContentOutputStream.close();
            zipFile.close();
            completeContentOutputStream.close();
        } catch (NullPointerException | IOException e) {
            throw new IllegalArgumentException("Zip File content is not a valid Content for PNB-Totem");
        } finally {
            Files.delete(Paths.get(zipFile.getName()));
        }

        isContentValid();
    }

    public ContentBundleImpl(String localPath) throws Exception {
        this.localPath = new File(localPath);

        //If the content is not valid, throws exception
        if (this.getSplashScreen() == null ||
                this.getThumbnail() == null ||
                this.getSplashScreen() == null ||
                this.getTotemContentAsZip() == null ||
                this.getAppContentAsZip() == null) {
            throw new IllegalArgumentException("Content path is not valid");
        }
    }

    @Override
    public BufferedImage getSplashScreen() {
        try {
            //System.err.println("Content " + localPath + ": " + md5(new ByteArrayInputStream(FileUtils.readFileToByteArray(new File(localPath + SPLASH_SCREEN)))));
            return ImageIO.read(new File(localPath, THUMBNAIL));
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public BufferedImage getThumbnail() {
        try {
            return ImageIO.read(new File(localPath, THUMBNAIL));
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public byte[] getTotemContent() {
        try {
            return FileUtils.readFileToByteArray(new File(localPath, TOTEM_CONTENT));
        } catch (IOException e) {
            return new byte[0];
        }
    }

    @Override
    public byte[] getAppContent() {
        try {
            return FileUtils.readFileToByteArray(new File(localPath, APP_CONTENT));
        } catch (IOException e) {
            return new byte[0];
        }
    }

    @Override
    public ZipFile getTotemContentAsZip() {
        try {
            return new ZipFile(new File(localPath, TOTEM_CONTENT));
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public ZipFile getAppContentAsZip() {
        try {
            return new ZipFile(new File(localPath, APP_CONTENT));
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public ZipFile getContentZip() {
        try {
            return new ZipFile(new File(localPath, COMPLETE_CONTENT));
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public String getLocalPath() {
        return localPath.getAbsolutePath();
    }

    private byte[] contentToByteArray(ZipFile zipFile) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ZipOutputStream zipOutputStream = new ZipOutputStream(bos);
        Enumeration<? extends ZipEntry> enumeration = zipFile.entries();
        while (enumeration.hasMoreElements()) {
            ZipEntry e = enumeration.nextElement();
            e.setCompressedSize(-1);
            zipOutputStream.putNextEntry(e);
            zipOutputStream.closeEntry();
        }
        zipOutputStream.close();

        return bos.toByteArray();
    }

    @Override
    public String toString() {
        return "ContentBundleImpl{" +
                "localPath='" + localPath + '\'' +
                '}';
    }

    private static char[] hexDigits = "0123456789abcdef".toCharArray();

    private static String md5(InputStream is) throws IOException {
        String md5 = "";

        try {
            byte[] bytes = new byte[4096];
            int read = 0;
            MessageDigest digest = MessageDigest.getInstance("MD5");

            while ((read = is.read(bytes)) != -1) {
                digest.update(bytes, 0, read);
            }

            byte[] messageDigest = digest.digest();

            StringBuilder sb = new StringBuilder(32);

            for (byte b : messageDigest) {
                sb.append(hexDigits[(b >> 4) & 0x0f]);
                sb.append(hexDigits[b & 0x0f]);
            }

            md5 = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return md5;
    }

    private void isContentValid() throws Exception {
        boolean splashScreenExists = this.getSplashScreen() != null;
        boolean thumbnailExists = this.getThumbnail() != null;
        boolean totemContentExists = this.getTotemContent() != null;
        boolean appContentExists = this.getAppContent() != null;
        boolean completeContentExists = this.getContentBytes() != null;

        //If the content is not valid, throws exception
        if (!(splashScreenExists &&
                thumbnailExists &&
                totemContentExists &&
                appContentExists &&
                completeContentExists)) {
            System.out.println("Local path exists = " + localPath.exists());
            System.out.println("Local path isDirectory = " + localPath.isDirectory());
            if (localPath.isDirectory()) {
                for (File file : localPath.listFiles()) {
                    System.out.println("File: " + file);
                }
            }

            System.out.println("splashScreenExists = " + splashScreenExists);
            System.out.println("thumbnailExists = " + thumbnailExists);
            System.out.println("totemContentExists = " + totemContentExists);
            System.out.println("appContentExists = " + appContentExists);
            System.out.println("completeContentExists = " + completeContentExists);
            throw new IllegalArgumentException("Content path is not valid: " + localPath);
        }
    }

    @Override
    public byte[] getContentBytes() {
        File f = new File(localPath, COMPLETE_CONTENT);
        try {
            return FileUtils.readFileToByteArray(f);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
