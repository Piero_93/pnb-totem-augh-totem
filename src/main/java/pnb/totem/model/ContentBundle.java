package pnb.totem.model;

import java.awt.image.BufferedImage;
import java.util.zip.ZipFile;

/**
 * ContentBundle:
 * All the data contained in a content.
 */
public interface ContentBundle {
    /**
     * Get the splash screen
     *
     * @return the splash screen
     */
    BufferedImage getSplashScreen();

    /**
     * Get the thumbnail
     *
     * @return the thumbnail
     */
    BufferedImage getThumbnail();

    /**
     * Get the totem content as bytes
     *
     * @return the totem content
     */
    byte[] getTotemContent();

    /**
     * Get the app content as bytes
     *
     * @return the app content
     */
    byte[] getAppContent();

    /**
     * Get the totem content as zip
     *
     * @return the totem content
     */
    ZipFile getTotemContentAsZip();

    /**
     * Get the app content as zip
     *
     * @return the app content
     */
    ZipFile getAppContentAsZip();

    /**
     * Get the local path
     *
     * @return the local path
     */
    String getLocalPath();

    /**
     * Get the whole content as byte array
     *
     * @return the content
     */
    byte[] getContentBytes();

    /**
     * Get the whole content as zip
     *
     * @return the content
     */
    ZipFile getContentZip();
}
