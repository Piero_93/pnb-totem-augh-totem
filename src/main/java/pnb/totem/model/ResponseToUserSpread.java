package pnb.totem.model;

import java.util.List;

public class ResponseToUserSpread {
    private List<Integer> toBeSentToTotem;
    private List<Integer> toBeSentToApp;

    public ResponseToUserSpread() {}

    public List<Integer> getToBeSentToTotem() {
        return toBeSentToTotem;
    }

    public void setToBeSentToTotem(List<Integer> toBeSentToTotem) {
        this.toBeSentToTotem = toBeSentToTotem;
    }

    public List<Integer> getToBeSentToApp() {
        return toBeSentToApp;
    }

    public void setToBeSentToApp(List<Integer> toBeSentToApp) {
        this.toBeSentToApp = toBeSentToApp;
    }
}
